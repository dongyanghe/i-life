//
//  ILBlueConnectView.m
//  iLife
//
//  Created by hedongyang on 2020/7/22.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBlueConnectView.h"
#import "ILBlueManager.h"

@interface ILBlueConnectView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView * tableView;
@end

@implementation ILBlueConnectView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubViews];
    }
    return self;
}

- (void)setEngineModel:(ILGlobalEngineModel *)engineModel
{
    _engineModel = engineModel;
    if ([ILBlueManager isConnected]) {
        [self.addButton setTitle:@"设备解绑" forState:UIControlStateNormal];
        self.addButton.backgroundColor = [UIColor redColor];
    }else {
        if (_engineModel.isBind) {
            [self.addButton setTitle:@"重新连接" forState:UIControlStateNormal];
            self.addButton.backgroundColor = MainColor;
        }else {
            [self.addButton setTitle:@"确认绑定" forState:UIControlStateNormal];
            self.addButton.backgroundColor = MainColor;
        }
    }
    [self.tableView reloadData];
}

- (void)addSubViews
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.tableFooterView = [UIView new];
    [self addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(@0);
        make.height.equalTo(@110);
    }];
        
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addButton setTitle:@"确认绑定" forState:UIControlStateNormal];
    self.addButton.layer.masksToBounds = YES;
    self.addButton.layer.cornerRadius = 5.0f;
    self.addButton.backgroundColor = MainColor;
    self.addButton.tintColor = [UIColor whiteColor];
    [self addSubview:self.addButton];
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.bottom.equalTo(@(-20));
        make.height.equalTo(@50);
    }];
    
    UIImageView * bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"bind_bg"];
    bgImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:bgImageView];
    
    [bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.left.equalTo(@40);
        make.right.equalTo(@(-40));
        make.top.equalTo(self.tableView.mas_bottom).offset(30);
        make.bottom.equalTo(self.addButton.mas_top).offset(-30);
    }];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"cellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"设备名称:%@",_engineModel.deviceName];
    }else if (indexPath.row == 1) {
        cell.textLabel.text = [NSString stringWithFormat:@"设备序号:%@",_engineModel.uuidStr];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

@end
