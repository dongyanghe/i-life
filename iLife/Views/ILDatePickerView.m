//
//  ILDatePickerView.m
//
//  Created by 陈君祁 on 2020/4/22.
//  Copyright © 2020 陈君祁. All rights reserved.
//

#import "ILDatePickerView.h"
#import "ILDatePickerView.h"

@interface ILDatePickerView ()

@property (nonatomic,strong) UIDatePicker * datePicker;
@property (nonatomic,copy) DateBlock myDateBlock;
@end

@implementation ILDatePickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubviews];
        self.hidden = YES;
    }
    return self;
}

-(void)addSubviews {
    
    UIButton * bgButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [bgButton setBackgroundColor:UIColorWithRGBA(0,0,0,0.3)];
    [self addSubview:bgButton];
    [bgButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    UIView *contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(364);
    }];
    
    UIView * headerView = [UIView new];
    headerView.backgroundColor = MainColor;
    [contentView addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(contentView);
        make.height.mas_equalTo(44);
    }];
    
    UIButton * cancelButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    cancelButton.tintColor = [UIColor whiteColor];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancelButton addTarget:self action:@selector(back) forControlEvents:(UIControlEventTouchUpInside)];
    [headerView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(headerView);
        make.left.equalTo(headerView).offset(20);
        make.width.mas_equalTo(60);
    }];
    
    UIButton * confirmButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    confirmButton.tintColor = [UIColor whiteColor];
    [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:(UIControlEventTouchUpInside)];
    [headerView addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(headerView);
        make.right.equalTo(headerView.mas_right).offset(-20);
        make.width.mas_equalTo(60);
    }];
    
    [contentView addSubview:self.datePicker];
    [_datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(contentView);
        make.top.equalTo(headerView.mas_bottom);
    }];
}

-(UIDatePicker *)datePicker {
    if (_datePicker == nil) {
        _datePicker = [[UIDatePicker alloc] init];
        _datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
        _datePicker.datePickerMode = UIDatePickerModeDate;
    }
    return _datePicker;
}

-(void)confirm {
    NSDateFormatter *dateFM = [[NSDateFormatter alloc] init];
    dateFM.dateFormat = @"yyyy-MM-dd";
    NSString * dateStr = [dateFM stringFromDate:_datePicker.date];
    if (self.dateCallback) {
        self.dateCallback(dateStr);
    }
    [self back];
}

- (void)back
{
    self.hidden = YES;
}

- (void)show
{
    NSDate * date = [NSDate date];
    if (self.dateStr && self.dateStr.length > 0) {
        NSDateFormatter *dateFM = [[NSDateFormatter alloc] init];
        dateFM.dateFormat = @"yyyy-MM-dd";
        date = [dateFM dateFromString:self.dateStr];
    }
    self.datePicker.date = date;
    self.hidden = NO;
}

@end
 
