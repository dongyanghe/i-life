//
//  ILMonthChartView.m
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILMonthChartView.h"


@implementation ILMonthChartView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       self.backgroundColor = [UIColor clearColor];
       self.frame = frame;
    }
    return self;
}

- (void)setChartLayerModel:(ILMonthChartLayerModel *)chartLayerModel
{
    _chartLayerModel = chartLayerModel;
    [self setFrame:_chartLayerModel.chartViewFrame];
    [self.layer setNeedsDisplay];
}

#pragma mark - AsyncLayerDelegate

+ (Class)layerClass {
    return YYAsyncLayer.class;
}

- (YYAsyncLayerDisplayTask *)newAsyncDisplayTask {
    
    YYAsyncLayerDisplayTask * displayTask = [YYAsyncLayerDisplayTask new];
    
    __weak __typeof(&*self)weakSelf = self;
    [displayTask setWillDisplay:^(CALayer * _Nonnull layer) {
        layer.contentsScale = 2;
    }];
    
    [displayTask setDisplay:^(CGContextRef _Nonnull context, CGSize size, BOOL (^ _Nonnull isCancelled)(void)) {
        if (isCancelled())return;
        [weakSelf drawInContext:context withSize:size];
    }];
    
    [displayTask setDidDisplay:^(CALayer * _Nonnull layer, BOOL isFinish) {
        
    }];
    
    return displayTask;
}

- (void)drawInContext:(CGContextRef)context withSize:(CGSize)size {
    CGContextClearRect(context,CGRectMake(0,0,size.width,size.height));
    
    CGContextSaveGState(context);
    for (int i = 0; i < _chartLayerModel.verticalLines.count; i++) {
        CGRect frame = CGRectFromString(_chartLayerModel.verticalLines[i]);
        CGContextSetLineWidth(context,frame.size.width);
        CGContextSetRGBStrokeColor(context,204/255.0,204/255.0,204/255.0,1.0);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context,frame.origin.x,frame.origin.y);
        CGContextAddLineToPoint(context,frame.origin.x + frame.size.width,frame.origin.y + frame.size.height);
        CGContextClosePath(context);
        CGContextStrokePath(context);
    }
    
    for (int i = 0; i < _chartLayerModel.horizontalLines.count; i++) {
        CGRect frame = CGRectFromString(_chartLayerModel.horizontalLines[i]);
        CGContextSetLineWidth(context,frame.size.height);
        CGContextSetRGBStrokeColor(context,204/255.0,204/255.0,204/255.0,1.0);
        CGContextBeginPath(context);
        CGContextSetLineCap(context, kCGLineCapRound);
        CGContextMoveToPoint(context,frame.origin.x,frame.origin.y);
        CGFloat lengths[] = {2,5};
        CGContextSetLineDash(context,0,lengths,2);
        CGContextAddLineToPoint(context,frame.origin.x + frame.size.width,frame.origin.y + frame.size.height);
        CGContextClosePath(context);
        CGContextStrokePath(context);
    }
    CGContextRestoreGState(context);
    
    for (int i = 0; i < _chartLayerModel.timerFrames.count; i++) {
        CGRect frame = CGRectFromString(_chartLayerModel.timerFrames[i]);
        NSString * timeStr = _chartLayerModel.timerTexts[i];
        NSMutableParagraphStyle * paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        NSDictionary * dic = @{
                                NSFontAttributeName:_chartLayerModel.timerFont,
                                NSParagraphStyleAttributeName:paragraphStyle,
                                NSForegroundColorAttributeName:_chartLayerModel.textColor};
        [timeStr drawInRect:frame withAttributes:dic];
    }
    
    for (int i = 0; i < _chartLayerModel.numberFrames.count; i++) {
        CGRect frame = CGRectFromString(_chartLayerModel.numberFrames[i]);
        NSString * numberStr = _chartLayerModel.numberTexts[i];
        NSMutableParagraphStyle * paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        NSDictionary * dic = @{
                                NSFontAttributeName:_chartLayerModel.timerFont,
                                NSParagraphStyleAttributeName:paragraphStyle,
                                NSForegroundColorAttributeName:_chartLayerModel.textColor};
        [numberStr drawInRect:frame withAttributes:dic];
    }
    
    for (int i = 0; i < _chartLayerModel.tempFrames.count; i++) {
        CGRect frame = CGRectFromString(_chartLayerModel.tempFrames[i]);
        CGContextSetLineWidth(context,frame.size.height);
        CGContextSetRGBStrokeColor(context,51/255.0,204/255.0,203/255.0,1.0);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context,frame.origin.x,frame.origin.y);
        CGContextAddLineToPoint(context,frame.origin.x + frame.size.width,frame.origin.y);
        CGContextClosePath(context);
        CGContextStrokePath(context);
    }
    
    if (_chartLayerModel.isShow) {
           CGRect frame = _chartLayerModel.showLineFrame;
           CGContextSaveGState(context);
           CGContextSetLineWidth(context,frame.size.width);
           CGContextSetRGBStrokeColor(context,204/255.0,204/255.0,204/255.0,1.0);
           CGContextBeginPath(context);
           CGContextMoveToPoint(context,frame.origin.x,frame.origin.y);
           CGContextAddLineToPoint(context,frame.origin.x + frame.size.width,frame.origin.y + frame.size.height);
           CGContextClosePath(context);
           CGContextStrokePath(context);
           CGContextRestoreGState(context);
       }
}

- (BOOL)containsFrameWithPoint:(CGPoint)point
                      valueDic:(NSDictionary **)valueDic
{
    for (NSDictionary * dic in _chartLayerModel.tempDatas) {
        CGRect frame = CGRectFromString([dic valueForKey:@"frame"]);
        if (   point.x >= frame.origin.x
            && point.y >= frame.origin.y
            && point.x <= frame.origin.x + frame.size.width
            && point.y <= frame.origin.y + frame.size.height) {
            if (valueDic) {
                *valueDic = dic;
            }
            return YES;
        }
    }
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self];
    /*
    if ([self containsFrameWithPoint:point valueDic:nil]) {
         _chartLayerModel.isShow = YES;
    }*/
    CGFloat outside = _chartLayerModel.chartViewFrame.size.width - 40;
    CGFloat x = point.x;
    if (point.x < 16) {
       x = point.x < 16 ? 16 : point.x;
    }else if (point.x > outside) {
       x = point.x > outside ? outside : point.x;
    }
    _chartLayerModel.showLineFrame = CGRectMake(x,0,_chartLayerModel.showLineFrame.size.width,_chartLayerModel.showLineFrame.size.height);
    [self.layer setNeedsDisplay];
    NSDictionary * dic = @{@"frame":NSStringFromCGRect(_chartLayerModel.showLineFrame)};
    if (self.selectOneData) {
        self.selectOneData(dic,NO);
    }
    _chartLayerModel.isShow = YES;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self];
    CGFloat outside = _chartLayerModel.chartViewFrame.size.width - 40;
    CGFloat x = point.x;
    if (point.x < 16) {
       x = point.x < 16 ? 16 : point.x;
    }else if (point.x > outside) {
       x = point.x > outside ? outside : point.x;
    }
    _chartLayerModel.showLineFrame = CGRectMake(x,0,_chartLayerModel.showLineFrame.size.width,_chartLayerModel.showLineFrame.size.height);
    [self.layer setNeedsDisplay];
    NSDictionary * dic = @{@"frame":NSStringFromCGRect(_chartLayerModel.showLineFrame)};
    if (self.selectOneData) {
        self.selectOneData(dic,YES);
    }
    /*
    NSDictionary * dic;
    if ([self containsFrameWithPoint:point valueDic:&dic]) {
       CGFloat outside = _chartLayerModel.chartViewFrame.size.width - 40;
       CGFloat x = point.x;
       if (point.x < 16) {
          x = point.x < 16 ? 16 : point.x;
       }else if (point.x > outside) {
          x = point.x > outside ? outside : point.x;
       }
       _chartLayerModel.showLineFrame = CGRectMake(x,0,_chartLayerModel.showLineFrame.size.width,_chartLayerModel.showLineFrame.size.height);
       [self.layer setNeedsDisplay];
       if (self.selectOneData) {
           self.selectOneData(dic);
       }
    }*/
    
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesMoved:touches withEvent:event];
    CGPoint point = [touches.anyObject locationInView:self];
    CGFloat outside = _chartLayerModel.chartViewFrame.size.width - 40;
    CGFloat x = point.x;
    if (point.x < 16) {
       x = point.x < 16 ? 16 : point.x;
    }else if (point.x > outside) {
       x = point.x > outside ? outside : point.x;
    }
    _chartLayerModel.showLineFrame = CGRectMake(x,0,_chartLayerModel.showLineFrame.size.width,_chartLayerModel.showLineFrame.size.height);
    [self.layer setNeedsDisplay];
    NSDictionary * dic = @{@"frame":NSStringFromCGRect(_chartLayerModel.showLineFrame)};
    if (self.selectOneData) {
        self.selectOneData(dic,NO);
    }
    /*
    NSDictionary * dic;
    if ([self containsFrameWithPoint:point valueDic:&dic]) {
        CGFloat outside = _chartLayerModel.chartViewFrame.size.width - 40;
        CGFloat x = point.x;
        if (point.x < 16) {
           x = point.x < 16 ? 16 : point.x;
        }else if (point.x > outside) {
           x = point.x > outside ? outside : point.x;
        }
        _chartLayerModel.showLineFrame = CGRectMake(x,0,_chartLayerModel.showLineFrame.size.width,_chartLayerModel.showLineFrame.size.height);
        [self.layer setNeedsDisplay];
        if (self.selectOneData) {
            self.selectOneData(dic);
        }
    }*/
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesCancelled:touches withEvent:event];
}

@end
