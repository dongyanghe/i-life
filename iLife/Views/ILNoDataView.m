//
//  ILNoDataView.m
//  iLife
//
//  Created by hedongyang on 2020/8/28.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILNoDataView.h"

@implementation ILNoDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView * iconView = [[UIImageView alloc]init];
        iconView.image = [UIImage imageNamed:@"no_data"];
        iconView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:iconView];
        [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY).offset(-30);
            make.centerX.equalTo(self.mas_centerX);
            make.width.height.equalTo(@100);
        }];
        
        UILabel * title = [[UILabel alloc]init];
        title.text = @"暂无数据";
        title.font = [UIFont systemFontOfSize:14];
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor lightGrayColor];
        [self addSubview:title];
        [title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(iconView.mas_bottom);
        }];
    }
    return self;
}

@end
