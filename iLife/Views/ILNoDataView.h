//
//  ILNoDataView.h
//  iLife
//
//  Created by hedongyang on 2020/8/28.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ILNoDataView : UIView

@end

NS_ASSUME_NONNULL_END
