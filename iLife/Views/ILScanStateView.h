//
//  ILScanStateView.h
//  iLife
//
//  Created by hedongyang on 2020/7/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ILScanStateView : UIView
@property (nonatomic,strong) UIImageView * stateIcon;
@property (nonatomic,strong) UILabel * labelStatus;
@property (nonatomic,strong) UILabel * labelTips;
@end

NS_ASSUME_NONNULL_END
