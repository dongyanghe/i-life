//
//  ILChartView.m
//  iLife
//
//  Created by hedongyang on 2020/7/8.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILChartView.h"
#import "ILHourChartView.h"
#import "ILDeviceInfoModel.h"
#import "ILNoDataView.h"
#import "ILHourChartCollectionViewCell.h"
#import "ILDayChartCollectionViewCell.h"
#import "ILWeekChartCollectionViewCell.h"
#import "ILMonthChartCollectionViewCell.h"
#import "ILYearChartCollectionViewCell.h"
#import "ILUtility.h"

@interface ILShowDataView : UIView
@property (nonatomic,strong) UILabel * titleLabel;
@property (nonatomic,strong) UILabel * tempLabel;
@property (nonatomic,strong) UILabel * dateLabel;
@property (nonatomic,strong) UILabel * timeLabel;
@end

@implementation ILShowDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.cornerRadius = 10;
        self.clipsToBounds = YES;
        self.titleLabel = [[UILabel alloc]init];
        self.titleLabel.font = [UIFont systemFontOfSize:12];
        self.titleLabel.text = @"平均温度";
        self.titleLabel.textColor = [UIColor grayColor];
        [self addSubview:self.titleLabel];
        
        self.tempLabel = [[UILabel alloc]init];
        self.tempLabel.font = [UIFont boldSystemFontOfSize:25];
        self.tempLabel.text = @"0.00";
        self.tempLabel.textColor = [UIColor blackColor];
        [self addSubview:self.tempLabel];
        
        self.dateLabel = [[UILabel alloc]init];
        self.dateLabel.font = [UIFont systemFontOfSize:14];
        self.dateLabel.text = [ILUtility getCurrentDayStr];
        self.dateLabel.textColor = [UIColor grayColor];
        [self addSubview:self.dateLabel];
        
        self.timeLabel = [[UILabel alloc]init];
        self.timeLabel.font = [UIFont systemFontOfSize:10];
        self.timeLabel.text = @"00:00-00:00";
        self.timeLabel.textColor = [UIColor grayColor];
        [self addSubview:self.timeLabel];
    }
    return self;
}

- (void)layoutSubviews
{
    self.titleLabel.frame = CGRectMake(5,0,self.frame.size.width-10,20);
    self.tempLabel.frame = CGRectMake(5,20,self.frame.size.width-10,40);
    self.dateLabel.frame = CGRectMake(5,60,self.frame.size.width-10,20);
    self.timeLabel.frame = CGRectMake(5,80,self.frame.size.width-10,20);
}

@end


@interface ILChartView ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong) UICollectionView * collectionView;
@property (nonatomic,strong) NSMutableArray * dataArray;
@property (nonatomic,strong) NSMutableArray * allDateArray;
@property (nonatomic,strong) NSArray * cellClasss;
@property (nonatomic,strong) ILShowDataView * showView;
@property (nonatomic,strong) ILNoDataView * noDataView;
@property (nonatomic,strong) ILDeviceInfoModel * lastModel;
@property (nonatomic,assign) BOOL isQuery;
@property (nonatomic,assign) NSInteger scrollIndex;
@property (nonatomic,copy) NSString * startDay;
@end

@implementation ILChartView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.scrollIndex = 1;
        self.showView = [[ILShowDataView alloc]initWithFrame:CGRectMake(16,0,165,100)];
        [self addSubview:self.showView];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.sectionInset = UIEdgeInsetsZero;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        
        CGRect scrolframe = CGRectMake(0,100,frame.size.width,frame.size.height-100);
        self.collectionView = [[UICollectionView alloc] initWithFrame:scrolframe collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.pagingEnabled = YES;
        self.collectionView.alwaysBounceHorizontal = YES;
        self.collectionView.showsVerticalScrollIndicator = FALSE;
        self.collectionView.showsHorizontalScrollIndicator = FALSE;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self addSubview:self.collectionView];
        
        //self.noDataView.frame = scrolframe;
        
        for (int i = 0; i < self.cellClasss.count; i++) {
           Class class = self.cellClasss[i];
           [self.collectionView registerClass:class forCellWithReuseIdentifier:NSStringFromClass(class)];
        }
    }
    return self;
}

- (NSArray *)cellClasss
{
    if (!_cellClasss) {
        _cellClasss = @[[ILHourChartCollectionViewCell class],[ILDayChartCollectionViewCell class],
                        [ILWeekChartCollectionViewCell class],[ILMonthChartCollectionViewCell class],
                        [ILYearChartCollectionViewCell class]];
    }
    return _cellClasss;
}

- (NSMutableArray *)dataArray
{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)allDateArray
{
    if (!_allDateArray) {
        _allDateArray = [NSMutableArray array];
    }
    return _allDateArray;
}

- (ILNoDataView *)noDataView
{
    if (!_noDataView) {
         _noDataView = [[ILNoDataView alloc]init];
        [self addSubview:_noDataView];
    }
    return _noDataView;
}

- (void)queryAllDateArray
{
   [self.allDateArray removeAllObjects];
   NSString * queryStr1 = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@' ORDER BY time_stamp DESC LIMIT 0,1",self.userModel.userId,self.userModel.macAddr];
    NSArray * array1 = [ILDeviceInfoModel queryModelsWithSqlStr:queryStr1];
    ILDeviceInfoModel * firstModel = [array1 lastObject];
    NSString * queryStr2 = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@' ORDER BY time_stamp ASC LIMIT 0,1",self.userModel.userId,self.userModel.macAddr];
    NSArray * array2 = [ILDeviceInfoModel queryModelsWithSqlStr:queryStr2];
    ILDeviceInfoModel * lastModel = [array2 lastObject];
    if(!firstModel || !lastModel) return;
    NSInteger allDay = [lastModel.dateStr integerValue] - [firstModel.dateStr integerValue];
    for (int i = 0; i < allDay + 1; i++) {
        NSString * dateStr = [NSString stringWithFormat:@"%ld",(long)[firstModel.dateStr integerValue] + i*60*60*24];
        NSString * queryStr3 = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@' AND date_str = '%@' ORDER BY time_stamp ASC LIMIT 0,1",self.userModel.userId,self.userModel.macAddr,dateStr];
        NSArray * array3 = [ILDeviceInfoModel queryModelsWithSqlStr:queryStr3];
        if (array3.count > 0) {
            [self.allDateArray addObject:[array3 firstObject]];
        }
    }
    if (self.allDateArray.count > 0) {
        self.lastModel = [self.allDateArray lastObject];
    }
}


- (void)setType:(IL_DATA_TYPE)type
{
    _type = type;
    self.scrollIndex = 1;
    self.startDay = @"";
    self.isQuery  = NO;
    [self.dataArray removeAllObjects];
//    [self queryAllDateArray];
    
    if (_type == IL_HOUR_DATA_TYPE) {
        NSArray * array = [ILDeviceInfoModel queryOneDayAllMinuteDataWithUserId:self.userModel.userId
                                                                     andMacAddr:self.userModel.macAddr
                                                                     andDateStr:[ILUtility currentDateStr]];
        NSDateComponents * components = [ILUtility dateComponentsWithDateStr:[ILUtility currentHourStr]];
        array = [array subarrayWithRange:NSMakeRange(0,components.hour+1)];
        [self.dataArray addObject:array];
        [self.collectionView reloadData];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:components.hour inSection:0];
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:components.hour inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                            animated:NO];
        [self setShowViewIndexPath:indexPath];
    }else if (_type == IL_DAY_DATA_TYPE) {
        NSArray * array = [ILDeviceInfoModel queryOneDayAllHourDataWithUserId:self.userModel.userId
                                                                    andMacAddr:self.userModel.macAddr
                                                                    andDateStr:[ILUtility currentDateStr]];
        [self.dataArray addObject:array];
        [self.collectionView reloadData];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self setShowViewIndexPath:indexPath];
    }else if (_type == IL_WEEK_DATA_TYPE) {
        NSArray * array = [ILDeviceInfoModel queryOneWeekAllDayDataWithUserId:self.userModel.userId
                                                                    andMacAddr:self.userModel.macAddr
                                                                    andDateStr:[ILUtility currentDateStr]];
        [self.dataArray addObject:array];
        [self.collectionView reloadData];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self setShowViewIndexPath:indexPath];
    }else if (_type == IL_MONTH_DATA_TYPE) {
        NSDateComponents * components = [ILUtility currentComponents];
        self.startDay = [ILUtility get1970timeTempWithYear:components.year
                                                  andMonth:components.month];
        NSArray * array = [ILDeviceInfoModel queryOneMonthAllDayDataWithUserId:self.userModel.userId
                                                                    andMacAddr:self.userModel.macAddr
                                                                    andDateStr:self.startDay];
        [self.dataArray addObject:array];
        [self.collectionView reloadData];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self setShowViewIndexPath:indexPath];
    }else if (_type == IL_YEAR_DATA_TYPE) {
        NSDateComponents * components = [ILUtility currentComponents];
        self.startDay = [ILUtility get1970timeTempWithYear:components.year];
        NSArray * array = [ILDeviceInfoModel queryOneYearAllMonthDataWithUserId:self.userModel.userId
                                                                     andMacAddr:self.userModel.macAddr
                                                                     andDateStr:self.startDay];
        [self.dataArray addObject:array];
        [self.collectionView reloadData];
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self setShowViewIndexPath:indexPath];
    }
    /*
    if(self.dataArray.count == 0) {
        self.showView.hidden = YES;
        self.collectionView.hidden = YES;
        self.noDataView.hidden = NO;
    }else {
        self.showView.hidden = NO;
        self.collectionView.hidden = NO;
        self.noDataView.hidden = YES;
    }*/
}

- (NSDictionary *)avgOneHourTempWithArray:(NSArray *)array
{
    CGFloat totalTemp = 0.0;
    NSInteger count = 0;
    for (int i = 0; i < array.count; i++) {
        ILDeviceInfoModel * model = array[i];
        if ([model.temperature1 floatValue] > 0) {
            totalTemp += [model.temperature1 floatValue];
            count ++;
        }
    }
    ILDeviceInfoModel * lastModel = [array firstObject];
    NSDateComponents * comp = [ILUtility dateComponentsWithDateStr:lastModel.timeStamp];
    CGFloat avgTemp = totalTemp == 0 ? 0 : (totalTemp/count);
    NSString * avgStr  = [NSString stringWithFormat:@"%.2f",avgTemp];
    NSString * timeStr = [NSString stringWithFormat:@"%02ld:00-%02ld:59",(long)comp.hour,(long)comp.hour];
    NSString * dateStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)comp.year,(long)comp.month,(long)comp.day];
    NSDictionary * dic = @{@"title":@"平均温度",@"temp":avgStr,@"time":timeStr,@"date":dateStr};
    return dic;
}

- (NSDictionary *)avgOneDayTempWithArray:(NSArray *)array
{
    CGFloat totalTemp = 0.0;
    NSInteger count = 0;
    ILDeviceInfoModel * lastModel = nil;
    for (int i = 0; i < array.count; i++) {
        NSArray * array1 = array[i];
        for (int j = 0; j < array1.count; j++) {
            lastModel = array1[j];
            if ([lastModel.temperature1 floatValue] > 0) {
                totalTemp += [lastModel.temperature1 floatValue];
                count ++;
            }
        }
    }
    NSDateComponents * comp = [ILUtility dateComponentsWithDateStr:lastModel.timeStamp];
    CGFloat avgTemp = totalTemp == 0 ? 0 : (totalTemp/count);
    NSString * avgStr  = [NSString stringWithFormat:@"%.2f",avgTemp];
    NSString * timeStr = @"";
    NSString * dateStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)comp.year,(long)comp.month,(long)comp.day];
    NSDictionary * dic = @{@"title":@"平均温度",@"temp":avgStr,@"time":timeStr,@"date":dateStr};
    return dic;
}

- (NSDictionary *)avgOneWeekTempWithArray:(NSArray *)array
{
    CGFloat totalTemp = 0.0;
    NSInteger count = 0;
    ILDeviceInfoModel * lastModel = nil;
    ILDeviceInfoModel * firstModel = [[array firstObject] firstObject];
    for (int i = 0; i < array.count; i++) {
        NSArray * array1 = array[i];
        for (int j = 0; j < array1.count; j++) {
            lastModel = array1[j];
            if ([lastModel.temperature1 floatValue] > 0) {
                totalTemp += [lastModel.temperature1 floatValue];
                count ++;
            }
        }
    }
    NSDateComponents * comp1 = [ILUtility dateComponentsWithDateStr:firstModel.timeStamp];
    NSDateComponents * comp2 = [ILUtility dateComponentsWithDateStr:lastModel.timeStamp];
    CGFloat avgTemp = totalTemp == 0 ? 0 : (totalTemp/count);
    NSString * avgStr  = [NSString stringWithFormat:@"%.2f",avgTemp];
    NSString * timeStr = @"";
    NSString * dateStr = [NSString stringWithFormat:@"%04ld日%ld月%ld日至%ld日",(long)comp1.year,(long)comp1.month,(long)comp1.day,(long)comp2.day];
    if (comp2.month != comp1.month) {
        dateStr = [NSString stringWithFormat:@"%04ld年%ld月%ld日至%ld月%ld日",(long)comp1.year,(long)comp1.month,
                   (long)comp1.day,(long)comp2.month,(long)comp2.day];
    }
    NSDictionary * dic = @{@"title":@"平均温度",@"temp":avgStr,@"time":timeStr,@"date":dateStr};
    return dic;
}

- (NSDictionary *)avgOneMonthTempWithArray:(NSArray *)array
{
    CGFloat totalTemp = 0.0;
    NSInteger count = 0;
    ILDeviceInfoModel * firstModel = [[array firstObject] firstObject];
    for (int i = 0; i < array.count; i++) {
        NSArray * array1 = array[i];
        for (int j = 0; j < array1.count; j++) {
            ILDeviceInfoModel * model = array1[j];
            if ([model.temperature1 floatValue] > 0) {
                totalTemp += [model.temperature1 floatValue];
                count ++;
            }
        }
    }
    NSDateComponents * comp1 = [ILUtility dateComponentsWithDateStr:firstModel.timeStamp];
    CGFloat avgTemp = totalTemp == 0 ? 0 : (totalTemp/count);
    NSString * avgStr  = [NSString stringWithFormat:@"%.2f",avgTemp];
    NSString * timeStr = @"";
    NSString * dateStr = [NSString stringWithFormat:@"%04ld年%ld月",(long)comp1.year,(long)comp1.month];
    NSDictionary * dic = @{@"title":@"平均温度",@"temp":avgStr,@"time":timeStr,@"date":dateStr};
    return dic;
}

- (NSDictionary *)avgOneYearTempWithArray:(NSArray *)array
{
    CGFloat totalTemp = 0.0;
    NSInteger count = 0;
    ILDeviceInfoModel * firstModel = [[array firstObject] firstObject];
    for (int i = 0; i < array.count; i++) {
        NSArray * array1 = array[i];
        for (int j = 0; j < array1.count; j++) {
            ILDeviceInfoModel * model = array1[j];
            if ([model.temperature1 floatValue] > 0) {
                totalTemp += [model.temperature1 floatValue];
                count ++;
            }
        }
    }
    NSDateComponents * comp1 = [ILUtility dateComponentsWithDateStr:firstModel.timeStamp];
    CGFloat avgTemp = totalTemp == 0 ? 0 : (totalTemp/count);
    NSString * avgStr  = [NSString stringWithFormat:@"%.2f",avgTemp];
    NSString * timeStr = @"";
    NSString * dateStr = [NSString stringWithFormat:@"%04ld年",(long)comp1.year];
    NSDictionary * dic = @{@"title":@"平均温度",@"temp":avgStr,@"time":timeStr,@"date":dateStr};
    return dic;
}



- (void)scrollHourChartCollectionView
{
    if (self.isQuery) return;
    self.isQuery = YES;
    NSString * dayStr = [NSString stringWithFormat:@"%ld",(long)[[ILUtility currentDateStr] integerValue] - self.scrollIndex*60*60*24];
    NSArray * array = [ILDeviceInfoModel queryOneDayAllMinuteDataWithUserId:self.userModel.userId
                                                                 andMacAddr:self.userModel.macAddr
                                                                 andDateStr:dayStr];
    [self.dataArray insertObject:array atIndex:0];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:NO];
    self.scrollIndex ++;
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.isQuery = NO;
    });
}

- (void)scrollDayChartCollectionView
{
    if (self.isQuery) return;
    self.isQuery = YES;
    NSInteger dayCount = [[ILUtility currentDateStr] integerValue] - self.scrollIndex*60*60*24;
    NSArray * array = [ILDeviceInfoModel queryOneDayAllHourDataWithUserId:self.userModel.userId
                                                                andMacAddr:self.userModel.macAddr
                                                                andDateStr:[NSString stringWithFormat:@"%ld",(long)dayCount]];
    [self.dataArray insertObject:array atIndex:0];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:NO];
    self.scrollIndex ++;
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.isQuery = NO;
    });
}

- (void)scrollWeekChartCollectionView
{
    if (self.isQuery) return;
    self.isQuery = YES;
    NSInteger dayCount = [[ILUtility currentDateStr] integerValue] - self.scrollIndex*7*60*60*24;
    NSArray * array =  [ILDeviceInfoModel queryOneWeekAllDayDataWithUserId:self.userModel.userId
                                                                andMacAddr:self.userModel.macAddr
                                                                andDateStr:[NSString stringWithFormat:@"%ld",(long)dayCount]];
    [self.dataArray insertObject:array atIndex:0];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:NO];
    self.scrollIndex ++;
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.isQuery = NO;
    });
}

- (void)scrollMonthChartCollectionView
{
    if (self.isQuery) return;
    self.isQuery = YES;
    NSInteger dayCount = [self.startDay integerValue] - self.scrollIndex*60*60*24;
    NSDateComponents * components = [ILUtility dateComponentsWithDateStr:[NSString stringWithFormat:@"%ld",(long)dayCount]];
    self.startDay = [ILUtility get1970timeTempWithYear:components.year andMonth:components.month];
    NSArray * array =  [ILDeviceInfoModel queryOneMonthAllDayDataWithUserId:self.userModel.userId
                                                                 andMacAddr:self.userModel.macAddr
                                                                 andDateStr:self.startDay];
    [self.dataArray insertObject:array atIndex:0];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:NO];
    self.scrollIndex ++;
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.isQuery = NO;
    });
}

- (void)scrollYearChartCollectionView
{
    if (self.isQuery) return;
    self.isQuery = YES;
    NSInteger dayCount = [self.startDay integerValue] - self.scrollIndex*60*60*24;
    NSDateComponents * components = [ILUtility dateComponentsWithDateStr:[NSString stringWithFormat:@"%ld",(long)dayCount]];
    self.startDay = [ILUtility get1970timeTempWithYear:components.year];
    NSArray * array =  [ILDeviceInfoModel queryOneYearAllMonthDataWithUserId:self.userModel.userId
                                                                  andMacAddr:self.userModel.macAddr
                                                                  andDateStr:self.startDay];
    [self.dataArray insertObject:array atIndex:0];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                        animated:NO];
    self.scrollIndex ++;
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.isQuery = NO;
    });
}

- (void)setShowViewIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * dic = [NSDictionary dictionary];
    if (_type == IL_HOUR_DATA_TYPE) {
        NSArray * array = self.dataArray[indexPath.section][indexPath.row];
        dic = [self avgOneHourTempWithArray:array];
    }else if (_type == IL_DAY_DATA_TYPE) {
        NSArray * array = self.dataArray[indexPath.row];
        dic = [self avgOneDayTempWithArray:array];
    }else if (_type == IL_WEEK_DATA_TYPE) {
        NSArray * array = self.dataArray[indexPath.row];
        dic = [self avgOneWeekTempWithArray:array];
    }else if (_type == IL_MONTH_DATA_TYPE) {
        NSArray * array = self.dataArray[indexPath.row];
        dic = [self avgOneMonthTempWithArray:array];
    }else if (_type == IL_YEAR_DATA_TYPE) {
        NSArray * array = self.dataArray[indexPath.row];
        dic = [self avgOneYearTempWithArray:array];
    }
    CGFloat width = [ILUtility getWidthWithText:dic[@"date"]
                                         height:20
                                       fontSize:[UIFont systemFontOfSize:14]] + 10.0f;
    width = width >= 165 ? width : 165;
    self.showView.frame = CGRectMake(16,0,width,100);
    self.showView.titleLabel.text = dic[@"title"];
    self.showView.tempLabel.text = dic[@"temp"];
    self.showView.dateLabel.text = dic[@"date"];
    self.showView.timeLabel.text = dic[@"time"];
}

#pragma mark ====== scrollviewDelegate ======
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x < 0) { //请求前一天的数据
        if (_type == IL_HOUR_DATA_TYPE) {
            [self scrollHourChartCollectionView];
        }else if (_type == IL_DAY_DATA_TYPE) {
            [self scrollDayChartCollectionView];
        }else if (_type == IL_WEEK_DATA_TYPE) {
            [self scrollWeekChartCollectionView];
        }else if (_type == IL_MONTH_DATA_TYPE) {
            [self scrollMonthChartCollectionView];
        }else if (_type == IL_YEAR_DATA_TYPE) {
            [self scrollYearChartCollectionView];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSIndexPath * indexPath = [self.collectionView indexPathForItemAtPoint:scrollView.contentOffset];
    [self setShowViewIndexPath:indexPath];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.showView.center = CGPointMake(self.showView.frame.size.width/2 + 16,self.showView.center.y);
    self.showView.backgroundColor = [UIColor whiteColor];
    for (UITableViewCell * cell in [self.collectionView visibleCells]) {
        if ([cell isKindOfClass:[ILHourChartCollectionViewCell class]]) {
            ILHourChartLayerModel * model = [(ILHourChartCollectionViewCell *)cell chartView].chartLayerModel;
            model.isShow = NO;
            [[(ILHourChartCollectionViewCell *)cell chartView].layer setNeedsDisplay];
        }else if ([cell isKindOfClass:[ILDayChartCollectionViewCell class]]) {
            ILDayChartLayerModel * model = [(ILDayChartCollectionViewCell *)cell chartView].chartLayerModel;
            model.isShow = NO;
            [[(ILDayChartCollectionViewCell *)cell chartView].layer setNeedsDisplay];
        }else if ([cell isKindOfClass:[ILWeekChartCollectionViewCell class]]) {
            ILWeekChartLayerModel * model = [(ILWeekChartCollectionViewCell *)cell chartView].chartLayerModel;
            model.isShow = NO;
            [[(ILWeekChartCollectionViewCell *)cell chartView].layer setNeedsDisplay];
        }else if ([cell isKindOfClass:[ILMonthChartCollectionViewCell class]]) {
            ILMonthChartLayerModel * model = [(ILMonthChartCollectionViewCell *)cell chartView].chartLayerModel;
            model.isShow = NO;
            [[(ILMonthChartCollectionViewCell *)cell chartView].layer setNeedsDisplay];
        }else if ([cell isKindOfClass:[ILYearChartCollectionViewCell class]]) {
            ILYearChartLayerModel * model = [(ILYearChartCollectionViewCell *)cell chartView].chartLayerModel;
            model.isShow = NO;
            [[(ILYearChartCollectionViewCell *)cell chartView].layer setNeedsDisplay];
        }
    }
}

#pragma mark ====== collectionviewDelegate ======

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView
                                   cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (_type == IL_HOUR_DATA_TYPE) {
        static NSString * cellId = @"ILHourChartCollectionViewCell";
        NSArray * sectionArray = self.dataArray[indexPath.section];
        NSArray * array = sectionArray[indexPath.row];
        ILHourChartCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
        ILHourChartLayerModel * model = [[ILHourChartLayerModel alloc]initWithModels:array];
        cell.chartView.chartLayerModel = model;
        __weak typeof(self) weakSelf = self;
        cell.chartView.selectOneData = ^(NSDictionary *dic,BOOL isEnd) {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf scrollShowViewWithDic:dic isEnd:isEnd];
        };
        return cell;
    }else if(_type == IL_DAY_DATA_TYPE) {
        static NSString * cellId = @"ILDayChartCollectionViewCell";
        ILDayChartCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
        NSArray * array = self.dataArray[indexPath.row];
        ILDayChartLayerModel * model = [[ILDayChartLayerModel alloc]initWithModels:array];
        cell.chartView.chartLayerModel = model;
        __weak typeof(self) weakSelf = self;
        cell.chartView.selectOneData = ^(NSDictionary *dic,BOOL isEnd) {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf scrollShowViewWithDic:dic isEnd:isEnd];
        };
        return cell;
    }else if(_type == IL_WEEK_DATA_TYPE) {
        static NSString * cellId = @"ILWeekChartCollectionViewCell";
        ILWeekChartCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
        NSArray * array = self.dataArray[indexPath.row];
        ILWeekChartLayerModel * model = [[ILWeekChartLayerModel alloc]initWithModels:array];
        cell.chartView.chartLayerModel = model;
        __weak typeof(self) weakSelf = self;
        cell.chartView.selectOneData = ^(NSDictionary *dic,BOOL isEnd) {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf scrollShowViewWithDic:dic isEnd:isEnd];
        };
        return cell;
    }else if(_type == IL_MONTH_DATA_TYPE) {
         static NSString * cellId = @"ILMonthChartCollectionViewCell";
         ILMonthChartCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
         NSArray * array = self.dataArray[indexPath.row];
         ILMonthChartLayerModel * model = [[ILMonthChartLayerModel alloc]initWithModels:array];
        cell.chartView.chartLayerModel = model;
        __weak typeof(self) weakSelf = self;
        cell.chartView.selectOneData = ^(NSDictionary *dic,BOOL isEnd) {
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf scrollShowViewWithDic:dic isEnd:isEnd];
        };
         return cell;
    }else if(_type == IL_YEAR_DATA_TYPE){
        static NSString * cellId = @"ILYearChartCollectionViewCell";
        ILYearChartCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellId forIndexPath:indexPath];
        NSArray * array = self.dataArray[indexPath.row];
        ILYearChartLayerModel * model = [[ILYearChartLayerModel alloc]initWithModels:array];
       cell.chartView.chartLayerModel = model;
       __weak typeof(self) weakSelf = self;
       cell.chartView.selectOneData = ^(NSDictionary *dic,BOOL isEnd) {
           __strong typeof(self) strongSelf = weakSelf;
           [strongSelf scrollShowViewWithDic:dic isEnd:isEnd];
       };
        return cell;
    }
    return nil;
}

- (void)scrollShowViewWithDic:(NSDictionary *)dic isEnd:(BOOL)isEnd
{
    self.collectionView.scrollEnabled = isEnd;
    CGRect frame = CGRectFromString([dic valueForKey:@"frame"]);
    self.showView.backgroundColor = [UIColor colorWithRed:240/255.0f green:240/255.0f blue:240/255.0f alpha:1.0];
    if (frame.origin.x - self.showView.frame.size.width/2 < 16) {
        CGFloat x = self.showView.frame.size.width/2 + 16;
        self.showView.center = CGPointMake(x,self.showView.center.y);
    }else if (frame.origin.x + self.showView.frame.size.width/2 > self.collectionView.frame.size.width - 40) {
        CGFloat x = self.collectionView.frame.size.width - 40 - self.showView.frame.size.width/2;
        self.showView.center = CGPointMake(x,self.showView.center.y);
    }else {
        self.showView.center = CGPointMake(frame.origin.x,self.showView.center.y);
    }
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if (_type == IL_HOUR_DATA_TYPE) {
        NSArray * items = [self.dataArray objectAtIndex:section];
        return items.count;
    }else {
        return [self.dataArray count];
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (_type == IL_HOUR_DATA_TYPE) {
        return [self.dataArray count];
    }else {
        return 1;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"click cell");
    
}

@end
