//
//  ILMonthChartView.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILMonthChartLayerModel.h"

@interface ILMonthChartView : UIView
@property (nonatomic,strong) ILMonthChartLayerModel * chartLayerModel;
@property (nonatomic,copy) void(^selectOneData)(NSDictionary * dic,BOOL isEnd);
@end

