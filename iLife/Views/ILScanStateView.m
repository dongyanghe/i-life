//
//  ILScanStateView.m
//  iLife
//
//  Created by hedongyang on 2020/7/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILScanStateView.h"

@implementation ILScanStateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubviews];
    }
    return self;
}

-(void)addSubviews {
    
    self.stateIcon = [UIImageView new];
    [self addSubview:_stateIcon];
    [_stateIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self.mas_centerY).offset(-80);
        make.height.width.mas_equalTo(145);
    }];
    
    self.labelStatus = [[UILabel alloc]init];
    _labelStatus.font = [UIFont systemFontOfSize:24];
    _labelStatus.textColor = UIColorWithRGBA(56,56,56,1);
    _labelStatus.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_labelStatus];
    [_labelStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_stateIcon.mas_bottom).offset(22.5);
        make.height.mas_equalTo(22.5);
    }];
    
    self.labelTips = [[UILabel alloc]init];
    _labelTips.font = [UIFont systemFontOfSize:17];
    _labelTips.textColor = UIColorWithRGBA(136,136,136,1);
    _labelTips.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_labelTips];
    [_labelTips mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_labelStatus);
        make.top.equalTo(_labelStatus.mas_bottom).offset(18);
        make.height.mas_equalTo(17);
    }];
}

@end
