//
//  ILHourChartView.h
//  iLife
//
//  Created by hedongyang on 2020/7/3.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILHourChartLayerModel.h"

@interface ILHourChartView : UIView
@property (nonatomic,strong) ILHourChartLayerModel * chartLayerModel;
@property (nonatomic,copy) void(^selectOneData)(NSDictionary * dic,BOOL isEnd);
@end

