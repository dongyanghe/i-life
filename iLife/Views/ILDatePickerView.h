//
//  ILDatePickerView.h
//
//  Created by 陈君祁 on 2020/4/22.
//  Copyright © 2020 陈君祁. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DateBlock)(NSString *dateStr);

@interface ILDatePickerView : UIView

@property (nonatomic,copy) DateBlock dateCallback;

@property (nonatomic,copy) NSString * dateStr;

- (void)show;

- (void)back;

@end

