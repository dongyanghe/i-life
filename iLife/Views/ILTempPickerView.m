//
//  ILTempPickerView.m
//  iLife
//
//  Created by hedongyang on 2020/8/1.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILTempPickerView.h"

@interface ILTempPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,strong) UIPickerView *pickerView;
@property (nonatomic,strong) NSArray *lagerArray;
@property (nonatomic,strong) NSArray *smallArray;
@end

@implementation ILTempPickerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.lagerArray = @[@"34",@"35",@"36",@"37",@"38",@"39",@"40",@"41"];
        self.smallArray = @[@"00",@"10",@"20",@"30",@"40",@"50",@"60",@"70",@"80",@"90"];
        [self addSubviews];
        [self.pickerView reloadAllComponents];
        self.hidden = YES;
    }
    return self;
}

-(void)addSubviews {
    
    UIButton * bgButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [bgButton setBackgroundColor:UIColorWithRGBA(0, 0, 0, 0.3)];
    [self addSubview:bgButton];
    
    [bgButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    UIView * contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.mas_equalTo(300);
    }];
    
    UIView * headerView = [UIView new];
    headerView.backgroundColor = MainColor;
    [contentView addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(contentView);
        make.height.mas_equalTo(44);
    }];
    
    UIButton *cancelButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    cancelButton.tintColor = [UIColor whiteColor];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [cancelButton addTarget:self action:@selector(back) forControlEvents:(UIControlEventTouchUpInside)];
    [headerView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(headerView);
        make.left.equalTo(headerView).offset(20);
        make.width.mas_equalTo(60);
    }];
    
    UIButton *confirmButton = [UIButton buttonWithType:(UIButtonTypeCustom)];
    confirmButton.tintColor = [UIColor whiteColor];
    [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:(UIControlEventTouchUpInside)];
    [headerView addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(headerView);
        make.right.equalTo(headerView.mas_right).offset(-20);
        make.width.mas_equalTo(60);
    }];
    
    [contentView addSubview:self.pickerView];
    [_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(contentView);
        make.top.equalTo(headerView.mas_bottom);
    }];
}

- (void)back
{
    self.hidden = YES;
}

- (void)confirm
{
    NSInteger lagerInt = [self.pickerView selectedRowInComponent:0];
    NSInteger smallInt = [self.pickerView selectedRowInComponent:2];
    NSString * resStr = [NSString stringWithFormat:@"%@.%@",_lagerArray[lagerInt],_smallArray[smallInt]];
    if (self.tempCallback) {
        self.tempCallback(resStr);
    }
    self.hidden = YES;
}

- (void)show
{
    self.hidden = NO;
}

- (void)setTempStr:(NSString *)tempStr
{
    _tempStr = tempStr;
    NSRange range = [_tempStr rangeOfString:@"."];
    if (range.location == NSNotFound) {
        return;
    }
    NSString * lagerStr = [_tempStr substringToIndex:range.location];
    NSString * smallStr = [_tempStr substringFromIndex:range.location + 1];
    NSInteger row1 = [_lagerArray indexOfObject:lagerStr];
    NSInteger row2 = [_smallArray indexOfObject:smallStr];
    [self.pickerView selectRow:row1 inComponent:0 animated:YES];
    [self.pickerView selectRow:row2 inComponent:2 animated:YES];
}

-(UIPickerView *)pickerView {
    if (_pickerView == nil) {
        self.pickerView = [[UIPickerView alloc] init];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
    }
    
    return _pickerView;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return _lagerArray.count;
    }else if (component == 1) {
        return 1;
    }else{
        return _smallArray.count;
    }
    
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView
                      titleForRow:(NSInteger)row
                     forComponent:(NSInteger)component {
    if (component==0) {
        return _lagerArray[row];
    }else if (component == 1) {
        return @".";
    }else{
        return _smallArray[row];
    }
}


@end
