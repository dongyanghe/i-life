//
//  ILTempPickerView.h
//  iLife
//
//  Created by hedongyang on 2020/8/1.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^tempBlock)(NSString *tempStr);

@interface ILTempPickerView : UIView
@property (nonatomic,copy) tempBlock tempCallback;
@property (nonatomic,copy) NSString * tempStr;
- (void)show;
@end

