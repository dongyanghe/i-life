//
//  ILChartView.h
//  iLife
//
//  Created by hedongyang on 2020/7/8.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILUserInfoModel.h"

typedef NS_ENUM(NSInteger, IL_DATA_TYPE) {
    
    IL_HOUR_DATA_TYPE = 0,
    
    IL_DAY_DATA_TYPE = 1,
    
    IL_WEEK_DATA_TYPE,
    
    IL_MONTH_DATA_TYPE,
    
    IL_YEAR_DATA_TYPE,
};

@interface ILChartView : UIView
@property (nonatomic,assign) IL_DATA_TYPE type;
@property (nonatomic,strong) ILUserInfoModel * userModel;
@end

