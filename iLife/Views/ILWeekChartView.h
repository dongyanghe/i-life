//
//  ILWeekChartView.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILWeekChartLayerModel.h"

@interface ILWeekChartView : UIView
@property (nonatomic,strong) ILWeekChartLayerModel * chartLayerModel;
@property (nonatomic,copy) void(^selectOneData)(NSDictionary * dic,BOOL isEnd);
@end

