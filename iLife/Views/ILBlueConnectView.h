//
//  ILBlueConnectView.h
//  iLife
//
//  Created by hedongyang on 2020/7/22.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILGlobalEngineModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ILBlueConnectView : UIView
@property (nonatomic,strong) ILGlobalEngineModel * engineModel;
@property (nonatomic,strong) UIButton * addButton;
@end

NS_ASSUME_NONNULL_END
