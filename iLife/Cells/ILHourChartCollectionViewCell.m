//
//  ILHourChartCollectionViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/8/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILHourChartCollectionViewCell.h"

@implementation ILHourChartCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self.chartView = [[ILHourChartView alloc]initWithFrame:self.bounds];
        [self addSubview:self.chartView];
    }
    return self;
}

@end
