//
//  ILSetDeviceTableViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILSetDeviceTableViewCell.h"

@implementation ILSetDeviceTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initViews];
    }
    return self;
}

- (void)initViews
{
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@16);
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIView * line  = [[UIView alloc]init];
    line.backgroundColor = LigntGrayColor;
    [self addSubview:line];
    CGFloat line_h = 1.0/[UIScreen mainScreen].scale;
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.height.equalTo(@(line_h));
    }];
}

@end


@implementation ILSetDeviceLabelTableViewCell

- (void)initViews
{
    [super initViews];
    self.subLabel = [[UILabel alloc]init];
    self.subLabel.font = [UIFont systemFontOfSize:14];
    self.subLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.subLabel];
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-16));
        make.centerY.equalTo(self.mas_centerY);
    }];
}

@end


@implementation ILSetDeviceArrowTableViewCell

- (void)initViews
{
    [super initViews];
    
    self.iconView = [[UIImageView alloc]init];
    self.iconView.image = [UIImage imageNamed:@"right_arrow"];
    self.iconView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.iconView];
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-16));
        make.centerY.equalTo(self.mas_centerY);
        make.width.equalTo(@25);
        make.height.equalTo(@25);
    }];
    
    self.subLabel = [[UILabel alloc]init];
    self.subLabel.font = [UIFont systemFontOfSize:14];
    self.subLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.subLabel];
    [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.iconView.mas_left).offset(-5);
        make.centerY.equalTo(self.mas_centerY);
    }];
}

@end

@implementation ILSetDeviceSwitchTableViewCell

- (void)initViews
{
    [super initViews];
    self.switchOnOff = [[UISwitch alloc]init];
    [self addSubview:self.switchOnOff];
    [self.switchOnOff mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(@(-16));
        make.centerY.equalTo(self.mas_centerY);
    }];
}

@end
