//
//  ILYearChartCollectionViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILYearChartView.h"

@interface ILYearChartCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) ILYearChartView * chartView;
@end

