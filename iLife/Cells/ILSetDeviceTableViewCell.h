//
//  ILSetDeviceTableViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ILSetDeviceTableViewCell : UITableViewCell
@property (nonatomic,strong) UILabel * titleLabel;
- (void)initViews;
@end

@interface ILSetDeviceLabelTableViewCell : ILSetDeviceTableViewCell
@property (nonatomic,strong) UILabel * subLabel;
@end

@interface ILSetDeviceArrowTableViewCell : ILSetDeviceTableViewCell
@property (nonatomic,strong) UILabel * subLabel;
@property (nonatomic,strong) UIImageView * iconView;
@end

@interface ILSetDeviceSwitchTableViewCell : ILSetDeviceTableViewCell
@property (nonatomic,strong) UISwitch * switchOnOff;
@end
