//
//  ILHourChartCollectionViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/8/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILHourChartView.h"

@interface ILHourChartCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) ILHourChartView * chartView;
@end

