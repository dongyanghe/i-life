//
//  ILWeekChartCollectionViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILWeekChartView.h"

@interface ILWeekChartCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) ILWeekChartView * chartView;
@end

