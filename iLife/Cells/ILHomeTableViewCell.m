//
//  ILHomeTableViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/7/15.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILHomeTableViewCell.h"

@implementation ILHomeTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self addSubviews];
    }
    return self;
}

-(void)addSubviews {
    UIView * contentView = [UIView new];
    contentView.layer.shadowOpacity = 0.63;// 阴影透明度
    contentView.layer.shadowColor = UIColorWithRGBA(205, 205, 205, 1).CGColor;// 阴影的颜色
    contentView.layer.shadowRadius = 3;// 阴影扩散的范围控制
    contentView.layer.cornerRadius = 15;
    contentView.layer.shadowOffset = CGSizeMake(1, 1);// 阴影的范围
    contentView.backgroundColor = UIColorWithRGBA(254,254,254,1);
    self.viewContent = contentView;
    [self.contentView addSubview:_viewContent];
    __weak typeof(self) weakSelf = self;
    [self.viewContent mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.centerX.equalTo(strongSelf);
        make.top.equalTo(strongSelf).offset(8);
        make.bottom.equalTo(strongSelf).offset(-8);
        make.left.equalTo(strongSelf).offset(10);
    }];
    
    self.nameLable = [[UILabel alloc]init];
    self.nameLable.text = @"hedongyang";
    self.nameLable.font = [UIFont systemFontOfSize:19];
    self.nameLable.textColor = BlackColor;
    [self.viewContent addSubview:self.nameLable];
    [self.nameLable mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.viewContent).offset(15);
        make.top.equalTo(strongSelf.viewContent).offset(10);
    }];
    
    UIImageView * imageViewSex = [UIImageView new];
    imageViewSex.image = [UIImage imageNamed:@"man"];
    [self.viewContent addSubview:imageViewSex];
    self.imageViewSex = imageViewSex;
    [self.imageViewSex mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.nameLable.mas_right).offset(13);
        make.centerY.equalTo(strongSelf.nameLable);
        make.size.mas_equalTo(CGSizeMake(18,18));
    }];
    
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
    [self.viewContent addSubview:self.editButton];
    
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.right.equalTo(strongSelf.viewContent.mas_right).offset(-20);
        make.centerY.equalTo(strongSelf.nameLable);
        make.size.mas_equalTo(CGSizeMake(23,23));
    }];
    
    self.birthLable = [[UILabel alloc]init];
    self.birthLable.font = [UIFont systemFontOfSize:17];
    self.birthLable.textColor = UIColorWithRGBA(147,147,147,1);
    self.birthLable.text = @"2020-04-01";
    [self.viewContent addSubview:self.birthLable];
    [self.birthLable mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.nameLable);
        make.top.equalTo(strongSelf.nameLable.mas_bottom).offset(2);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(_viewContent);
    }];
    
    self.rebindButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rebindButton setTitle:@"去连接" forState:UIControlStateNormal];
    self.rebindButton.backgroundColor = [UIColor whiteColor];
    self.rebindButton.tintColor = MainColor;
    self.rebindButton.layer.borderWidth = 1.0f;
    self.rebindButton.layer.cornerRadius = 5.0f;
    [self.rebindButton setTitleColor:MainColor forState:(UIControlStateNormal)];
    self.rebindButton.layer.borderColor = [MainColor CGColor];
    [self.viewContent addSubview:self.rebindButton];
    
    [self.rebindButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.birthLable);
        make.bottom.equalTo(strongSelf.viewContent).offset(-10);
        make.size.mas_equalTo(CGSizeMake(114,40));
    }];
    
    self.readButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.readButton setTitle:@"查看体温" forState:UIControlStateNormal];
    self.readButton.layer.masksToBounds = YES;
    self.readButton.layer.cornerRadius = 5.0f;
    self.readButton.backgroundColor = MainColor;
    self.readButton.tintColor = [UIColor whiteColor];
    [self.viewContent addSubview:self.readButton];
    
    [self.readButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.size.bottom.equalTo(strongSelf.rebindButton);
        make.left.equalTo(strongSelf.rebindButton.mas_right).offset(10);
    }];
}

@end
