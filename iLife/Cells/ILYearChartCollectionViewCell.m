//
//  ILYearChartCollectionViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILYearChartCollectionViewCell.h"

@implementation ILYearChartCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self.chartView = [[ILYearChartView alloc]initWithFrame:self.bounds];
        [self addSubview:self.chartView];
    }
    return self;
}


@end
