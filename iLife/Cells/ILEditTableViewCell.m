//
//  ILEditTableViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILEditTableViewCell.h"

@implementation ILEditTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.title = [[UILabel alloc]init];
        self.title.font = [UIFont fontWithName:@"PingFang-SC-Regular" size:17];
        self.title.textColor = BlackColor;
        [self.contentView addSubview:self.title];
        [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.mas_centerY);
            make.left.equalTo(@16);
        }];
        
        self.textField = [[UITextField alloc]init];
        self.textField.font = [UIFont fontWithName:@"PingFang-SC-Medium" size:17];
        self.textField.textAlignment = NSTextAlignmentRight;
        self.textField.textColor = MainColor;
        self.textField.placeholder = @"";
        [self.contentView addSubview:self.textField];
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_centerX);
            make.right.equalTo(self.mas_right).offset(-16);
            make.centerY.equalTo(self.mas_centerY);
        }];
        
        UIView * line  = [[UIView alloc]init];
        line.backgroundColor = LigntGrayColor;
        [self.contentView addSubview:line];
        CGFloat line_h = 1.0/[UIScreen mainScreen].scale;
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.mas_bottom);
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.height.equalTo(@(line_h));
        }];
    }
    return self;
}

@end
