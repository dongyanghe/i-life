//
//  ILMonthChartCollectionViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILMonthChartView.h"

@interface ILMonthChartCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) ILMonthChartView * chartView;
@end

