//
//  ILHomeTableViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/7/15.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ILHomeTableViewCell : UITableViewCell
@property (nonatomic,strong) UIView * viewContent;
@property (nonatomic,strong) UILabel * nameLable;
@property (nonatomic,strong) UIImageView * imageViewSex;
@property (nonatomic,strong) UILabel * birthLable;
@property (nonatomic,strong) UIButton * editButton;
@property (nonatomic,strong) UIButton * rebindButton;
@property (nonatomic,strong) UIButton * readButton;
@end

