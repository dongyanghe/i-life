//
//  ILDayChartCollectionViewCell.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILDayChartView.h"

@interface ILDayChartCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) ILDayChartView * chartView;
@end

