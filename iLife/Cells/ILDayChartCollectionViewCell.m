//
//  ILDayChartCollectionViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILDayChartCollectionViewCell.h"

@implementation ILDayChartCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
        self.chartView = [[ILDayChartView alloc]initWithFrame:self.bounds];
        [self addSubview:self.chartView];
    }
    return self;
}

@end
