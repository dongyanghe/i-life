//
//  ILButtonTableViewCell.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILButtonTableViewCell.h"

@implementation ILButtonTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
       [self.addButton setTitle:@"保存" forState:UIControlStateNormal];
       self.addButton.layer.masksToBounds = YES;
       self.addButton.layer.cornerRadius = 5.0f;
       self.addButton.backgroundColor = MainColor;
       self.addButton.tintColor = [UIColor whiteColor];
       [self.contentView addSubview: self.addButton];
        [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@(16));
            make.right.equalTo(@(-16));
            make.height.equalTo(@(40));
            make.centerY.equalTo(self.mas_centerY);
        }];
    }
    return self;
}

@end
