//
//  ILBlueManager.m
//  iLife
//
//  Created by hedongyang on 2020/7/8.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBlueManager.h"
#import "ILPeripheralModel.h"
#import "ILUtility.h"
#import "ILStructModel.h"
#import "ILDeviceInfoModel.h"
#import "ILUpdateManager.h"
#import "ILCommandManager.h"
#import "ILGlobalEngineModel.h"

#define CANCELFUNC(Object,Sel) [NSObject cancelPreviousPerformRequestsWithTarget:Object selector:Sel object:nil];
#define ADDFUNC(Object,Sel,Time) [Object performSelector:Sel withObject:nil afterDelay:Time];

@interface ILBlueManager ()<CBCentralManagerDelegate,CBPeripheralDelegate>
@property (nonatomic,strong) NSSortDescriptor   * rssiSortDesc;
@property (nonatomic,strong) NSPredicate        * rssiPredicate;
@property (nonatomic,strong) NSPredicate        * otaPredicate;
@property (nonatomic,strong) NSMutableArray     * devices;
@property (nonatomic,assign) BOOL isStop;
@property (nonatomic,assign) NSInteger timeCount;
@end

@implementation ILBlueManager

static ILBlueManager * blueManager = nil;

+(instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        blueManager = [[ILBlueManager alloc] init];
    });
    return blueManager;
}

- (CBCentralManager *)centralManager
{
    if (!_centralManager) {
        NSDictionary * options =  @{
                                   CBCentralManagerOptionShowPowerAlertKey   : @(YES),
                                   CBCentralManagerOptionRestoreIdentifierKey: @"ILBluetoothStrapRestoreIdentifier"
                                   };
         _centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                                queue:dispatch_get_main_queue()
                                                              options:options];
    }
    return _centralManager;
}

- (void)startScan
{
    if (self.centralManager.delegate != self) {
        self.centralManager.delegate  = self;
    }
    self.isStop = NO;
    [self addSysListConnectPeripherals];
    [self.centralManager scanForPeripheralsWithServices:[ILUtility getServiceUUIDs]
                                                options:@{CBCentralManagerScanOptionAllowDuplicatesKey:@YES}];
}

- (void)addSysListConnectPeripherals
{
    if (self.devices.count > 0) [self.devices removeAllObjects];
    if ([ILBlueManager isOta])return;
    NSArray<CBPeripheral *> * peripherals = [self.centralManager retrieveConnectedPeripheralsWithServices:[ILUtility getServiceUUIDs]];
    for (CBPeripheral * peripheral in peripherals) {
        NSString * uuidStr = peripheral.identifier.UUIDString;
        NSString * sqlStr = [NSString stringWithFormat:@"WHERE uuid_str = '%@'",uuidStr];
        ILUserInfoModel * userModel = (ILUserInfoModel *)[[ILUserInfoModel queryModelsWithSqlStr:sqlStr] firstObject];
        ILPeripheralModel * model = [[ILPeripheralModel alloc]init];
        model.peripheral = peripheral;
        model.name     = peripheral.name;
        model.uuidStr  = uuidStr;
        model.macAddr  = userModel ? userModel.macAddr : @"";
        model.rssi     = 0;
        model.distance = 0;
        [self.devices addObject:model];
    }
    
    if ([ILGlobalEngineModel shareInstance].isBind) {
        for (ILPeripheralModel * model in self.devices) {
            if (   [model.uuidStr isEqualToString:[ILGlobalEngineModel shareInstance].uuidStr]
                || [model.macAddr isEqualToString:[ILGlobalEngineModel shareInstance].macAddr]) {
                [ILBlueManager connectDeviceWithModel:model];
                [self addConnectTimeout];
                self.isStop = YES;
            }
        }
    }else {
       NSArray * allDevices = [self.devices sortedArrayUsingDescriptors:@[self.rssiSortDesc]];
       allDevices = [allDevices filteredArrayUsingPredicate:self.rssiPredicate];
       if (self.delegate && [self.delegate respondsToSelector:@selector(bluetoothManager:allDevices:)]) {
           [self.delegate bluetoothManager:self allDevices:allDevices];
       }
    }
}

//取消所有超时方法
- (void)cancelAllFuncs
{
    CANCELFUNC(self, @selector(connectTimeout));//取消连接超时
    CANCELFUNC(self, @selector(discoveryServiceTimeout));//取消发现服务超时
    CANCELFUNC(self, @selector(discoverCharacteristicsTimeout));//取消发现服务特征超时
}

- (void)stopScan {
    self.isStop = YES;
    [self cancelAllFuncs];
    if (@available(iOS 9.0, *)) {
        if (self.centralManager.isScanning) {
            [self.centralManager stopScan];
            [self setDelegateUpdateState:IL_MANAGER_STATE_SCAN_STOP];
        }
    }else {
        [self.centralManager stopScan];
        [self setDelegateUpdateState:IL_MANAGER_STATE_SCAN_STOP];
    }
}

- (NSMutableArray *)devices
{
    if (!_devices) {
        _devices = [NSMutableArray array];
    }
    return _devices;
}

- (NSSortDescriptor *)rssiSortDesc
{
    if (!_rssiSortDesc) {
        _rssiSortDesc = [NSSortDescriptor sortDescriptorWithKey:@"rssi" ascending:YES];
    }
    return _rssiSortDesc;
}

- (NSPredicate *)rssiPredicate
{
    if(!_rssiPredicate) {
        _rssiPredicate = [NSPredicate predicateWithFormat:@"rssi < %ld",100];
    }
    return _rssiPredicate;
}

+ (void)connectDeviceWithModel:(ILPeripheralModel *)model
{
    ILBlueManager * manager = [ILBlueManager shareInstance];
    manager.currentModel = model;
    [manager.centralManager connectPeripheral:model.peripheral options:nil];
    [manager addConnectTimeout];
}

+ (void)cancelCurrentPeripheralConnection
{
    if ([ILBlueManager isPoweredOff])return;
    CBPeripheral * peripheral = [ILBlueManager shareInstance].currentPeripheral;
    CBCentralManager * manager =  [ILBlueManager shareInstance].centralManager;
    if(peripheral)[manager cancelPeripheralConnection:peripheral];
}

+ (void)sendData:(const void *)data dataLength:(NSInteger)length
{
    if ([ILBlueManager isPoweredOff] || !data)return;
    CBPeripheral * peripheral = [ILBlueManager shareInstance].currentPeripheral;
    CBCharacteristic * characteristic =  [ILBlueManager shareInstance].writeCharacteristic;
    NSData * sendData = [[NSData alloc] initWithBytes:data length:length];
    [peripheral writeValue:sendData forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
}

+ (BOOL)isPoweredOff
{
    BOOL isOff = NO;
    if (@available(iOS 10.0, *)) {
        isOff  = [ILBlueManager shareInstance].centralManager.state == CBManagerStatePoweredOff;
    } else {
        isOff  = [ILBlueManager shareInstance].centralManager.state == CBCentralManagerStatePoweredOff;
    }
    return isOff;
}

+ (BOOL)isConnected
{
    if (![ILBlueManager shareInstance].currentPeripheral) {
        return NO;
    }
    BOOL connected =  [ILBlueManager shareInstance].currentPeripheral.state == CBPeripheralStateConnected;
    return connected;
}

+ (BOOL)isOta
{
    return [ILUtility isOtaModeWithPeripheral:[ILBlueManager shareInstance].currentPeripheral];
}

//取消连接超时
- (void)cancelConnectTimeout
{
    CANCELFUNC(self,@selector(connectTimeout));
}

- (void)addConnectTimeout
{
    [self cancelConnectTimeout];
    ADDFUNC(self, @selector(connectTimeout), 20);
}

//连接超时
- (void)connectTimeout
{
    //超时需要取消当前发起的连接
    [self cancelConnectTimeout];
    [ILBlueManager cancelCurrentPeripheralConnection];
    NSString * errorStr = @"bluetooth connect peripheral timeout.";
    int errorCode = IL_BLUETOOTH_CONNECT_TIME_OUT_TYPE;
    NSError * error = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
    [self setDelegateScanConnectError:error];
    [self stopScan];
}

//发现服务超时
- (void)cancelDiscoveryServiceTimeout
{
    CANCELFUNC(self,@selector(discoveryServiceTimeout));
}


//添加发现服务超时
- (void)addDiscoveryServiceTimeout
{
  [self cancelDiscoveryServiceTimeout];
  ADDFUNC(self,@selector(discoveryServiceTimeout),20);
}

//发现服务超时
- (void)discoveryServiceTimeout
{
    [self cancelDiscoveryServiceTimeout];
    [ILBlueManager cancelCurrentPeripheralConnection];
    NSString * errorStr = @"the peripheral service discovery timeout.";
    int errorCode = IL_BLUETOOTH_DISCOVER_SERVICE_FAIL_TYPE;
    NSError * error = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
    [self setDelegateScanConnectError:error];
}

//发现服务特征超时
- (void)cancelDiscoverCharacteristicsTimeout
{
   CANCELFUNC(self,@selector(discoverCharacteristicsTimeout));
}

//添加发现服务特征超时
- (void)adddiscoverCharacteristicsTimeout
{
    [self cancelDiscoverCharacteristicsTimeout];
    ADDFUNC(self,@selector(discoverCharacteristicsTimeout),20);
}

//发现服务特征超时
- (void)discoverCharacteristicsTimeout
{
    //超时需要取消当前发现服务特征超时
    [self cancelDiscoverCharacteristicsTimeout];
    [ILBlueManager cancelCurrentPeripheralConnection];
    NSString * errorStr = @"peripheral device discovery service characteristics timeout.";
    int errorCode = IL_BLUETOOTH_DISCOVER_CHARACTERISTICS_TYPE;
    NSError * error = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
    [self setDelegateScanConnectError:error];
}

- (void)centralManagerDidUpdateState:(nonnull CBCentralManager *)central {
    switch (central.state) {
        case CBManagerStateUnknown:
            break;
        case CBManagerStateResetting:
            break;
        case CBManagerStateUnsupported:
            break;
        case CBManagerStateUnauthorized:
            break;
        case CBManagerStatePoweredOff:
        {
            [self setDelegateUpdateState:IL_MANAGER_STATE_POWEREDOFF];
        }
            break;
        case CBManagerStatePoweredOn:
        {
            [self setDelegateUpdateState:IL_MANAGER_STATE_POWEREDON];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                         (int64_t)(3 * NSEC_PER_SEC)),
                           dispatch_get_main_queue(), ^{
                [self startScan];
            });
        }
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict
{
    
}

#pragma mark 发现外设
- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary<NSString *,id> *)advertisementData
                  RSSI:(NSNumber *)RSSI{
    if (self.isStop)return;
    if ([ILGlobalEngineModel shareInstance].isBind) {
       ILPeripheralModel * model = [ILUtility didDiscoverPeripheral:peripheral
                                                  advertisementData:advertisementData
                                                               RSSI:RSSI];
        if ([model.macAddr isEqualToString:[ILGlobalEngineModel shareInstance].macAddr]) {
            [ILBlueManager connectDeviceWithModel:model];
            [self addConnectTimeout];
            self.isStop = YES;
        }
    }else {
      NSPredicate * containPredicate = [NSPredicate predicateWithFormat:@"uuidStr == %@",peripheral.identifier.UUIDString];
      NSArray * containDevices = [self.devices filteredArrayUsingPredicate:containPredicate];
      ILPeripheralModel * model = [ILUtility didDiscoverPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
      if (containDevices.count == 0) {
          [self.devices addObject:model];
      }else {
          ILPeripheralModel * containModel = [containDevices firstObject];
          containModel.name       = model.name;
          containModel.uuidStr    = model.uuidStr;
          containModel.rssi       = model.rssi;
          containModel.distance   = model.distance;
          containModel.macAddr    = model.macAddr;
          containModel.peripheral = peripheral;
      }
      NSArray * allDevices = [self.devices sortedArrayUsingDescriptors:@[self.rssiSortDesc]];
      allDevices = [allDevices filteredArrayUsingPredicate:self.rssiPredicate];
      if (self.delegate && [self.delegate respondsToSelector:@selector(bluetoothManager:allDevices:)]) {
          [self.delegate bluetoothManager:self allDevices:allDevices];
      }
    }
}

#pragma mark 连接外设成功
- (void)centralManager:(CBCentralManager *)central
  didConnectPeripheral:(CBPeripheral *)peripheral{
    [self stopScan];
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
    [self addDiscoveryServiceTimeout];
}

#pragma mark 断开连接
- (void)centralManager:(CBCentralManager *)central
didDisconnectPeripheral:(CBPeripheral *)peripheral
                 error:(NSError *)error
{
    [self cancelAllFuncs];
    NSString * errorStr = @"bluetooth did manual mode disconnect peripheral.";
    int errorCode = IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE;
    NSError * otherError = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
    [self setDelegateScanConnectError:otherError];
}

#pragma mark 链接失败
- (void)centralManager:(CBCentralManager *)central
didFailToConnectPeripheral:(CBPeripheral *)peripheral
                 error:(NSError *)error {
    if ([ILBlueManager isPoweredOff])return;
    [self cancelAllFuncs];
    NSString * errorStr = @"bluetooth did fail to connect peripheral.";
    int errorCode = IL_BLUETOOTH_CONNECT_FAIL_TYPE;
    NSError * otherError = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
    [self setDelegateScanConnectError:otherError];
}

#pragma mark 发现服务回调
static int serviceIndex = 0;
- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverServices:(NSError *)error {
    serviceIndex = 0;
    [self cancelDiscoveryServiceTimeout];
    if (!error) {
        for (CBService * service in peripheral.services)
        {
            [peripheral discoverCharacteristics:nil forService:service];
        }
        [self adddiscoverCharacteristicsTimeout];
    }else {
        [ILBlueManager cancelCurrentPeripheralConnection];
        NSString * errorStr = @"the peripheral service was found to fail.";
        int errorCode = IL_BLUETOOTH_DISCOVER_SERVICE_FAIL_TYPE;
        NSError * otherError = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
        [self setDelegateScanConnectError:otherError];
    }
}


#pragma mark 发现特征回调
- (void)peripheral:(CBPeripheral *)peripheral
didDiscoverCharacteristicsForService:(CBService *)service
             error:(NSError *)error{
    [self cancelDiscoverCharacteristicsTimeout];
    if (!error) {
        serviceIndex ++;
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FF12"]]) {
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"FF11"]]) {
                self.writeCharacteristic = characteristic;
            }
        }
        if ([ILUtility isOtaModeWithPeripheral:peripheral]) { //is ota mode
            self.timeCount = 0;
            self.currentPeripheral = peripheral;
            [self setDelegateUpdateState:IL_MANAGER_STATE_DID_CONNECT];
            if (self.delegate && [self.delegate respondsToSelector:@selector(bluetoothManager:centerManager:didConnectPeripheral:isOatMode:)]) {
                [self.delegate bluetoothManager:self centerManager:self.centralManager didConnectPeripheral:peripheral isOatMode:YES];
            }
        }else {
             if (peripheral.services.count == serviceIndex || serviceIndex == 0) {
                 self.timeCount = 0;
                  self.currentPeripheral = peripheral;
                 [self setDelegateUpdateState:IL_MANAGER_STATE_DID_CONNECT];
                    if (self.delegate && [self.delegate respondsToSelector:@selector(bluetoothManager:centerManager:didConnectPeripheral:isOatMode:)]) {
                        [self.delegate bluetoothManager:self centerManager:self.centralManager didConnectPeripheral:peripheral isOatMode:NO];
                    }
             }
         }
    }else {
        [ILBlueManager cancelCurrentPeripheralConnection];
        NSString * errorStr = @"peripheral device discovery service characteristics failed.";
        int errorCode = IL_BLUETOOTH_DISCOVER_CHARACTERISTICS_TYPE;
        NSError * otherError = [NSError errorWithDomain:errorStr code:errorCode userInfo:nil];
        [self setDelegateScanConnectError:otherError];
    }
}

#pragma mark 获取值
- (void)peripheral:(CBPeripheral *)peripheral
didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error{
    NSLog(@"characteristic == %@\n",characteristic);
    if (!error) {
        Byte codeBytes[characteristic.value.length];
        for (int i = 0 ; i < characteristic.value.length; i++) {
            NSData * itemData = [characteristic.value subdataWithRange:NSMakeRange(i,1)];
            codeBytes[i] = ((Byte*)[itemData bytes])[0];
        }
        if (codeBytes[0] == 0x01 && codeBytes[1] == 0x01) {
            [ILUpdateManager updateReceive:codeBytes];
        }else {
            struct protocol_get_real_time_temp_reply * live_temp = (struct protocol_get_real_time_temp_reply *)codeBytes;
            NSString * temp1 = [NSString stringWithFormat:@"%02X.%02X",live_temp->bcd_int,live_temp->bcd_dec];
            NSString * temp2 = [NSString stringWithFormat:@"%02X.%02X",live_temp->raw_bcd_int,live_temp->raw_bcd_dec];
            NSString * original = @"";
            for (int i = 0 ; i < characteristic.value.length; i++) {
                original = [original stringByAppendingFormat:@"%@%02X",i==0?@"":@"-",codeBytes[i]];
            }
            if (self.timeCount >= 5) {
            ILDeviceInfoModel * model = [[ILDeviceInfoModel alloc]init];
            model.macAddr = [ILGlobalEngineModel shareInstance].macAddr;
            model.uuidStr = [ILGlobalEngineModel shareInstance].uuidStr;
            model.userId  = [ILGlobalEngineModel shareInstance].userId;
            model.timeStamp = [ILUtility currentTimeStamp];
            model.minuteStr = [ILUtility currentMinuteStr];
            model.dateStr   = [ILUtility currentDateStr];
            model.hourStr   = [ILUtility currentHourStr];
            NSDateComponents * components = [ILUtility currentComponents];
            model.year  = components.year;
            model.month = components.month;
            model.originalData = original;
            model.temperature1 = temp1;
            model.temperature2 = temp2;
            [model saveOrUpdate];
            self.timeCount = 0;
            postNotify(@"ILBlueUpdateValue",@{@"value":temp1});
            }
            self.timeCount ++;
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(bluetoothManager:didUpdateValue:)]) {
            [self.delegate bluetoothManager:self didUpdateValue:characteristic.value];
        }
    }
}


- (void)setDelegateUpdateState:(IL_BLUETOOTH_MANAGER_STATE)state
{
    if (self.state == state)return;
    [self setValue:@(state) forKey:@"state"];
    if (self.state == IL_MANAGER_STATE_DID_CONNECT)[self setValue:@(0) forKey:@"errorCode"];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]postNotificationName:@"ILBlueState"
                                                           object:@{@"state":@(state)}];
        __strong typeof(self) strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(bluetoothManager:didUpdateState:)]) {
            [strongSelf.delegate bluetoothManager:strongSelf didUpdateState:strongSelf.state];
        }
    });
}

- (void)setDelegateScanConnectError:(NSError *)error
{
    if(self.errorCode == error.code)return;
    [self setValue:@(error.code) forKey:@"errorCode"];
    [self setValue:@(0) forKey:@"state"];
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        postNotify(@"ILBlueError",error);
        __strong typeof(self) strongSelf = weakSelf;
        if(strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(bluetoothManager:connectPeripheralError:)]) {
            [strongSelf.delegate bluetoothManager:strongSelf connectPeripheralError:error];
        }
    });
}

@end
