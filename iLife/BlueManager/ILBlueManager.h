//
//  ILBlueManager.h
//  iLife
//
//  Created by hedongyang on 2020/7/8.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ILBlueEnum.h"
#import "ILPeripheralModel.h"
#import "ILUserInfoModel.h"

@class ILBlueManager;
@protocol ILBlueManagerDelegate<NSObject>

- (void)bluetoothManager:(ILBlueManager *)manager
              allDevices:(NSArray <ILPeripheralModel *>*)allDevices;

- (BOOL)bluetoothManager:(ILBlueManager *)manager
           centerManager:(CBCentralManager *)centerManager
    didConnectPeripheral:(CBPeripheral *)peripheral
               isOatMode:(BOOL)isOtaMode;

- (void)bluetoothManager:(ILBlueManager *)manager
          didUpdateState:(IL_BLUETOOTH_MANAGER_STATE)state;

- (void)bluetoothManager:(ILBlueManager *)manager
  connectPeripheralError:(NSError *)error;

@optional
- (void)bluetoothManager:(ILBlueManager *)manager
          didUpdateValue:(NSData*)data;

@end

@interface ILBlueManager : NSObject
+(instancetype)shareInstance;
- (void)startScan;
- (void)stopScan;
+ (void)connectDeviceWithModel:(ILPeripheralModel *)model;
+ (void)cancelCurrentPeripheralConnection;
+ (void)sendData:(const void *)data dataLength:(NSInteger)length;
+ (BOOL)isPoweredOff;
+ (BOOL)isConnected;
+ (BOOL)isOta;
@property (nonatomic,strong) CBCentralManager * centralManager;
@property (nonatomic,strong) CBPeripheral * currentPeripheral;
@property (nonatomic,strong) CBCharacteristic * writeCharacteristic;
@property (nonatomic,strong) ILPeripheralModel * currentModel;
@property (nonatomic,assign) id<ILBlueManagerDelegate> delegate;
@property (nonatomic,assign,readonly) IL_BLUETOOTH_MANAGER_STATE  state;
@property (nonatomic,assign,readonly) IL_BLUETOOTH_CONNECT_ERROR_TYPE errorCode;
@end

