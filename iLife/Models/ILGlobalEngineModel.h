//
//  ILGlobalEngineModel.h
//  iLife
//
//  Created by hedongyang on 2020/8/26.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface ILGlobalEngineModel : NSObject
@property (nonatomic,copy) NSString * macAddr;
@property (nonatomic,copy) NSString * uuidStr;
@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString * deviceName;
@property (nonatomic,assign) BOOL isBind;
@property (nonatomic,strong) CBPeripheral * peripheral;
+ (__kindof ILGlobalEngineModel *)shareInstance;
@end

NS_ASSUME_NONNULL_END
