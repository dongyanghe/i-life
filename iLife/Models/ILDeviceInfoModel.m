//
//  ILDeviceInfoModel.m
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILDeviceInfoModel.h"
#import "ILUtility.h"

@implementation ILDeviceInfoModel

/**
查询一天所有分钟的数据
 */
+ (NSArray *)queryOneDayAllMinuteDataWithUserId:(NSString *)userId
                                     andMacAddr:(NSString *)macAddr
                                     andDateStr:(NSString *)dateStr
{
    NSMutableArray * dayMinutesArray = [NSMutableArray array];
//    if (   !userId  || userId.length == 0
//        || !macAddr || macAddr.length == 0
//        || !dateStr || dateStr.length == 0) {
//        return dayMinutesArray;
//    }
    for (int i = 0; i < 24; i++) {
        NSMutableArray * oneMinutes = [NSMutableArray array];
        NSString * hour = [NSString stringWithFormat:@"%ld",(long)[dateStr integerValue]+i*3600];
        for (int j = 0; j < 60; j++) {
            NSString * minute = [NSString stringWithFormat:@"%ld",(long)[hour integerValue]+j*60];
            /*
            NSString * minute = [NSString stringWithFormat:@"%ld",(long)[hour integerValue]+j*60];
            NSString * queryStr = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@' AND minute_str = '%@' ORDER BY time_stamp ASC ",userId,macAddr,minute];
            NSArray * array = [ILDeviceInfoModel queryModelsWithSqlStr:queryStr];
            if (array.count > 0) {
                ILDeviceInfoModel * model = [ILDeviceInfoModel calculateAvgDataWithModels:array];
                [oneMinutes addObject:model];
            }else {
                ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:minute
                                                                                      hour:hour
                                                                                   macAddr:macAddr
                                                                                   dateStr:dateStr
                                                                                    userId:userId];
                [oneMinutes addObject:model];
            }*/
            ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:minute
                                                                                  hour:hour
                                                                               macAddr:macAddr
                                                                               dateStr:dateStr
                                                                                userId:userId];
            [oneMinutes addObject:model];
        }
        [dayMinutesArray addObject:oneMinutes];
    }
    return dayMinutesArray;
}

/**
查询一天所有时钟的数据
 */
+ (NSArray *)queryOneDayAllHourDataWithUserId:(NSString *)userId
                                   andMacAddr:(NSString *)macAddr
                                   andDateStr:(NSString *)dateStr
{
    NSMutableArray * dayHoursArray = [NSMutableArray array];
//    if (   !userId  || userId.length == 0
//        || !macAddr || macAddr.length == 0
//        || !dateStr || dateStr.length == 0) {
//        return dayHoursArray;
//    }
    for (int i = 0; i < 24; i++) {
        NSString * hour = [NSString stringWithFormat:@"%ld",(long)[dateStr integerValue]+i*3600];
        /*
        NSString * hour = [NSString stringWithFormat:@"%ld",(long)[dateStr integerValue]+i*3600];
        NSString * queryStr = [NSString stringWithFormat:@"SELECT * ,COUNT(DISTINCT minute_str) FROM 'device_info_table' WHERE user_id = '%@' AND mac_addr = '%@' AND hour_str = '%@' ORDER BY time_stamp ASC",userId,macAddr,hour];
        NSArray * oneHours = [ILDeviceInfoModel queryElementModelsWithSqlStr:queryStr];
        if (oneHours.count > 0) {
           [dayHoursArray addObject:oneHours];
        }else {
            NSMutableArray * oneMinutes = [NSMutableArray array];
            for (int j = 0; j < 60; j++) {
            NSString * minute = [NSString stringWithFormat:@"%ld",[hour integerValue] + j*60];
            ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:minute
                                                                                  hour:hour
                                                                               macAddr:macAddr
                                                                               dateStr:dateStr
                                                                                userId:userId];
              [oneMinutes addObject:model];
            }
            [dayHoursArray addObject:oneMinutes];
        }
         */
        NSMutableArray * oneMinutes = [NSMutableArray array];
        for (int j = 0; j < 60; j++) {
        NSString * minute = [NSString stringWithFormat:@"%ld",[hour integerValue] + j*60];
        ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:minute
                                                                              hour:hour
                                                                           macAddr:macAddr
                                                                           dateStr:dateStr
                                                                            userId:userId];
          [oneMinutes addObject:model];
        }
        [dayHoursArray addObject:oneMinutes];
    }
    return dayHoursArray;
}

/**
查询一 周所有日期的数据
 */
+ (NSArray *)queryOneWeekAllDayDataWithUserId:(NSString *)userId
                                   andMacAddr:(NSString *)macAddr
                                   andDateStr:(NSString *)dateStr
{
    NSMutableArray * oneWeekArray = [NSMutableArray array];
//    if (   !userId  || userId.length  == 0
//        || !macAddr || macAddr.length == 0
//        || !dateStr || dateStr.length == 0) {
//        return oneWeekArray;
//    }
    NSDateComponents * components = [ILUtility dateComponentsWithDateStr:dateStr];
    NSInteger  currentWeekDay = (components.weekday > 1) ? (components.weekday - 1) : (7 - labs(components.weekday - 1));
    NSString * weekStartDayStr = [NSString stringWithFormat:@"%ld",[dateStr integerValue] - (currentWeekDay - 1) * 60 * 60 * 24];
    for (int i = 0; i < 7; i++) {
        NSString * oneDateStr = [NSString stringWithFormat:@"%ld",[weekStartDayStr integerValue] + i * 60 * 60 * 24];
        /*
        NSString * queryStr = [NSString stringWithFormat:@"SELECT * ,COUNT(DISTINCT minute_str) FROM 'device_info_table' WHERE user_id = '%@' AND mac_addr = '%@' AND date_str = '%@' ORDER BY time_stamp ASC",userId,macAddr,oneDateStr];
        NSArray * oneDays = [ILDeviceInfoModel queryElementModelsWithSqlStr:queryStr];
        if (oneDays.count > 0) {
            [oneWeekArray addObject:oneDays];
        }else {
            NSMutableArray * oneDayArray = [NSMutableArray array];
            for (int j = 0; j < 24; j++) {
            NSString * hour = [NSString stringWithFormat:@"%ld",[oneDateStr integerValue] + j*60*60];
            ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:hour
                                                                                  hour:hour
                                                                               macAddr:macAddr
                                                                               dateStr:oneDateStr
                                                                                userId:userId];
              [oneDayArray addObject:model];
            }
            [oneWeekArray addObject:oneDayArray];
        }*/
        NSMutableArray * oneDayArray = [NSMutableArray array];
        for (int j = 0; j < 24; j++) {
        NSString * hour = [NSString stringWithFormat:@"%ld",[oneDateStr integerValue] + j*60*60];
        ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:hour
                                                                              hour:hour
                                                                           macAddr:macAddr
                                                                           dateStr:oneDateStr
                                                                            userId:userId];
          [oneDayArray addObject:model];
        }
        [oneWeekArray addObject:oneDayArray];
    }
    return oneWeekArray;
}

/**
查询一 月所有日期的数据
 */
+ (NSArray *)queryOneMonthAllDayDataWithUserId:(NSString *)userId
                                    andMacAddr:(NSString *)macAddr
                                    andDateStr:(NSString *)dateStr
{
    NSMutableArray * oneMonthArray = [NSMutableArray array];
//    if (   !userId  || userId.length  == 0
//        || !macAddr || macAddr.length == 0
//        || !dateStr || dateStr.length == 0) {
//        return oneMonthArray;
//    }
    NSDateComponents * components = [ILUtility dateComponentsWithDateStr:dateStr];
    NSInteger dayLength = [ILUtility getDaysInMonthWithYear:components.year month:components.month];
    NSString * startDay = [ILUtility get1970timeTempWithYear:components.year
                                                    andMonth:components.month];
    for (int i = 0; i < dayLength; i++) {
        NSString * oneDateStr = [NSString stringWithFormat:@"%ld",[startDay integerValue] + i * 60 * 60 * 24];
        /*
        NSString * oneDateStr = [NSString stringWithFormat:@"%ld",[startDay integerValue] + i * 60 * 60 * 24];
        NSString * queryStr = [NSString stringWithFormat:@"SELECT * ,COUNT(DISTINCT minute_str) FROM 'device_info_table' WHERE user_id = '%@' AND mac_addr = '%@' AND date_str = '%@' ORDER BY time_stamp ASC",userId,macAddr,oneDateStr];
        NSArray * oneDays = [ILDeviceInfoModel queryElementModelsWithSqlStr:queryStr];
        if (oneDays.count > 0) {
            [oneMonthArray addObject:oneDays];
        }else {
            NSMutableArray * oneDayArray = [NSMutableArray array];
            for (int j = 0; j < 24; j++) {
            NSString * hour = [NSString stringWithFormat:@"%ld",[oneDateStr integerValue] + j*60*60];
            ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:hour
                                                                                  hour:hour
                                                                               macAddr:macAddr
                                                                               dateStr:oneDateStr
                                                                                userId:userId];
              [oneDayArray addObject:model];
            }
            [oneMonthArray addObject:oneDayArray];
        }*/
        
        NSMutableArray * oneDayArray = [NSMutableArray array];
        for (int j = 0; j < 24; j++) {
        NSString * hour = [NSString stringWithFormat:@"%ld",[oneDateStr integerValue] + j*60*60];
        ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:hour
                                                                              hour:hour
                                                                           macAddr:macAddr
                                                                           dateStr:oneDateStr
                                                                            userId:userId];
          [oneDayArray addObject:model];
        }
        [oneMonthArray addObject:oneDayArray];
    }
    return oneMonthArray;
}

/**
查询一 年所有月份的数据
 */
+ (NSArray *)queryOneYearAllMonthDataWithUserId:(NSString *)userId
                                    andMacAddr:(NSString *)macAddr
                                    andDateStr:(NSString *)dateStr
{
    NSMutableArray * oneYearArray = [NSMutableArray array];
//    if (   !userId  || userId.length  == 0
//        || !macAddr || macAddr.length == 0
//        || !dateStr || dateStr.length == 0) {
//        return oneYearArray;
//    }
    NSDateComponents * components = [ILUtility dateComponentsWithDateStr:dateStr];
    for (int i = 0; i < 12; i++) {
       /* NSString * queryStr = [NSString stringWithFormat:@"SELECT * ,COUNT(DISTINCT date_str) FROM 'device_info_table' WHERE user_id = '%@' AND mac_addr = '%@' AND year = %ld AND month = %ld ORDER BY time_stamp ASC",userId,macAddr,(long)components.year,(long)i+1];
        NSArray * oneMonths = [ILDeviceInfoModel queryElementModelsWithSqlStr:queryStr];
        if (oneMonths.count > 0) {
            [oneYearArray addObject:oneMonths];
        }else {
            NSMutableArray * oneMonthArray = [NSMutableArray array];
            NSInteger dayLength = [ILUtility getDaysInMonthWithYear:components.year month:i+1];
            NSString * startDay = [ILUtility get1970timeTempWithYear:components.year
                                                            andMonth:i+1
                                                              andDay:0
                                                             andHour:0
                                                           andMinute:0];
            for (int j = 0; j < dayLength; j++) {
                NSString * oneDateStr = [NSString stringWithFormat:@"%ld",[startDay integerValue] + j * 60 * 60 * 24];
                ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:oneDateStr
                                                                                      hour:oneDateStr
                                                                                   macAddr:macAddr
                                                                                   dateStr:oneDateStr
                                                                                    userId:userId];
                [oneMonthArray addObject:model];
            }
            [oneYearArray addObject:oneMonthArray];
        }*/
        NSMutableArray * oneMonthArray = [NSMutableArray array];
        NSInteger dayLength = [ILUtility getDaysInMonthWithYear:components.year month:i+1];
        NSString * startDay = [ILUtility get1970timeTempWithYear:components.year
                                                        andMonth:i+1];
        for (int j = 0; j < dayLength; j++) {
            NSString * oneDateStr = [NSString stringWithFormat:@"%ld",[startDay integerValue] + j * 60 * 60 * 24];
            ILDeviceInfoModel * model = [ILDeviceInfoModel initOneMinuteDataWithMinute:oneDateStr
                                                                                  hour:oneDateStr
                                                                               macAddr:macAddr
                                                                               dateStr:oneDateStr
                                                                                userId:userId];
            [oneMonthArray addObject:model];
        }
        [oneYearArray addObject:oneMonthArray];
    }
    return oneYearArray;
}

+ (ILDeviceInfoModel *)calculateAvgDataWithModels:(NSArray *)models
{
    NSPredicate * predicate =  [NSPredicate predicateWithFormat:@"temperature1 > 0 AND temperature2 > 0"];
    NSArray * newModels     =  [models filteredArrayUsingPredicate:predicate];
    CGFloat avgTemp1  = (NSInteger)[[newModels valueForKeyPath:@"@avg.temperature1"] floatValue];
    CGFloat avgTemp2  = (NSInteger)[[newModels valueForKeyPath:@"@avg.temperature2"] floatValue];
    ILDeviceInfoModel * model = [newModels firstObject];
    model.temperature1 = [NSString stringWithFormat:@"%.2f",avgTemp1];
    model.temperature2 = [NSString stringWithFormat:@"%.2f",avgTemp2];
    return model;
}

+ (ILDeviceInfoModel *)initOneMinuteDataWithMinute:(NSString *)minute
                                              hour:(NSString *)hour
                                           macAddr:(NSString *)macAddr
                                           dateStr:(NSString *)dateStr
                                            userId:(NSString *)userId
{
     ILDeviceInfoModel * model = [[ILDeviceInfoModel alloc]init];
     model.macAddr = macAddr;
     model.dateStr = dateStr;
     model.userId  = userId;
     model.hourStr = hour;
     model.minuteStr = minute;
     model.timeStamp = minute;
     NSDateComponents * components = [ILUtility dateComponentsWithDateStr:dateStr];
     model.month = components.month;
     model.year = components.year;
     model.temperature1 = [NSString stringWithFormat:@"0"];
     model.temperature2 = [NSString stringWithFormat:@"0"];
     model.originalData = @"";
     return model;
}

@end
