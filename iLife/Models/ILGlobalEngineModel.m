//
//  ILGlobalEngineModel.m
//  iLife
//
//  Created by hedongyang on 2020/8/26.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILGlobalEngineModel.h"

@implementation ILGlobalEngineModel

static ILGlobalEngineModel * EngineModel = nil;
+ (__kindof ILGlobalEngineModel *)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        EngineModel = [[super allocWithZone:NULL] init] ;
    });
    return EngineModel;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    return [ILGlobalEngineModel shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [ILGlobalEngineModel shareInstance];
}

@end
