//
//  ILGetLiveTempModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILGetLiveTempModel : ILBaseModel
/**
 实时温度数值
 */
@property (nonatomic,copy) NSString * temperature;
@end
