//
//  ILSetUpInfoModel.m
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILSetUpInfoModel.h"

@implementation ILSetUpInfoModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.userId = @"";
        self.uuidStr = @"";
        self.macAddr = @"";
        self.hotTemp = @"37.50";
        self.lowTemp = @"36.50";
        self.isConnected = YES;
        self.isLowBattery = YES;
        self.isOpenBell = YES;
        self.isVibration = YES;
    }
    return self;
}

@end
