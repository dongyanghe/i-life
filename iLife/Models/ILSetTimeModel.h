//
//  ILSetTimeModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILSetTimeModel : ILBaseModel
/**
0x00表示成功,0x01表示失败
*/
@property (nonatomic,assign) NSInteger state;

@property (nonatomic,assign) NSInteger year;

@property (nonatomic,assign) NSInteger month;

@property (nonatomic,assign) NSInteger day;

@property (nonatomic,assign) NSInteger hour;

@property (nonatomic,assign) NSInteger minute;

@property (nonatomic,assign) NSInteger second;

@end

