//
//  ILBaseModel.m
//  iLife
//
//  Created by hedongyang on 2020/7/12.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"
#import "ILDBManager.h"

@implementation ILBaseModel

+ (void)initialize
{
    if ([ILDBManager getTableNameWithClass:[self class]]) {
        [ILDBManager createTableWithClass:[self class]];
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self getColumns];
    }
    return self;
}

- (void)getColumns
{
    NSDictionary * dic = [ILDBManager getPropertysWithClass:[self class] isOrigin:NO];
    NSDictionary * originDic = [ILDBManager getPropertysWithClass:[self class] isOrigin:YES];
    _originColumnNames = [originDic objectForKey:@"name"]?:[NSArray array];
    _columnNames = [dic objectForKey:@"name"]?:[NSArray array];
    _columnTypes = [dic objectForKey:@"type"]?:[NSArray array];
}

- (BOOL)saveOrUpdate
{
    if (self.pk <= 0) {
     return [self save];
    }
     return [self update];
}

- (BOOL)save
{

    return [ILDBManager saveWithModel:self];
}

- (BOOL)update
{
    return [ILDBManager updateWithModel:self];
}

- (BOOL)del
{
    return [ILDBManager delWithModel:self];
}

+ (NSArray <ILBaseModel *>*)queryModelsWithSqlStr:(NSString *)sqlStr
{
   return  [ILDBManager queryAllColumnWithClass:[self class] queryStr:sqlStr];
}

+ (NSArray <ILBaseModel *>*)queryElementModelsWithSqlStr:(NSString *)sqlStr
{
   return  [ILDBManager queryAsColumnElementWithClass:[self class] queryStr:sqlStr];
}

+ (NSArray <ILBaseModel *>*)queryAllModels
{
    return [ILDBManager getOneTableAllDataWithClassName:[self class]];
}

+ (BOOL)deleteModelsWithSqlStr:(NSString *)sqlStr
{
    return [ILDBManager deleteDataWithClass:[self class] queryStr:sqlStr];
}

+ (BOOL)deleteCurrentTable
{
    BOOL isSuccess = [ILDBManager deleteCurrentTableWithClass:[self class]];
    if (isSuccess) {
       isSuccess = [ILDBManager createTableWithClass:[self class]];
    }
    return isSuccess;
}

@end
