//
//  ILStructModel.h
//  iLife
//
//  Created by hedongyang on 2020/8/5.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

struct protocol_head
{
  uint8_t cmd;
  uint8_t key;
};

struct protocol_ota_reply
{
struct protocol_head head;
uint8_t state; //0x00:成功, 0x01:电量过低,0x02:设备不⽀持
};

struct protocol_device_info
{
struct protocol_head head;
uint16_t device_id; //设备id
uint8_t version; //固件版本号
uint8_t batt_level; //电量等级
uint8_t bind_state; //绑定状态
uint8_t platform; //设备的平台 0x00:nordic 0x01:realtek
uint8_t mac_addr[6]; //mac地址
 };

struct protocol_get_new_real_time_temp_reply
{
    struct protocol_head head;
    uint8_t int_temp[2];   //实际温度值*100,表示范围 0~42 保留两位小数
};

struct protocol_get_real_time_temp_reply
{
//struct protocol_head head;
uint8_t bcd_sign; //负数符号位 0-正数 1-负数
uint8_t bcd_int; //矫正后整数部分温度值
uint8_t bcd_dec; //矫正后⼩数部分温度值
uint8_t int_temp[2]; //有符号数，实际温度值*100。 表示范围 -32768~32767， 除以100后还原为实际温度值
uint8_t raw_bcd_sign; //原始数据负数符号位 0-正数 1-负数
uint8_t raw_bcd_int; //原始数据bcd整数部分
uint8_t raw_bcd_dec; //原始数据bcd⼩数部分
};

struct protocol_start_bind
{
struct protocol_head head;
uint8_t system; //系统:（0x01:ios，0x02:android）
uint8_t system_version; //系统版本号 
uint8_t clear_data_flag; //是否清除当前设备数据, 0x00 不清除数据 0x01 清除数据
};

struct protocol_start_bind_reply
{
struct protocol_head head;
uint8_t bind_state; //0x00表示成功,0x01表示失败,0x02表示已经绑
};

struct protocol_set_time
{
struct protocol_head head;
uint16_t year;
uint8_t month;
uint8_t day;
uint8_t hour;
uint8_t minute;
uint8_t second;
};

struct protocol_set_time_reply
{
struct protocol_head head;
uint8_t state; //0x00表示成功,0x01表示失败
};

struct protocol_un_bind_reply
{
struct protocol_head head;
uint8_t state; //0x00表示成功,0x01表示失败
};

struct protocol_sync_temperature_data
{
struct protocol_head head;
    
};

@interface ILStructModel : NSObject

@end
