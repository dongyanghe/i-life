//
//  ILSetUpInfoModel.h
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILSetUpInfoModel : ILBaseModel
@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString * macAddr;
@property (nonatomic,copy) NSString * uuidStr;
@property (nonatomic,copy) NSString * hotTemp;
@property (nonatomic,copy) NSString * lowTemp;
@property (nonatomic,assign) BOOL isOpenBell;
@property (nonatomic,assign) BOOL isVibration;
@property (nonatomic,assign) BOOL isLowBattery;
@property (nonatomic,assign) BOOL isConnected;
@end

