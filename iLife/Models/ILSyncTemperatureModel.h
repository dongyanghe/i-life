//
//  ILSyncTemperatureModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILSyncTemperatureItemModel : ILBaseModel

@property (nonatomic,assign) NSInteger offset;
@property (nonatomic,copy) NSString * dateStr;
@property (nonatomic,copy) NSString * hourStr;
@property (nonatomic,copy) NSString * minuteStr;
@property (nonatomic,copy) NSString * timeStamp;
@property (nonatomic,copy) NSString * temperature;

@end

@interface ILSyncTemperatureModel : ILBaseModel

@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString * macAddr;
@property (nonatomic,copy) NSString * uuidStr;
@property (nonatomic,copy) NSString * dateStr;
@property (nonatomic,assign) NSInteger year;
@property (nonatomic,assign) NSInteger month;
@property (nonatomic,assign) NSInteger day;
@property (nonatomic,assign) NSInteger itemsCount;
@property (nonatomic,assign) NSInteger startTime;
@property (nonatomic,strong) NSArray<ILSyncTemperatureItemModel *> * items;

@end

