//
//  ILGetDeviceInfoModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"


@interface ILGetDeviceInfoModel : ILBaseModel
/**
 设备ID
 */
@property (nonatomic,assign) NSInteger deviceId;
/**
 固件版本
 */
@property (nonatomic,assign) NSInteger version;
/**
 电量级别 0-100
 */
@property (nonatomic,assign) NSInteger battLevel;
/**
 绑定状态 0未绑定 1已绑定
 */
@property (nonatomic,assign) NSInteger bindState;
/**
 设备的平台 0x00:nordic 0x01:realtek 0x02:汇顶
 */
@property (nonatomic,assign) NSInteger platform;
/**
 Mac 地址
 */
@property (nonatomic,copy) NSString * macAddr;

@end
