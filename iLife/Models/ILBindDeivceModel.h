//
//  ILBindDeivceModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/13.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILBindDeivceModel : ILBaseModel
/**
 0x00表示成功,0x01表示失败,0x02表示已经绑
 */
@property (nonatomic,assign) NSInteger bindState;
@end
