//
//  ILGetLiveTempModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILGetLiveTempModel.h"

@implementation ILGetLiveTempModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cmd = 0x02;
        self.key = 0x02;
    }
    return self;
}

@end
