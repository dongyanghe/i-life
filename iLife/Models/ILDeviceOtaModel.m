//
//  ILDeviceOtaModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILDeviceOtaModel.h"

@implementation ILDeviceOtaModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cmd = 0x01;
        self.key = 0x01;
    }
    return self;
}

@end
