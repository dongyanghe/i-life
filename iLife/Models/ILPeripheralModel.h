//
//  ILPeripheralModel.h
//  iLife
//
//  Created by hedongyang on 2020/7/17.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface ILPeripheralModel : NSObject

@property (nullable,nonatomic,strong) CBPeripheral * peripheral;
@property (nullable,nonatomic,copy)   NSString * name;
@property (nullable,nonatomic,copy)   NSString * uuidStr;
@property (nonatomic,assign) NSInteger  rssi;
@property (nonatomic,assign) float distance;
@property (nullable,nonatomic,copy) NSString * macAddr;
@end

NS_ASSUME_NONNULL_END
