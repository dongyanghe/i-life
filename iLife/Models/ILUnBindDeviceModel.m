//
//  ILUnBindDeviceModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILUnBindDeviceModel.h"

@implementation ILUnBindDeviceModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cmd = 0x03;
        self.key = 0x02;
    }
    return self;
}


@end
