//
//  ILSetTimeModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILSetTimeModel.h"
#import "ILUtility.h"

@implementation ILSetTimeModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cmd = 0x03;
        self.key = 0x03;
        NSDateComponents * components = [ILUtility currentComponents];
        self.year = components.year;
        self.month = components.month;
        self.day = components.day;
        self.hour = components.hour;
        self.minute = components.minute;
        self.second = components.second;
    }
    return self;
}

@end
