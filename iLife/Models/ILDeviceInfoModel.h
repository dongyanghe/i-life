//
//  ILDeviceInfoModel.h
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILDeviceInfoModel : ILBaseModel
@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString * macAddr;
@property (nonatomic,copy) NSString * uuidStr;
@property (nonatomic,copy) NSString * timeStamp;
@property (nonatomic,copy) NSString * dateStr;
@property (nonatomic,copy) NSString * hourStr;
@property (nonatomic,copy) NSString * minuteStr;
@property (nonatomic,assign) NSInteger year;
@property (nonatomic,assign) NSInteger month;
@property (nonatomic,copy) NSString * temperature1;
@property (nonatomic,copy) NSString * temperature2;
@property (nonatomic,copy) NSString * originalData;

+ (ILDeviceInfoModel *)calculateAvgDataWithModels:(NSArray *)models;

+ (NSArray *)queryOneDayAllMinuteDataWithUserId:(NSString *)userId
                                    andMacAddr:(NSString *)macAddr
                                    andDateStr:(NSString *)dateStr;

+ (NSArray *)queryOneDayAllHourDataWithUserId:(NSString *)userId
                                   andMacAddr:(NSString *)macAddr
                                   andDateStr:(NSString *)dateStr;


+ (NSArray *)queryOneWeekAllDayDataWithUserId:(NSString *)userId
                                   andMacAddr:(NSString *)macAddr
                                   andDateStr:(NSString *)dateStr;


+ (NSArray *)queryOneMonthAllDayDataWithUserId:(NSString *)userId
                                    andMacAddr:(NSString *)macAddr
                                    andDateStr:(NSString *)dateStr;


+ (NSArray *)queryOneYearAllMonthDataWithUserId:(NSString *)userId
                                     andMacAddr:(NSString *)macAddr
                                     andDateStr:(NSString *)dateStr;
@end
