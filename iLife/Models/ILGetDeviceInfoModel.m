//
//  ILGetDeviceInfoModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILGetDeviceInfoModel.h"

@implementation ILGetDeviceInfoModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cmd = 0x02;
        self.key = 0x01;
    }
    return self;
}

@end
