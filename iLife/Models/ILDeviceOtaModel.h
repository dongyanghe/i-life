//
//  ILDeviceOtaModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILDeviceOtaModel : ILBaseModel
/**
 0x00:成功, 0x01:电量过低,0x02:设备不⽀持
 */
@property(nonatomic,assign) NSInteger state;
@end

