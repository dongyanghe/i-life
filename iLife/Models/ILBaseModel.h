//
//  ILBaseModel.h
//  iLife
//
//  Created by hedongyang on 2020/7/12.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ILBaseModel : NSObject
@property (copy,readonly,nonatomic,nullable) NSArray<NSString *>  * originColumnNames;
@property (copy,readonly,nonatomic,nullable) NSArray<NSString *>  * columnNames;
@property (copy,readonly,nonatomic,nullable) NSArray<NSString *>  * columnTypes;

@property (nonatomic,assign) NSInteger pk;
@property (nonatomic,assign) NSInteger cmd;
@property (nonatomic,assign) NSInteger key;

- (BOOL)saveOrUpdate;

- (BOOL)del;

+ (NSArray <ILBaseModel *>*)queryModelsWithSqlStr:(NSString *)sqlStr;

+ (NSArray <ILBaseModel *>*)queryElementModelsWithSqlStr:(NSString *)sqlStr;

+ (BOOL)deleteModelsWithSqlStr:(NSString *)sqlStr;

+ (NSArray <ILBaseModel *>*)queryAllModels;

+ (BOOL)deleteCurrentTable;

@end

NS_ASSUME_NONNULL_END
