//
//  ILUnBindDeviceModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/14.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILBaseModel.h"

@interface ILUnBindDeviceModel : ILBaseModel
/**
 0x00表示成功,0x01表示失败
 */
@property (nonatomic,assign) NSInteger unBindState;
@end

