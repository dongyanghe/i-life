//
//  ILUserInfoModel.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILBaseModel.h"

@interface ILUserInfoModel : ILBaseModel
@property (nonatomic,assign) NSInteger sex;
@property (nonatomic,copy) NSString * userName;
@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString * birthday;
@property (nonatomic,copy) NSString * macAddr;
@property (nonatomic,copy) NSString * uuidStr;
@property (nonatomic,assign) BOOL bindState;
@property (nonatomic,copy) NSString * deviceName;
@end

