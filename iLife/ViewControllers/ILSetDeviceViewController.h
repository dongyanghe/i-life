//
//  ILSetDeviceViewController.h
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILSetUpInfoModel.h"

@interface ILSetDeviceViewController : UIViewController
@property (nonatomic,strong) ILSetUpInfoModel * setUpModel;
@end
