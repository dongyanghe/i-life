//
//  ILConnectDeviceViewController.h
//  iLife
//
//  Created by hedongyang on 2020/7/17.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILUserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ILConnectDeviceViewController : UIViewController
@property (nonatomic,strong) ILUserInfoModel * userModel;
@end

NS_ASSUME_NONNULL_END
