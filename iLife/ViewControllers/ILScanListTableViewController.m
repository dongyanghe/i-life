//
//  ILScanListTableViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILScanListTableViewController.h"

@interface ILScanListTableViewController ()

@end

@implementation ILScanListTableViewController

- (void)dealloc
{
    
}

- (void)addLeftButton
{
    UIBarButtonItem * leftButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"✖"
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(leftAction)];
    self.navigationItem.leftBarButtonItem = leftButtonItem;
}

- (void)leftAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设备列表";
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [UIView new];
    [self addLeftButton];
}

- (void)setDeviceModels:(NSArray<ILPeripheralModel *> *)deviceModels
{
    _deviceModels = deviceModels;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _deviceModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"cellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = _deviceModels[indexPath.row].name;
    cell.detailTextLabel.text = _deviceModels[indexPath.row].macAddr;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectDeviceCallbak) {
        self.selectDeviceCallbak(_deviceModels[indexPath.row]);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
