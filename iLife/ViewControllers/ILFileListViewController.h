//
//  ILFileListViewController.h
//  iLife
//
//  Created by hedongyang on 2020/8/6.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ILFileListViewController : UIViewController
@property (nonatomic,copy) NSString * filePath;
@end

