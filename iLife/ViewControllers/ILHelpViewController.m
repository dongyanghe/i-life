//
//  ILHelpViewController.m
//  iLife
//
//  Created by hedongyang on 2020/8/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILHelpViewController.h"
#import "ILUtility.h"

@interface ILHelpViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * items;
@end

@implementation ILHelpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = NO;
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
      make.edges.equalTo(self.view);
    }];
    self.items = @[@"蓝牙无法连接:",
                   @"(1)手机系统是否满足要求,Android 4.4及以上版本，iOS9.0及以上版本",
                   @"(2)确认APP是否为最新版本的iLife+，建议您使用最新版本进行体验;设备蓝牙是否打开，打开时会有红绿灯交替闪光",
                   @"(3)手机蓝牙是否已打开并能正常使用，建议关闭所有程序重启蓝牙之后再链接",
                   @"蓝牙连接经常断开:",
                   @"(1)设备和手机距离是否过远：7米以外蓝牙连接效果会减弱",
                   @"(2)设备和手机之间是否有遮挡物，人体、佩戴的金属首饰等会干扰蓝牙的连接",
                   @"(3)设备电量是否充足，足以打开蓝牙，有红绿灯交替闪光",
                   @"(4)手机蓝牙功能是否异常，建议关闭程序并重启蓝牙",
                   @"(5)当APP退出到后台运行时，iOS系统可能会自动杀死系统，导致蓝牙连接断开",
                   @"(6)手机黑屏之后蓝牙自动断开，可能是因为手机的省电模式或手机助手自动关闭蓝牙，可设置手机后重试"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellIdentifier = @"cellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.numberOfLines = 0;
    }
    if (indexPath.row == 0 || indexPath.row == 4) {
        cell.backgroundColor = LigntGrayColor;
        cell.textLabel.font = [UIFont systemFontOfSize:18];
    }else{
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    cell.textLabel.text = [self.items objectAtIndex:indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * str = self.items[indexPath.row];
    CGFloat height = 0;
    if (indexPath.row == 0 || indexPath.row == 4) {
        height = [ILUtility getHeightWithText:str
           width:tableView.frame.size.width - 32
        fontSize:[UIFont systemFontOfSize:18]];
    }else{
        height = [ILUtility getHeightWithText:str
           width:tableView.frame.size.width - 32
        fontSize:[UIFont systemFontOfSize:14]];
    }
    return height + 20;
}

@end
