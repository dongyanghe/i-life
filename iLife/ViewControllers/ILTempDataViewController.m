//
//  ILTempDataViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/16.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILTempDataViewController.h"
#import "UIImage+ColorToImage.h"
#import "ILSetDeviceViewController.h"
#import "ILChartView.h"
#import "ILBlueEnum.h"
#import "ILBlueManager.h"
#import "ILSoundManager.h"
#import "ILGlobalEngineModel.h"

@interface ILTempDataViewController ()<UIScrollViewDelegate,YMLSegmentControlDelegate>
@property (nonatomic,strong) UIImage * lineImage;
@property (nonatomic,strong) UIColor * barTintColor;
@property (nonatomic,strong) UIView  * bgView;
@property (nonatomic,strong) UILabel * temperatureLabel;
@property (nonatomic,strong) UILabel * connectLabel;
@property (nonatomic,strong) UIImageView * imageViewConnect;
@property (nonatomic,strong) UILabel * powerLabel;
@property (nonatomic,strong) ILChartView * chartView;
@end

@implementation ILTempDataViewController

- (void)dealloc
{
    removeNotification(self);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UINavigationBar * navigationBar = self.navigationController.navigationBar;
    navigationBar.barTintColor = self.barTintColor;
    [navigationBar setShadowImage:self.lineImage];
    navigationBar.tintColor = [UIColor blackColor];
    navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]};
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.lineImage = self.navigationController.navigationBar.shadowImage;
    self.barTintColor = self.navigationController.navigationBar.barTintColor;
    UINavigationBar * navigationBar = self.navigationController.navigationBar;
    navigationBar.barTintColor = MainColor;
    [navigationBar setShadowImage:[UIImage imageWithColor:MainColor]];
    navigationBar.tintColor = [UIColor whiteColor];
    navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    NSString * sqlStr = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@'",self.userModel.userId,self.userModel.macAddr];
    NSArray <ILBaseModel *>* array = [ILSetUpInfoModel queryModelsWithSqlStr:sqlStr];
    [ILSoundManager shareInstance].setUpModel = (ILSetUpInfoModel *)[array firstObject];
    if (array.count == 0) {
        [ILSoundManager shareInstance].setUpModel.userId = self.userModel.userId;
        [ILSoundManager shareInstance].setUpModel.macAddr = self.userModel.macAddr;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent=NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"查看体温";
    addNotification(self,@selector(BlueState:),@"ILBlueState");
    addNotification(self,@selector(BlueUpdate:),@"ILBlueUpdateValue");
    [ILGlobalEngineModel shareInstance].macAddr = self.userModel.macAddr;
    [ILGlobalEngineModel shareInstance].deviceName = self.userModel.deviceName;
    [ILGlobalEngineModel shareInstance].isBind = self.userModel.bindState;
    [ILGlobalEngineModel shareInstance].userId = self.userModel.userId;
    [ILGlobalEngineModel shareInstance].uuidStr = self.userModel.uuidStr;
    [self addRightButton];
    [self addSubviews];
}

- (void)addRightButton
{
    UIBarButtonItem * rightButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"setting"]
                                                                         style:UIBarButtonItemStyleDone
                                                                        target:self
                                                                        action:@selector(rightAction)];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

- (void)rightAction
{
    ILSetDeviceViewController * vc = [[ILSetDeviceViewController alloc]init];
    vc.setUpModel = [ILSoundManager shareInstance].setUpModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)BlueUpdate:(NSNotification *)notification
{
    NSDictionary * dic = notification.object;
    NSString * tempStr = [dic valueForKey:@"value"];
    tempStr = [tempStr stringByAppendingString:@"℃"];
    NSMutableAttributedString * attribut = [self getTempAttributedStr:tempStr];
    self.temperatureLabel.attributedText = attribut;
}

- (void)BlueState:(NSNotification *)notification
{
    NSDictionary * dic = notification.object;
    IL_BLUETOOTH_MANAGER_STATE state = [[dic valueForKey:@"state"] integerValue];
    if (state == IL_MANAGER_STATE_DID_CONNECT) {
        self.connectLabel.text = @"已连接";
        self.imageViewConnect.image = [UIImage imageNamed:@"connect"];
    }else {
        self.connectLabel.text = @"未连接";
        self.imageViewConnect.image = [UIImage imageNamed:@"connect_no"];
    }
}

- (void)segmentValueChange:(UISegmentedControl *)control
{
    if (control.selectedSegmentIndex == 0) {
        self.chartView.type = IL_HOUR_DATA_TYPE;
    }else if (control.selectedSegmentIndex == 1) {
        self.chartView.type = IL_DAY_DATA_TYPE;
    }else if (control.selectedSegmentIndex == 2) {
        self.chartView.type = IL_WEEK_DATA_TYPE;
    }else if (control.selectedSegmentIndex == 3) {
        self.chartView.type = IL_MONTH_DATA_TYPE;
    }else if (control.selectedSegmentIndex == 4) {
        self.chartView.type = IL_YEAR_DATA_TYPE;
    }
}

#pragma mark ==== YMLSegmentControlDelegate =====
- (void)segmentControl:(YMLSegmentControl *)segmentControl didSelectedAtIndex:(NSInteger)index
{
   if (index == 0) {
       self.chartView.type = IL_HOUR_DATA_TYPE;
   }else if (index == 1) {
       self.chartView.type = IL_DAY_DATA_TYPE;
   }else if (index == 2) {
       self.chartView.type = IL_WEEK_DATA_TYPE;
   }else if (index == 3) {
       self.chartView.type = IL_MONTH_DATA_TYPE;
   }else if (index == 4) {
       self.chartView.type = IL_YEAR_DATA_TYPE;
   }
}

- (void)loadNewData
{
    
}

-(void)addSubviews {
    
    self.bgView = [UIView new];
    self.bgView.backgroundColor = MainColor;
    [self.view addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    UIScrollView * contentView = [UIScrollView new];
    contentView.backgroundColor = [UIColor clearColor];
    contentView.delegate = self;
    contentView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:contentView];
    
    /*
    MJRefreshNormalHeader * normalHeader = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    normalHeader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    normalHeader.stateLabel.textColor = [UIColor whiteColor];
    normalHeader.lastUpdatedTimeLabel.hidden = YES;
    contentView.mj_header = normalHeader;*/
    
    UIView * topview = [UIView new];
    topview.backgroundColor = MainColor;
    [contentView addSubview:topview];
    
    UIImageView *bgView = [UIImageView new];
    bgView.image = [UIImage imageNamed:@"topBg"];
    [topview addSubview:bgView];
    
    UIView * bottomview = [UIView new];
    bottomview.backgroundColor = [UIColor whiteColor];
    [contentView addSubview:bottomview];
    
    NSString * tipLableText = [NSString stringWithFormat:@"%@的体温",self.userModel.userName];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = tipLableText;
    titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [topview addSubview:titleLabel];
    
    NSString * temperatureNum = @"--.--℃";
    NSMutableAttributedString * attribut = [self getTempAttributedStr:temperatureNum];
    self.temperatureLabel = [[UILabel alloc]init];
    self.temperatureLabel.backgroundColor = [UIColor clearColor];
    self.temperatureLabel.font = [UIFont fontWithName:@"Digiface" size:100];
    self.temperatureLabel.textColor = [UIColor whiteColor];
    self.temperatureLabel.textAlignment = NSTextAlignmentCenter;
    self.temperatureLabel.attributedText = attribut;
    [topview addSubview:_temperatureLabel];
    
    
    BOOL isConnected = [ILBlueManager isConnected];
    
    self.connectLabel = [[UILabel alloc]init];
    self.connectLabel.text = isConnected ? @"已连接" : @"未连接";
    self.connectLabel.font = [UIFont systemFontOfSize:15];
    self.connectLabel.textColor = [UIColor whiteColor];
    [topview addSubview:self.connectLabel];
    
    self.imageViewConnect = [UIImageView new];
    self.imageViewConnect.image = [UIImage imageNamed:(isConnected ? @"connect" : @"connect_no")];
    [topview addSubview:self.imageViewConnect];
    
    self.powerLabel = [[UILabel alloc]init];
    self.powerLabel.text = @"70%";
    self.powerLabel.font = [UIFont systemFontOfSize:15];
    self.powerLabel.textColor = [UIColor whiteColor];
    [topview addSubview:self.powerLabel];
    
    UIImageView * imageViewPower = [UIImageView new];
    imageViewPower.image = [UIImage imageNamed:@"power"];
    [topview addSubview:imageViewPower];
    
    UIView * line = [UIView new];
    line.backgroundColor = LigntGrayColor;
    [bottomview addSubview:line];
        
    NSArray * arraySeg = @[@"小时",@"日",@"周",@"月",@"年"];
    /*
    UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:arraySeg];
    [segment setBackgroundImage:[UIImage imageWithColor:MainColor]
                       forState:UIControlStateSelected
                     barMetrics:UIBarMetricsDefault];
    [segment setBackgroundImage:[UIImage imageWithColor:LigntGrayColor]
                       forState:UIControlStateNormal
                     barMetrics:UIBarMetricsDefault];
    [segment setTitleTextAttributes:@{NSForegroundColorAttributeName:BlackColor} forState:(UIControlStateNormal)];
    [segment setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:(UIControlStateSelected)];
    segment.selectedSegmentIndex = 0;
    [segment addTarget:self action:@selector(segmentValueChange:) forControlEvents:(UIControlEventValueChanged)];
    [bottomview addSubview:segment];
    */
    
    YMLSegmentControl * segment = [[YMLSegmentControl alloc] initWithFrame:CGRectMake(18,16,self.view.frame.size.width-36,40)
                                                                    titles:arraySeg
                                                               selectBlock:nil];
    segment.arrangeType = ItemsArrangeByEqualWidth;
    segment.maxShowNum  = 5;
    segment.markLineHidden = YES;
    segment.sepLineColor = [UIColor clearColor];
    segment.buttonFont = [UIFont systemFontOfSize:16];
    segment.buttonBackgroundColor = LigntGrayColor;
    segment.buttonSelectColor = MainColor;
    segment.buttonNormalColor = BlackColor;
    segment.itemInter = 1;
    segment.delegate = self;
    [bottomview addSubview:segment];
    
    CGFloat chartViewHeight = self.view.frame.size.width + 100;
    CGRect frame = CGRectMake(0,62,self.view.frame.size.width,chartViewHeight);
    self.chartView = [[ILChartView alloc]initWithFrame:frame];
    self.chartView.userModel = self.userModel;
    self.chartView.type = IL_HOUR_DATA_TYPE;
    [bottomview addSubview:self.chartView];
        
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(bottomview);
        make.height.mas_equalTo(10);
    }];
    
    /*
    [segment mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line.mas_bottom).offset(10);
        make.left.equalTo(line).offset(18);
        make.centerX.equalTo(line);
        make.height.mas_equalTo(32);
    }];*/

    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.equalTo(@30);
       make.right.left.centerX.equalTo(topview);
       make.height.equalTo(@20);
    }];
    
    [self.temperatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLabel.mas_bottom).offset(20);
        make.height.equalTo(@100);
        make.left.right.equalTo(topview);
    }];
    
    [self.connectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(topview.mas_centerX).offset(-10);
        make.top.equalTo(self.temperatureLabel.mas_bottom).offset(10);
        make.height.equalTo(@20);
    }];
    
    [self.imageViewConnect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.connectLabel.mas_left).offset(-10);
        make.centerY.equalTo(self.connectLabel.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(32,32));
    }];
    
    [imageViewPower mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(32,32));
        make.left.equalTo(topview.mas_centerX).offset(10);
        make.centerY.equalTo(self.imageViewConnect.mas_centerY);
    }];
    
    [self.powerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageViewPower.mas_right).offset(10);
        make.centerY.equalTo(imageViewPower.mas_centerY);
        make.height.equalTo(@20);
    }];
    
    
    [contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        make.bottom.equalTo(bottomview).offset(0);
    }];
    
    [topview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView);
        make.left.right.equalTo(self.view);
        make.height.equalTo(self.view.mas_height).multipliedBy(0.5);
    }];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(topview);
        make.height.equalTo(@60);
    }];
    
    [bottomview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topview.mas_bottom);
        make.left.right.equalTo(topview);
        make.bottom.equalTo(self.chartView.mas_bottom);
    }];
}

- (NSMutableAttributedString *)getTempAttributedStr:(NSString *)str
{
    NSMutableAttributedString * attribut = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange pointRange = NSMakeRange(5,1);
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    dic[NSFontAttributeName] = [UIFont fontWithName:@"Digiface" size:30];
    [attribut addAttributes:dic range:pointRange];
    return attribut;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > 0) {
        self.bgView.hidden = YES;
    }else {
        self.bgView.hidden = NO;
    }
}

@end
