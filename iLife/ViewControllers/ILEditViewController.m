//
//  ILEditViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILEditViewController.h"
#import "ILEditTableViewCell.h"
#import "UITableView+CellSeparator.h"
#import "UIAlertController+Alert.h"
#import "ILButtonTableViewCell.h"
#import "ILDatePickerView.h"
#import "ILUtility.h"


@interface ILEditViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * classArray;
@property (nonatomic,strong) UIView * coverView;
@property (nonatomic,strong) ILDatePickerView * datePicker;
@end

@implementation ILEditViewController

- (void)dealloc
{
   removeNotification(self);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (UIView *)coverView
{
    if (!_coverView) {
        _coverView = [[UIView alloc]init];
        _coverView.backgroundColor = [UIColor clearColor];
        _coverView.userInteractionEnabled = YES;
        UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hiddenKeyboard)];
        [_coverView addGestureRecognizer:recognizer];
        [self.view addSubview:_coverView];
        [_coverView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.top.equalTo(@0);
            make.height.equalTo(@0);
        }];
    }
    return _coverView;
}

- (void)hiddenKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"编辑";
    addNotification(self, @selector(keyboardWillChangeFrame:), UIKeyboardWillShowNotification);
    addNotification(self, @selector(keyboardDidHide:), UIKeyboardWillHideNotification);
    
    self.classArray = @[[ILEditTableViewCell class],[ILEditTableViewCell class],
                        [ILEditTableViewCell class],[ILButtonTableViewCell class]];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = NO;
    
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark --键盘弹出
- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = self.view.frame.size.height - keyboardFrame.size.height;
    self.coverView.hidden = NO;
    [self.coverView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(height));
    }];
}

#pragma mark --键盘收回
- (void)keyboardDidHide:(NSNotification *)notification{
    self.coverView.hidden = YES;
}

- (void)buttonClick:(id)sender
{
    
}

- (void)actionClick:(id)sender
{
    if (self.userInfoModel.userName.length == 0) {
        showToast(self.view, @"用户名不能为空");
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (self.userInfoModel.pk == 0) {
        NSString * userIdStr = [ILUtility base64EncodeString:[ILUtility currentTimeStamp]];
        self.userInfoModel.userId = userIdStr;
    }
    [self.userInfoModel saveOrUpdate];
    __weak typeof(self) weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        [MBProgressHUD hideHUDForView:strongSelf.view animated:YES];
        [strongSelf.navigationController popViewControllerAnimated:YES];
    });
}

-(ILDatePickerView *)datePicker {
    if (_datePicker == nil) {
        _datePicker = [[ILDatePickerView alloc] initWithFrame:CGRectZero];
        [self.view addSubview:self.datePicker];
        [_datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _datePicker;
}

- (ILUserInfoModel *)userInfoModel
{
    if (!_userInfoModel) {
        _userInfoModel = [[ILUserInfoModel alloc]init];
        _userInfoModel.birthday = [ILUtility getCurrentDayStr];
    }
    return _userInfoModel;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.classArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Class class = self.classArray[indexPath.row];
    NSString * cellIdentifier = NSStringFromClass(class);
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
         cell = [[class alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if ([cell isKindOfClass:[ILEditTableViewCell class]]) {
        ILEditTableViewCell * editCell = (ILEditTableViewCell *)cell;
        if (indexPath.row == 0) {
            editCell.title.text = @"姓名";
            editCell.textField.placeholder = @"请输入姓名";
            editCell.textField.delegate = self;
            editCell.textField.text = self.userInfoModel.userName ? : @"";
        }else if (indexPath.row == 1) {
            editCell.title.text = @"性别";
            editCell.textField.placeholder = @"请选择性别";
            editCell.textField.delegate = self;
            editCell.textField.text = self.userInfoModel.sex == 0 ? @"男" : @"女";
        }else if (indexPath.row == 2) {
            editCell.title.text = @"生日";
            editCell.textField.placeholder = @"请选择日期";
            editCell.textField.delegate = self;
            editCell.textField.text = self.userInfoModel.birthday ? : [ILUtility getCurrentDayStr];
        }
    }else {
        ILButtonTableViewCell * buttonCell = (ILButtonTableViewCell *)cell;
        [buttonCell.addButton addTarget:self
                                 action:@selector(actionClick:)
                       forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 3) {
        return 100;
    }else {
        return 55;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    UIView * cellContentView = (UIView *)textField.superview;
    UITableViewCell * cell = (UITableViewCell *)cellContentView.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.row == 0) {
        return YES;
    }else if (indexPath.row == 1) {
        __weak typeof(self) weakSelf = self;
        UIAlertController * alertVC = [UIAlertController actionSheetWithTitle:nil message:nil dataList:@[@"男",@"女"] cancelButton:YES itemClickBlock:^(UIAlertAction *alertAction, NSInteger index) {
            __strong typeof(self) strongSelf = weakSelf;
             if (index == 0) {
                 strongSelf.userInfoModel.sex = 0;
             }else if (index == 1){
                 strongSelf.userInfoModel.sex = 1;
             }
            [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         }];
         [self presentViewController:alertVC animated:YES completion:^{
             
         }];
        return NO;
    }else if (indexPath.row == 2) {
        __weak typeof(self) weakSelf = self;
        self.datePicker.dateStr = self.userInfoModel.birthday;
        self.datePicker.dateCallback = ^(NSString *dateStr) {
            __strong typeof(self) strongSelf = weakSelf;
            strongSelf.userInfoModel.birthday = dateStr;
            [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        };
        [self.datePicker show];
        return NO;
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.userInfoModel.userName = textField.text;
}

@end
