//
//  ILFileListViewController.m
//  iLife
//
//  Created by hedongyang on 2020/8/6.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILFileListViewController.h"

@interface ILFileListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * fileList;
@end

@implementation ILFileListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.edges.equalTo(self.view);
    }];
}

- (void)setFilePath:(NSString *)filePath
{
    _filePath = filePath;
    self.fileList =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:_filePath error:nil];
    [self.tableView reloadData];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fileList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * cellIdentifier = @"cellIdentifier";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSString * name = [self.fileList objectAtIndex:indexPath.row];
    cell.textLabel.text = name;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * name = [self.fileList objectAtIndex:indexPath.row];
    NSString * dirPath = [self.filePath stringByAppendingPathComponent:name];
    BOOL isDir = NO;
    [[NSFileManager defaultManager] fileExistsAtPath:dirPath isDirectory:&isDir];
    if(isDir) { //目录有下级文件
       ILFileListViewController * fileVc = [[ILFileListViewController alloc]init];
       fileVc.filePath = dirPath;
       fileVc.title = name;
       [self.navigationController pushViewController:fileVc animated:YES];
    }else { //文件无下级文件
        postNotify(@"ILSelectFileNotice",@{@"file":dirPath});
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


@end
