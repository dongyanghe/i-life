//
//  ILConnectDeviceViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/17.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILConnectDeviceViewController.h"
#import "ILScanListTableViewController.h"
#import "ILBlueManager.h"
#import "ILScanStateView.h"
#import "ILBlueConnectView.h"
#import "ILGlobalEngineModel.h"


@interface ILConnectDeviceViewController ()<ILBlueManagerDelegate>
@property (nonatomic,strong) ILScanStateView * scanStateView;
@property (nonatomic,strong) ILScanListTableViewController * scanListTableVc;
@property (nonatomic,strong) ILBlueConnectView * connectView;
@property (nonatomic,assign) BOOL isShowList;
@end

@implementation ILConnectDeviceViewController

- (void)dealloc
{
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [ILBlueManager shareInstance].delegate = nil;
    [[ILBlueManager shareInstance] stopScan];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [ILBlueManager shareInstance].delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent=NO;
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"设备绑定";
    [self addSubViews];
}

- (void)addSubViews
{
    self.connectView = [[ILBlueConnectView alloc]init];
    self.connectView.hidden = YES;
    [self.connectView.addButton addTarget:self
                   action:@selector(buttonClick:)
         forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.connectView];
    [self.connectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.scanStateView = [[ILScanStateView alloc]init];
    self.scanStateView.userInteractionEnabled = YES;
    self.scanStateView.hidden = YES;
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reStartScan)];
    [self.scanStateView addGestureRecognizer:recognizer];
    [self.view addSubview:self.scanStateView];
    [self.scanStateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    if ([ILBlueManager isPoweredOff]) {
       self.connectView.hidden = YES;
       self.scanStateView.hidden = NO;
       self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search_fail"];
       self.scanStateView.labelStatus.text = @"添加失败";
       self.scanStateView.labelTips.text   = @"请检查设备开关是否打开";
    }else {
        [ILGlobalEngineModel shareInstance].macAddr = self.userModel.macAddr;
        [ILGlobalEngineModel shareInstance].deviceName = self.userModel.deviceName;
        [ILGlobalEngineModel shareInstance].isBind = self.userModel.bindState;
        [ILGlobalEngineModel shareInstance].userId = self.userModel.userId;
        [ILGlobalEngineModel shareInstance].uuidStr = self.userModel.uuidStr;
       if ([ILBlueManager isConnected]
           || self.userModel.bindState) {
           self.connectView.hidden = NO;
           self.scanStateView.hidden = YES;
           self.connectView.engineModel = [ILGlobalEngineModel shareInstance];
       }else {
           self.connectView.hidden = YES;
           self.scanStateView.hidden = NO;
           self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search"];
           self.scanStateView.labelStatus.text = @"正在搜索";
           self.scanStateView.labelTips.text   = @"请将设备靠近手机";
           [[ILBlueManager shareInstance] startScan];
           cancelDelayMethod(self,@selector(scanTimeout));
           addAfterDelay(self,@selector(scanTimeout),20);
       }
    }
}

- (void)scanTimeout
{
    cancelDelayMethod(self,@selector(scanTimeout));
    showToast(self.view, @"扫描超时");
    self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search_fail"];
    self.scanStateView.labelStatus.text = @"未扫描到设备";
    self.scanStateView.labelTips.text   = @"请检查设备开关是否打开";
    [[ILBlueManager shareInstance] stopScan];
}

- (void)reStartScan
{
    self.isShowList = YES;
    self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search"];
    self.scanStateView.labelStatus.text = @"正在搜索";
    self.scanStateView.labelTips.text   = @"请将设备靠近手机";
    [[ILBlueManager shareInstance] startScan];
    cancelDelayMethod(self,@selector(scanTimeout));
    addAfterDelay(self,@selector(scanTimeout),20);
}

- (void)reconnectTimeout
{
    cancelDelayMethod(self,@selector(reconnectTimeout));
    showToast(self.view, @"重连超时");
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[ILBlueManager shareInstance] stopScan];
}

- (void)buttonClick:(UIButton *)sender
{
    if (![ILBlueManager isConnected]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (self.userModel.bindState) {
            [[ILBlueManager shareInstance] startScan];
            cancelDelayMethod(self,@selector(reconnectTimeout));
            addAfterDelay(self,@selector(reconnectTimeout),20);
        }else {
            [ILBlueManager connectDeviceWithModel:[ILBlueManager shareInstance].currentModel];
        }
    }else {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [ILBlueManager cancelCurrentPeripheralConnection];
        self.userModel.bindState = NO;
        [self.userModel saveOrUpdate];
    }
}

- (ILScanListTableViewController *)scanListTableVc
{
    if (!_scanListTableVc) {
        _scanListTableVc = [[ILScanListTableViewController alloc]init];
        __weak typeof(self) weakSelf = self;
        _scanListTableVc.selectDeviceCallbak = ^(ILPeripheralModel *model) {
            __strong typeof(self) strongSelf = weakSelf;
            [ILBlueManager shareInstance].currentModel = model;
            [ILGlobalEngineModel shareInstance].macAddr = model.macAddr;
            [ILGlobalEngineModel shareInstance].uuidStr = model.uuidStr;
            [ILGlobalEngineModel shareInstance].deviceName = model.name;
            strongSelf.connectView.engineModel = [ILGlobalEngineModel shareInstance];
            strongSelf.connectView.hidden = NO;
            strongSelf.scanStateView.hidden = YES;
        };
        self.isShowList = YES;
    }
    return _scanListTableVc;
}

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
              allDevices:(nonnull NSArray<ILPeripheralModel *> *)allDevices {
    if ([ILGlobalEngineModel shareInstance].isBind) return;
    if (allDevices.count > 0) {
        cancelDelayMethod(self,@selector(scanTimeout));
        self.scanListTableVc.deviceModels = allDevices;
        if (self.isShowList) {
            self.isShowList = NO;
            UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:self.scanListTableVc];
            nav.modalPresentationStyle = UIModalPresentationFullScreen;
            [self.navigationController presentViewController:nav animated:YES completion:^{
                
            }];
        }
    }
}

- (BOOL)bluetoothManager:(nonnull ILBlueManager *)manager
           centerManager:(nonnull CBCentralManager *)centerManager
    didConnectPeripheral:(nonnull CBPeripheral *)peripheral
               isOatMode:(BOOL)isOtaMode {
    if ([ILGlobalEngineModel shareInstance].isBind) {
        showToast(self.view,@"重连成功");
        cancelDelayMethod(self,@selector(reconnectTimeout));
    }else {
        showToast(self.view,@"连接成功");
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (self.userModel) {
        self.userModel.bindState  = YES;
        self.userModel.macAddr    = manager.currentModel.macAddr;
        self.userModel.uuidStr    = manager.currentModel.uuidStr;
        self.userModel.deviceName = manager.currentModel.name;
        [self.userModel saveOrUpdate];
    }
    __weak typeof(self)weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                 (int64_t)(1 * NSEC_PER_SEC)),
                   dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    });
    return YES;
}

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
  connectPeripheralError:(nonnull NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if ([ILGlobalEngineModel shareInstance].isBind) {
        if (error.code == IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE) {
            showToast(self.view, @"设备断开");
        }else {
            showToast(self.view, @"连接失败");
        }
        __weak typeof(self)weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                     (int64_t)(1 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
            __strong typeof(self) strongSelf = weakSelf;
            [strongSelf.navigationController popViewControllerAnimated:YES];
        });
        return;
    }
    if (error.code == IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE) {
        self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search"];
        self.scanStateView.labelStatus.text = @"正在搜索";
        self.scanStateView.labelTips.text   = @"请将设备靠近手机";
        self.scanStateView.hidden = NO;
        self.connectView.hidden = YES;
        self.isShowList = YES;
        [[ILBlueManager shareInstance] startScan];
    }else {
        self.scanStateView.hidden = NO;
        self.connectView.hidden = YES;
        self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search_fail"];
        self.scanStateView.labelStatus.text = @"添加失败";
        self.scanStateView.labelTips.text   = @"请检查设备开关是否打开";
    }
}

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
          didUpdateState:(IL_BLUETOOTH_MANAGER_STATE)state {
    if (state == IL_MANAGER_STATE_POWEREDOFF) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if([ILGlobalEngineModel shareInstance].isBind){
            self.scanStateView.hidden = YES;
            self.connectView.hidden = NO;
            showToast(self.view, @"蓝牙关闭");
        }else {
           self.scanStateView.hidden = NO;
           self.connectView.hidden = YES;
           self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search_fail"];
           self.scanStateView.labelStatus.text = @"添加失败";
           self.scanStateView.labelTips.text   = @"请检查设备开关是否打开";
        }
    }else if (state == IL_MANAGER_STATE_POWEREDON) {
        if (   [ILBlueManager isConnected]
            || [ILGlobalEngineModel shareInstance].isBind) {
            self.scanStateView.hidden = YES;
            self.connectView.hidden = NO;
        }else {
            self.scanStateView.stateIcon.image  = [UIImage imageNamed:@"search"];
            self.scanStateView.labelStatus.text = @"正在搜索";
            self.scanStateView.labelTips.text   = @"请将设备靠近手机";
            self.scanStateView.hidden = NO;
            self.connectView.hidden = YES;
            self.isShowList = YES;
        }
    }
}

@end
