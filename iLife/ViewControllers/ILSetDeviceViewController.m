//
//  ILSetDeviceViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/30.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILSetDeviceViewController.h"
#import "ILSetDeviceTableViewCell.h"
#import "ILTempPickerView.h"
#import "ILUpdateViewController.h"

@interface ILSetDeviceViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSArray * dataArray;
@property (nonatomic,strong) ILTempPickerView * pickerView;
@end

@implementation ILSetDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"设备设置";
    
    self.dataArray = @[@{@"title":@"MAC地址",@"class":@"ILSetDeviceLabelTableViewCell"},
                       @{@"title":@"高温提醒",@"class":@"ILSetDeviceArrowTableViewCell"},
                       @{@"title":@"低温提醒",@"class":@"ILSetDeviceArrowTableViewCell"},
                       @{@"title":@"提醒铃声",@"class":@"ILSetDeviceSwitchTableViewCell"},
                       @{@"title":@"震动",@"class":@"ILSetDeviceSwitchTableViewCell"},
                       @{@"title":@"低电量提醒",@"class":@"ILSetDeviceSwitchTableViewCell"},
                       @{@"title":@"连接中断提醒",@"class":@"ILSetDeviceSwitchTableViewCell"},
                       @{@"title":@"设备升级",@"class":@"ILSetDeviceArrowTableViewCell"},
                       @{@"title":@"数据库分享",@"class":@"ILSetDeviceArrowTableViewCell"}];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = NO;
    self.tableView.tableFooterView = [UIView new];
    [self.view addSubview:self.tableView];

    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.edges.equalTo(self.view);
    }];
}

- (ILTempPickerView *)pickerView
{
    if (!_pickerView) {
        _pickerView = [[ILTempPickerView alloc]initWithFrame:CGRectZero];
        [self.view addSubview:_pickerView];
        [_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    return _pickerView;
}

- (void)onSwitchOffOn:(UISwitch *)switchOffOn
{
    ILSetDeviceSwitchTableViewCell * switchCell = (ILSetDeviceSwitchTableViewCell *)switchOffOn.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:switchCell];
    if (indexPath.row == 3) {
        self.setUpModel.isOpenBell = switchOffOn.isOn;
    }else if (indexPath.row == 4) {
        self.setUpModel.isVibration = switchOffOn.isOn;
    }else if (indexPath.row == 5) {
        self.setUpModel.isLowBattery = switchOffOn.isOn;
    }else if (indexPath.row == 6) {
        self.setUpModel.isConnected = switchOffOn.isOn;
    }
    if ([self.setUpModel saveOrUpdate]) {
        if (self.setUpModel.pk == 0) {
            self.setUpModel.pk = 1;
        }
    }
}

- (void)shareDb
{
    NSString * docsdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    docsdir = [docsdir stringByAppendingPathComponent:@"ILDB"];
    NSString * zipPath = [docsdir stringByAppendingPathComponent:@"ILife.zip"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:zipPath]) {
        [[NSFileManager defaultManager]removeItemAtPath:zipPath error:nil];
    }
    
    if ([SSZipArchive createZipFileAtPath:zipPath withContentsOfDirectory:docsdir]) {
        NSURL * urlToShare = [NSURL fileURLWithPath:zipPath];
        NSData * data = [NSData dataWithContentsOfFile:zipPath];
        NSArray * activityItems = @[data, urlToShare];
        UIActivityViewController * activity = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(UIActivityType __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError)
        {
            NSLog(@"调用分享的应用id :%@", activityType);
            if (completed) {
                NSLog(@"分享成功!");
            } else {
                NSLog(@"分享失败!");
            }
        };
        activity.completionWithItemsHandler = myBlock;
        [self presentViewController:activity animated:YES completion:nil];
    }else {
        showToast(self.view, @"本地无数据库");
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * dic = self.dataArray[indexPath.row];
    NSString * cellIdentifier = [dic valueForKey:@"class"];
    ILSetDeviceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        Class class = NSClassFromString(cellIdentifier);
        cell = [[class alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.titleLabel.text = [dic valueForKey:@"title"];
    if (indexPath.row == 0) {
        ILSetDeviceLabelTableViewCell * labelCell = (ILSetDeviceLabelTableViewCell *)cell;
        labelCell.subLabel.text = self.setUpModel.macAddr ? : @"";
    }else if (indexPath.row == 1) {
        ILSetDeviceArrowTableViewCell * arrowCell = (ILSetDeviceArrowTableViewCell *)cell;
        arrowCell.subLabel.text = self.setUpModel.hotTemp ? : @"";
    }else if (indexPath.row == 2) {
        ILSetDeviceArrowTableViewCell * arrowCell = (ILSetDeviceArrowTableViewCell *)cell;
        arrowCell.subLabel.text = self.setUpModel.lowTemp ? : @"";
    }else if (indexPath.row == 3) {
        ILSetDeviceSwitchTableViewCell * switchCell = (ILSetDeviceSwitchTableViewCell *)cell;
        [switchCell.switchOnOff addTarget:self action:@selector(onSwitchOffOn:) forControlEvents:UIControlEventValueChanged];
        switchCell.switchOnOff.on = self.setUpModel.isOpenBell;
    }else if (indexPath.row == 4) {
        ILSetDeviceSwitchTableViewCell * switchCell = (ILSetDeviceSwitchTableViewCell *)cell;
        [switchCell.switchOnOff addTarget:self action:@selector(onSwitchOffOn:) forControlEvents:UIControlEventValueChanged];
        switchCell.switchOnOff.on = self.setUpModel.isVibration;
    }else if (indexPath.row == 5) {
        ILSetDeviceSwitchTableViewCell * switchCell = (ILSetDeviceSwitchTableViewCell *)cell;
        [switchCell.switchOnOff addTarget:self action:@selector(onSwitchOffOn:) forControlEvents:UIControlEventValueChanged];
        switchCell.switchOnOff.on = self.setUpModel.isLowBattery;
    }else if (indexPath.row == 6) {
        ILSetDeviceSwitchTableViewCell * switchCell = (ILSetDeviceSwitchTableViewCell *)cell;
        [switchCell.switchOnOff addTarget:self action:@selector(onSwitchOffOn:) forControlEvents:UIControlEventValueChanged];
        switchCell.switchOnOff.on = self.setUpModel.isConnected;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1 || indexPath.row == 2) {
        if (indexPath.row == 1) {
            self.pickerView.tempStr = self.setUpModel.hotTemp?:@"";
        }else {
            self.pickerView.tempStr = self.setUpModel.lowTemp?:@"";
        }
        __weak typeof(self) weakSelf = self;
        self.pickerView.tempCallback = ^(NSString *tempStr) {
            if (indexPath.row == 1) {
                weakSelf.setUpModel.hotTemp = tempStr;
            }else {
                weakSelf.setUpModel.lowTemp = tempStr;
            }
            if ([weakSelf.setUpModel saveOrUpdate]) {
                if (weakSelf.setUpModel.pk == 0) {
                    weakSelf.setUpModel.pk = 1;
                }
            }
            [tableView reloadData];
        };
        [self.pickerView show];
    }else if (indexPath.row == 7) {
        ILUpdateViewController * update = [[ILUpdateViewController alloc]init];
        [self.navigationController pushViewController:update animated:YES];
    }else if (indexPath.row == 8) {
        [self shareDb];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}


@end
