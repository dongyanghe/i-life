//
//  ILScanListTableViewController.h
//  iLife
//
//  Created by hedongyang on 2020/7/20.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILPeripheralModel.h"


@interface ILScanListTableViewController : UITableViewController
@property (nonatomic,strong) NSArray <ILPeripheralModel *>* deviceModels;
@property (nonatomic,copy) void(^selectDeviceCallbak)(ILPeripheralModel * model);
@end

