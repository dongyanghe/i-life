//
//  ILHomeViewController.m
//  iLife
//
//  Created by hedongyang on 2020/7/3.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILHomeViewController.h"
#import "ILEditViewController.h"
#import "UIImage+ColorToImage.h"
#import "ILSoundManager.h"
#import "ILBlueManager.h"
#import "ILDeviceInfoModel.h"
#import "ILGlobalEngineModel.h"
#import "ILHomeTableViewCell.h"
#import "ILTempDataViewController.h"
#import "ILConnectDeviceViewController.h"
#import "ILUtility.h"

@interface ILHomeViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSMutableArray * userModels;
@end

@implementation ILHomeViewController

- (void)dealloc
{

}

- (NSMutableArray *)userModels
{
    if (!_userModels) {
        _userModels = [NSMutableArray arrayWithArray:[ILUserInfoModel queryAllModels]];
    }
    return _userModels;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];    
    UINavigationBar * navigationBar = self.navigationController.navigationBar;
    navigationBar.barTintColor = [UIColor whiteColor];
    navigationBar.tintColor = [UIColor blackColor];
    self.userModels = [NSMutableArray arrayWithArray:[ILUserInfoModel queryAllModels]];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self modificationNavigationBarStyle];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationController.navigationBar.translucent=NO;
    self.view.backgroundColor = [UIColor whiteColor];
    [self initSubviews];
    __weak typeof(self) weakSelf = self;
    [ILSoundManager shareInstance].didConnectCallback = ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.userModels = [NSMutableArray arrayWithArray:[ILUserInfoModel queryAllModels]];
        [strongSelf.tableView reloadData];
    };

   // [self saveOneYearData];
}



- (void)saveOneYearData
{
    NSString * currentDate = [ILUtility currentDateStr];
    for (int i = 0; i < 365; i++) {
        NSInteger currentTime = [currentDate integerValue] - i*60*60*24;
        [self saveOneDayDataWithTime:[NSString stringWithFormat:@"%ld",(long)currentTime]];
    }
}

- (void)saveOneDayDataWithTime:(NSString *)timeStamp
{
    for (int i = 1; i <= 1440; i++) {
        ILDeviceInfoModel * model = [[ILDeviceInfoModel alloc]init];
        model.macAddr = @"D5:95:0F:7D:EB:DB";
        model.uuidStr = @"0600260D-4DC3-8407-6423-8D31EF9E4735";
        model.userId  = @"MTU5ODk2NjA3NA==";
        model.timeStamp = [NSString stringWithFormat:@"%ld",[timeStamp integerValue] - i*60];
        model.minuteStr = [NSString stringWithFormat:@"%ld",[timeStamp integerValue] - i*60];
        model.dateStr = timeStamp;
        model.hourStr = [NSString stringWithFormat:@"%ld",[timeStamp integerValue] - (i/60)*3600];
        NSDateComponents * components = [ILUtility dateComponentsWithDateStr:timeStamp];
        model.year  = components.year;
        model.month = components.month;
        model.originalData = @"00-27-43-B7-0A-00-27-43";
        int temp = [self getRandomNumber:25 to:42];
        model.temperature1 = [NSString stringWithFormat:@"%d",temp];
        model.temperature2 = [NSString stringWithFormat:@"%d",temp];
        [model saveOrUpdate];
    }
}

- (int)getRandomNumber:(int)from to:(int)to
{
   return (int)(from + (arc4random() % (to - from + 1)));
}

- (void)modificationNavigationBarStyle
{
    UIBarButtonItem * backButtonItem = [[UIBarButtonItem alloc] init];
    backButtonItem.style = UIBarButtonItemStyleDone;
    self.navigationItem.backBarButtonItem = backButtonItem;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
}

-(void)initSubviews {
    
    CGFloat width = self.view.frame.size.width;
    UILabel * label = [[UILabel alloc]init];
    label.text = @"为哪位宝宝测量呢?";
    label.font = [UIFont systemFontOfSize:23];
    label.textColor = MainColor;
    label.frame = CGRectMake(20,0,width - 20,60);
    UIView * tableViewHeader = [UIView new];
    tableViewHeader.frame = CGRectMake(0,0,width,60);
    [tableViewHeader addSubview:label];
    
    UIView * tableViewFoot = [UIView new];
    tableViewFoot.frame = CGRectMake(0,0,width,200);
    UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,20,width,120)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:@"fly"];
    [tableViewFoot addSubview:imageView];
    
    UIButton * addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [addButton setTitle:@"+新增档案" forState:UIControlStateNormal];
    addButton.layer.masksToBounds = YES;
    addButton.layer.cornerRadius = 5.0f;
    addButton.backgroundColor = MainColor;
    addButton.tintColor = [UIColor whiteColor];
    addButton.frame = CGRectMake(20,160,width-40,40);
    [addButton addTarget:self
                  action:@selector(buttonClick:)
        forControlEvents:(UIControlEventTouchUpInside)];
    [tableViewFoot addSubview:addButton];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableHeaderView = tableViewHeader;
    self.tableView.tableFooterView = tableViewFoot;
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)buttonClick:(id)sender
{
    ILEditViewController * vc = [[ILEditViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.userModels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellIdentifier = @"cellIdentifier";
    ILHomeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
      cell = [[ILHomeTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    ILUserInfoModel * model = self.userModels[indexPath.row];
    cell.nameLable.text = model.userName;
    cell.imageViewSex.image = model.sex == 0 ? [UIImage imageNamed:@"man"]: [UIImage imageNamed:@"woman"];
    cell.birthLable.text = model.birthday;
    if ([ILBlueManager shareInstance].currentPeripheral.state == CBPeripheralStateConnected) {
        ILGlobalEngineModel * engine = [ILGlobalEngineModel shareInstance];
        if (   [engine.macAddr isEqualToString:model.macAddr]
            && [engine.userId isEqualToString:model.userId]) {
            [cell.rebindButton setTitle:@"已连接" forState:UIControlStateNormal];
        }else {
            [cell.rebindButton setTitle:@"去连接" forState:UIControlStateNormal];
        }
    }else {
        if (model.bindState == YES) {
          [cell.rebindButton setTitle:@"重新连接" forState:UIControlStateNormal];
        }else {
          [cell.rebindButton setTitle:@"去连接" forState:UIControlStateNormal];
        }
    }
    [cell.rebindButton addTarget:self action:@selector(connectDevice:) forControlEvents:UIControlEventTouchUpInside];
    [cell.editButton addTarget:self action:@selector(editUserInfo:) forControlEvents:UIControlEventTouchUpInside];
    [cell.readButton addTarget:self action:@selector(readTemp:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([ILBlueManager isConnected]) {
            showToast(self.view, @"设备连接中")
            [tableView reloadData];
           return;
        }
        ILUserInfoModel * model = self.userModels[indexPath.row];
        if ([model del]) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            __weak typeof(self) weakSelf = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
              [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
              [weakSelf.userModels removeObjectAtIndex:indexPath.row];
              [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            });
            NSString * sqlStr = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@'",model.userId,model.macAddr];
            [ILDeviceInfoModel deleteModelsWithSqlStr:sqlStr];
        }
    }
}
        
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)connectDevice:(UIButton *)sender
{
    UIView * view = sender.superview;
    UITableViewCell * cell = (UITableViewCell *)view.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    ILUserInfoModel * model = self.userModels[indexPath.row];
    ILConnectDeviceViewController * vc = [[ILConnectDeviceViewController alloc]init];
    vc.userModel = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)scanTimeout
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scanTimeout) object:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    showToast(self.view, @"扫描连接超时");
    [[ILBlueManager shareInstance] stopScan];
}

- (void)editUserInfo:(UIButton *)sender
{
    UIView * view = sender.superview;
    UITableViewCell * cell = (UITableViewCell *)view.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    ILUserInfoModel * model = self.userModels[indexPath.row];
    ILEditViewController * vc = [[ILEditViewController alloc]init];
    vc.userInfoModel = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)readTemp:(UIButton *)sender
{
    UIView * view = sender.superview;
    UITableViewCell * cell = (UITableViewCell *)view.superview;
    NSIndexPath * indexPath = [self.tableView indexPathForCell:cell];
    ILUserInfoModel * model = self.userModels[indexPath.row];
//    if (model.macAddr.length == 0) {
//        showToast(self.view, @"未绑定设备");
//        return;
//    }
    ILTempDataViewController * vc = [[ILTempDataViewController alloc]init];
    vc.userModel = model;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
