//
//  ILUpdateViewController.m
//  iLife
//
//  Created by hedongyang on 2020/8/6.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILUpdateViewController.h"
#import "ILFileListViewController.h"
#import "ILBlueManager.h"
#import "ILUtility.h"
#import "ILGlobalEngineModel.h"
#import "ILUpdateManager.h"

@interface ILUpdateViewController ()<ILBlueManagerDelegate,ILUpdateManagerDelegate>
@property (nonatomic,copy) NSString * filePath;
@property (nonatomic,strong) UIView * bgView;
@property (nonatomic,strong) UIView * progressView;
@property (nonatomic,strong) UILabel * progressLabel;
@property (nonatomic,strong) UILabel * tipLabel;
@property (nonatomic,strong) UIButton * updateButton;
@property (nonatomic,assign) BOOL isUpdate;
@end

@implementation ILUpdateViewController

- (void)dealloc
{
    removeNotification(self);
    [ILBlueManager shareInstance].delegate = nil;
    [ILUpdateManager shareInstance].delegate = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [ILUtility popGestureClose:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [ILUtility popGestureOpen:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"设备升级";
    addNotification(self,@selector(selectFileNotice:),@"ILSelectFileNotice");
    [ILBlueManager shareInstance].delegate = self;
    [ILUpdateManager shareInstance].delegate = self;
    [self addViews];
}

- (void)selectFileNotice:(NSNotification *)notice
{
    NSDictionary * dic = notice.object;
    self.filePath = [dic valueForKey:@"file"]?:@"";
    self.tipLabel.text = self.filePath;
    if ([ILBlueManager isConnected]) {
       [self.updateButton setTitle:@"开始升级" forState:UIControlStateNormal];
    }else {
       [self.updateButton setTitle:@"连接设备" forState:UIControlStateNormal];
    }
    [self.navigationController popToViewController:self animated:YES];
}

- (void)addRightButton
{
    UIBarButtonItem * rightButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"file"]
                                                                         style:UIBarButtonItemStyleDone
                                                                        target:self
                                                                        action:@selector(rightAction)];
    self.navigationItem.rightBarButtonItem = rightButtonItem;
}

- (void)rightAction
{
    ILFileListViewController * fileVc = [[ILFileListViewController alloc]init];
    NSString * dirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject];
    fileVc.filePath = dirPath;
    fileVc.title = @"文件目录";
    [self.navigationController pushViewController:fileVc animated:YES];
}


- (void)addViews
{
    UIImageView * imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"update"];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.bottom.equalTo(self.view.mas_centerY).offset(-60);
        make.width.height.equalTo(@100);
    }];
    
    self.progressLabel = [[UILabel alloc]init];
    self.progressLabel.text = @"0.0%";
    self.progressLabel.textAlignment = NSTextAlignmentCenter;
    self.progressLabel.font = [UIFont systemFontOfSize:20];
    [self.view addSubview:self.progressLabel];
    [self.progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(self.view.mas_centerY);
        make.height.equalTo(@30);
    }];
    
    self.tipLabel = [[UILabel alloc]init];
    self.tipLabel.text = @"";
    self.tipLabel.textAlignment = NSTextAlignmentLeft;
    self.tipLabel.font = [UIFont systemFontOfSize:12];
    self.tipLabel.numberOfLines = 0;
    [self.view addSubview:self.tipLabel];
    [self.tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.right.equalTo(@(-40));
        make.top.equalTo(self.progressLabel.mas_bottom);
        make.height.equalTo(@60);
    }];
    
    self.bgView = [[UIView alloc]init];
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.cornerRadius = 5;
    self.bgView.backgroundColor = UIColorWithRGBA(230.0f,230.0f,230.0f,1);
    [self.view addSubview:self.bgView];
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.right.equalTo(@(-40));
        make.height.equalTo(@50);
        make.top.equalTo(self.tipLabel.mas_bottom).offset(50);
    }];
    
    
    self.updateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([ILBlueManager isPoweredOff]) {
        self.updateButton.backgroundColor = UIColorWithRGBA(230.0f,230.0f,230.0f,1);
        [self.updateButton setTitle:@"蓝牙关闭" forState:UIControlStateNormal];
    }else {
        [self.updateButton setTitle:@"选择文件" forState:UIControlStateNormal];
        self.updateButton.backgroundColor = MainColor;
    }
    self.updateButton.layer.masksToBounds = YES;
    self.updateButton.layer.cornerRadius = 5.0f;
    self.updateButton.tintColor = [UIColor whiteColor];
    [self.updateButton addTarget:self action:@selector(startUpdate:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.updateButton];
    [self.updateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.right.equalTo(@(-40));
        make.height.equalTo(@50);
        make.top.equalTo(self.tipLabel.mas_bottom).offset(50);
    }];
    
    self.progressView = [[UIView alloc]init];
    self.progressView.clipsToBounds = YES;
    self.progressView.layer.cornerRadius = 5;
    self.progressView.backgroundColor = MainColor;
    [self.view addSubview:self.progressView];
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.width.equalTo(@0);
        make.height.equalTo(@50);
        make.top.equalTo(self.tipLabel.mas_bottom).offset(50);
    }];
    self.progressView.hidden = YES;
    
}

- (void)startUpdate:(UIButton *)sender
{
    if (![ILGlobalEngineModel shareInstance].macAddr
        || [ILGlobalEngineModel shareInstance].macAddr.length == 0) {
        showToast(self.view, @"无设备Mac地址无法升级");
        return;
    }
    if ([sender.titleLabel.text isEqualToString:@"开始升级"]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.isUpdate = YES;
        [ILUpdateManager startUpdate];
    }else if ([sender.titleLabel.text isEqualToString:@"连接设备"]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.isUpdate = NO;
        [[ILBlueManager shareInstance] startScan];
        cancelDelayMethod(self, @selector(connectTimeout));
        addAfterDelay(self, @selector(connectTimeout), 20);
    }else if ([sender.titleLabel.text isEqualToString:@"选择文件"]) {
        [self rightAction];
    }
}

- (void)connectTimeout
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[ILBlueManager shareInstance] stopScan];
    showToast(self.view, @"连接超时");
}

#pragma mark ILBlueManagerDelegate

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
              allDevices:(nonnull NSArray<ILPeripheralModel *> *)allDevices {
    if (self.isUpdate)return;
    for (ILPeripheralModel * model in allDevices) {
        if ([[ILGlobalEngineModel shareInstance].macAddr isEqualToString:model.macAddr]) {
            [ILBlueManager connectDeviceWithModel:model];
        }
    }
}

- (BOOL)bluetoothManager:(nonnull ILBlueManager *)manager
           centerManager:(nonnull CBCentralManager *)centerManager
    didConnectPeripheral:(nonnull CBPeripheral *)peripheral
               isOatMode:(BOOL)isOtaMode {
    if (self.isUpdate)return YES;
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    showToast(self.view, @"连接成功");
    cancelDelayMethod(self, @selector(connectTimeout));
    if (self.filePath.length > 0) {
        [self.updateButton setTitle:@"开始升级" forState:UIControlStateNormal];
    }else {
        [self.updateButton setTitle:@"选择文件" forState:UIControlStateNormal];
    }
    return YES;
}

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
  connectPeripheralError:(nonnull NSError *)error {
    if(self.isUpdate)return;
   [self.updateButton setTitle:@"连接设备" forState:UIControlStateNormal];
}

- (void)bluetoothManager:(nonnull ILBlueManager *)manager
          didUpdateState:(IL_BLUETOOTH_MANAGER_STATE)state {
    if (self.isUpdate) return;
    if (state == IL_MANAGER_STATE_POWEREDOFF) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        cancelDelayMethod(self, @selector(connectTimeout));
        self.updateButton.backgroundColor = UIColorWithRGBA(230.0f,230.0f,230.0f,1);
        [self.updateButton setTitle:@"蓝牙关闭" forState:UIControlStateNormal];
    }else if (state == IL_MANAGER_STATE_POWEREDON) {
        self.updateButton.backgroundColor = MainColor;
        if (self.filePath.length > 0) {
            [self.updateButton setTitle:@"连接设备" forState:UIControlStateNormal];
        }else {
            [self.updateButton setTitle:@"选择文件" forState:UIControlStateNormal];
        }
    }
}

- (NSString * _Nullable)currentPackagePathWithUpdateManager:(ILUpdateManager * _Nullable)manager {
   return  self.filePath;
}

- (void)updateManager:(ILUpdateManager * _Nullable)manager
             progress:(float)progress
              message:(NSString * _Nullable)message {
    self.progressLabel.text = [NSString stringWithFormat:@"%.2f%@",progress*100.0f,@"%"];
    CGFloat offset = (self.view.frame.size.width - 80)/100;
    CGFloat width = offset * progress * 100.0f;
    if (width >= self.view.frame.size.width - 80) {
       width = self.bgView.frame.size.width;
       [self.progressView mas_updateConstraints:^(MASConstraintMaker *make) {
           make.width.equalTo(@(width));
       }];
   }else {
       [self.progressView mas_updateConstraints:^(MASConstraintMaker *make) {
           make.width.equalTo(@(width));
       }];
   }
}

- (void)updateManager:(ILUpdateManager * _Nullable)manager state:(IL_UPDATE_STATE)state {
    
    if (   state == IL_UPDATE_START_ENTER_OTA
        || state == IL_UPDATE_DID_ENTER_OTA
        || state == IL_UPDATE_START_RECONECT_DEVICE
        || state == IL_UPDATE_DID_RECONECT_DEVICE) {
        self.updateButton.backgroundColor = UIColorWithRGBA(230.0f,230.0f,230.0f,1);
        if (state == IL_UPDATE_DID_ENTER_OTA) {
            [self.updateButton setTitle:@"成功进入ota" forState:UIControlStateNormal];
        }else if (state == IL_UPDATE_START_RECONECT_DEVICE) {
            [self.updateButton setTitle:@"开始设备重连" forState:UIControlStateNormal];
        }
    }else if (state == IL_UPDATE_STARTING) {
       [MBProgressHUD hideHUDForView:self.view animated:YES];
        showToast(self.view, @"开始固件升级");
        self.updateButton.hidden = YES;
        self.progressView.hidden = NO;
    }else if (state == IL_UPDATE_COMPLETED) {
        showToast(self.view, @"固件升级完成");
        __weak typeof(self) weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                     (int64_t)(2 * NSEC_PER_SEC)),
                       dispatch_get_main_queue(), ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        });
    }
}

- (void)updateManager:(ILUpdateManager * _Nullable)manager
          updateError:(NSError * _Nullable)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    showToast(self.view, @"固件升级失败");
    self.updateButton.hidden = NO;
    self.progressView.hidden = YES;
    self.updateButton.backgroundColor = MainColor;
    [self.updateButton setTitle:@"连接设备" forState:UIControlStateNormal];
}

@end
