//
//  ILEditViewController.h
//  iLife
//
//  Created by hedongyang on 2020/7/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILUserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ILEditViewController : UIViewController
@property (nonatomic,strong) ILUserInfoModel * userInfoModel;
@end

NS_ASSUME_NONNULL_END
