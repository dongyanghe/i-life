//
//  ILAboutUsViewController.m
//  iLife
//
//  Created by hedongyang on 2020/8/26.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILAboutUsViewController.h"

@interface ILAboutUsViewController ()

@end

@implementation ILAboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView * iconView = [[UIImageView alloc]init];
    iconView.image = [UIImage imageNamed:@"icon"];
    iconView.layer.cornerRadius = 5;
    iconView.clipsToBounds = YES;
    [self.view addSubview:iconView];
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY).offset(-120);
        make.width.height.equalTo(@(100));
    }];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString * app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    UILabel * versionLabel = [[UILabel alloc]init];
    versionLabel.font = [UIFont systemFontOfSize:18];
    versionLabel.text = app_Version;
    versionLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:versionLabel];
    [versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    
    UILabel * subText = [[UILabel alloc]init];
    subText.font = [UIFont systemFontOfSize:14];
    subText.textColor = [UIColor lightGrayColor];
    subText.textAlignment = NSTextAlignmentCenter;
//    subText.backgroundColor = [UIColor greenColor];
    subText.text = @"科技让生活更完美";
    [self.view addSubview:subText];
    [subText mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@40);
        make.right.equalTo(@(-40));
        make.top.equalTo(versionLabel.mas_bottom).offset(20);
        make.bottom.equalTo(self.view.mas_bottom).offset(-60);
    }];
    
    UILabel * tipLabel = [[UILabel alloc]init];
    tipLabel.font = [UIFont systemFontOfSize:14];
    tipLabel.textColor = [UIColor lightGrayColor];
    tipLabel.textAlignment = NSTextAlignmentCenter;
//    tipLabel.backgroundColor = [UIColor redColor];
    tipLabel.text = @"Copyright © 2020年 LeYunZhiXin. All rights reserved.";
    [self.view addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.top.equalTo(subText.mas_bottom).offset(10);
        make.bottom.equalTo(self.view.mas_bottom).offset(-30);
    }];
}

@end
