//
//  AppDelegate.m
//  iLife
//
//  Created by hedongyang on 2020/7/3.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "AppDelegate.h"
#import "ILHomeViewController.h"
#import "ILSetupViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [NSThread sleepForTimeInterval:3];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    NSArray * baseImgs = @[@"icon_home_n", @"icon_setting_n"];
    NSArray * selectImgs = @[@"icon_home_s",@"icon_setting_s"];
    
    ILHomeViewController * homeVC  = [[ILHomeViewController alloc] init];
    UINavigationController * nav1 = [[UINavigationController alloc]initWithRootViewController:homeVC];
    ILSetupViewController * setupVC = [[ILSetupViewController alloc]init];
    UINavigationController * nav2 = [[UINavigationController alloc]initWithRootViewController:setupVC];
    NSArray<UINavigationController *> * navArray = @[nav1, nav2];
    NSArray * titles = @[@"档案管理", @"设置"];
    for (int i = 0; i < navArray.count; i++)
   {
       UIImage * bImg = [UIImage imageNamed:baseImgs[i]];
       UIImage * selectImg = [UIImage imageNamed:selectImgs[i]];
       UITabBarItem * item = [[UITabBarItem alloc] initWithTitle:titles[i] image:bImg selectedImage:selectImg];
       item.selectedImage = [item.selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
       item.image = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
       item.tag = i;
       [navArray[i] setTabBarItem:item];
       UINavigationController * nav =  navArray[i];
       [nav.viewControllers firstObject].title = titles[i];
   }
    UITabBarController * tabVC = [[UITabBarController alloc] init];
    tabVC.viewControllers = navArray;
    tabVC.tabBar.tintColor = [UIColor colorWithRed:51/255.0 green:204/255.0 blue:203/255.0 alpha:1.0];
    tabVC.tabBar.barTintColor = [UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:1.0];
    tabVC.selectedIndex = 0;
    self.window.rootViewController = tabVC;
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    NSString * docsdirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES) lastObject];
    if (url != nil) {
        NSString *path = [url absoluteString];
        path = [path stringByRemovingPercentEncoding];
        NSMutableString *string = [[NSMutableString alloc] initWithString:path];
        if ([path hasPrefix:@"file:///private"]) {
            [string replaceOccurrencesOfString:@"file:///private" withString:@"" options:NSCaseInsensitiveSearch  range:NSMakeRange(0, path.length)];
        }
        NSArray  * tempArray  = [string componentsSeparatedByString:@"/"];
        NSString * fileName   = tempArray.lastObject;
        NSFileManager * fileManager = [NSFileManager defaultManager];
        NSString * filePath = [docsdirPath stringByAppendingPathComponent:fileName];
        if ([fileManager fileExistsAtPath:filePath]) {
            [fileManager removeItemAtPath:filePath error:nil];
        }
        [fileManager copyItemAtPath:string toPath:filePath error:nil];
    }
    return  YES;
}

@end
