//
//  ILMonthChartLayerModel.h
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILDeviceInfoModel.h"

@interface ILMonthChartLayerModel : NSObject

- (instancetype)initWithModels:(NSArray <NSArray<ILDeviceInfoModel *> *>*)models;

@property (nonatomic,assign,readonly) CGRect chartViewFrame;

@property (nonatomic,assign) CGRect showLineFrame;

@property (nonatomic,assign) BOOL isShow;

@property (nonatomic,strong,readonly) NSArray<NSString *> * verticalLines;

@property (nonatomic,strong,readonly) NSArray<NSString *> * horizontalLines;

@property (nonatomic,strong,readonly) NSArray<NSString *> * numberFrames;

@property (nonatomic,strong,readonly) NSArray<NSString *> * numberTexts;

@property (nonatomic,strong,readonly) NSArray<NSString *> * timerFrames;

@property (nonatomic,strong,readonly) NSArray<NSString *> * timerTexts;

@property (nonatomic,strong,readonly) NSArray<NSString *> * tempFrames;

@property (nonatomic,strong,readonly) NSArray<NSDictionary *> * tempDatas;

@property (nonatomic,strong,readonly) UIColor * textColor;

@property (nonatomic,strong,readonly) UIColor * pointColor;

@property (nonatomic,strong,readonly) UIFont * tempFont;

@property (nonatomic,strong,readonly) UIFont * timerFont;
@end

