//
//  ILMonthChartLayerModel.m
//  iLife
//
//  Created by hedongyang on 2020/9/9.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILMonthChartLayerModel.h"
#import "ILUtility.h"

@implementation ILMonthChartLayerModel

- (instancetype)initWithModels:(NSArray <NSArray<ILDeviceInfoModel *> *>*)models
{
    self = [super init];
    if (self) {
        
        _timerFont   = [UIFont systemFontOfSize:12];
        _tempFont    = [UIFont systemFontOfSize:12];
        _textColor   = [UIColor lightGrayColor];
        _pointColor  = MainColor;
       
        UIWindow * window = [UIApplication sharedApplication].delegate.window;
        CGFloat width = window.frame.size.width;
        CGFloat height = window.frame.size.width;
        _chartViewFrame = CGRectMake(0, 0, width, height);
        
        NSArray <ILDeviceInfoModel *>* firstDayModels = [models firstObject];
        NSDateComponents * components = [ILUtility currentComponents];
        NSInteger dayLength = [ILUtility getDaysInMonthWithYear:components.year month:components.month];
        if (firstDayModels.count > 0) {
            ILDeviceInfoModel * firstInfoModel = [firstDayModels firstObject];
            components = [ILUtility dateComponentsWithDateStr:firstInfoModel.dateStr];
            dayLength = [ILUtility getDaysInMonthWithYear:components.year month:components.month];
        }
        
        CGFloat chartWidth = width - 16 - 40;
        CGFloat h_offset = chartWidth/dayLength;
        CGFloat line_w = 1.0/[UIScreen mainScreen].scale;
        CGFloat v_offset = height/8.0f;
        CGFloat chartHeight = height - v_offset;
        v_offset = chartHeight/8.0f;
        chartHeight = chartHeight - v_offset;
        
        _showLineFrame = CGRectMake(0,0,line_w,chartHeight+v_offset/2);
       
        NSMutableArray * verticaLines = [NSMutableArray array];
        for (int i = 0; i < 6; i++) {
            if (dayLength <= 28 && i >= 5)continue;
            CGRect frame = CGRectMake(i*7*h_offset+16,v_offset/2,line_w,chartHeight);
            if (i >= 5) {
                frame = CGRectMake(dayLength*h_offset+16,v_offset/2,line_w,chartHeight);
            }
           [verticaLines addObject:NSStringFromCGRect(frame)];
        }
       _verticalLines = verticaLines;
       
       NSMutableArray * horizontalLines = [NSMutableArray array];
       for (int i = 0; i < 8; i++) {
           CGRect frame = CGRectMake(16,v_offset*i + v_offset/2,chartWidth,line_w);
           [horizontalLines addObject:NSStringFromCGRect(frame)];
       }
       _horizontalLines = horizontalLines;
    
       _numberTexts = @[@"42",@"41",@"40",@"39",@"38",@"37",@"36",@"0"];
       
       NSMutableArray * numberFrames = [NSMutableArray array];
       for (int i = 0; i < _numberTexts.count; i++) {
           CGRect frame = CGRectMake(chartWidth+15,v_offset*i-6 + v_offset/2,20,20);
           if (i == 0) {
               frame = CGRectMake(chartWidth+15,v_offset*i + v_offset/2,20,20);
           }else if(i == 7) {
               frame = CGRectMake(chartWidth+15,v_offset*i-12 + v_offset/2,20,20);
           }
           [numberFrames addObject:NSStringFromCGRect(frame)];
       }
       _numberFrames = numberFrames;
            
       NSMutableArray * timerTexts = [NSMutableArray array];
       NSMutableArray * timerFrames = [NSMutableArray array];
        for (int i = 0; i < 6; i++) {
           if (dayLength <= 28 && i >= 5)continue;
           NSString * text = [NSString stringWithFormat:@"%ld日",i >= 5 ? (long)dayLength : (i == 0 ? 1 : (long)i*7)];
           CGFloat textWidth = [ILUtility getWidthWithText:text height:25 fontSize:_timerFont];
           CGRect frame = CGRectMake(i*7*h_offset+16 - textWidth/2,chartHeight + v_offset/2,textWidth,25);
           if (i >= 5) {
               frame = CGRectMake(dayLength*h_offset+16,chartHeight + v_offset/2,textWidth,chartHeight);
           }else if(i == 4 && dayLength == 29) {
               frame = CGRectMake(i*7*h_offset+16 - textWidth,chartHeight + v_offset/2,textWidth,chartHeight);
           }
           [timerFrames addObject:NSStringFromCGRect(frame)];
           [timerTexts addObject:text];
       }
        _timerFrames = timerFrames;
        _timerTexts  = timerTexts;
        
        CGFloat oneDayOffset = (width - 16 - 40)/dayLength;
        CGFloat oneTempOffset0 = v_offset/36.0f;
        CGFloat oneTempOffset1 = chartHeight/7.0f;
        NSMutableArray * tempFrames = [NSMutableArray array];
        NSMutableArray * tempDatas  = [NSMutableArray array];
        NSMutableArray * valueArray = [NSMutableArray array];
        
        for (int i = 0; i < models.count; i++) {
            NSArray <ILDeviceInfoModel *>* oneDayModels = models[i];
            for (ILDeviceInfoModel * model in oneDayModels) {
                if ([model.temperature1 floatValue] > 42) model.temperature1 = @"42";
                if ([model.temperature1 floatValue] < 0) model.temperature1 = @"0";
                [valueArray addObject:@([model.temperature1 floatValue])];
            }
            if (valueArray.count > 0) {
                NSPredicate * peredicate = [NSPredicate predicateWithFormat:@"SELF > 0"];
                NSArray * newArray = [valueArray filteredArrayUsingPredicate:peredicate];
                CGFloat maxValue = [[newArray valueForKeyPath:@"@max.floatValue"] floatValue];
                CGFloat minValue = [[newArray valueForKeyPath:@"@min.floatValue"] floatValue];
                if (maxValue > 0) {
                    CGFloat x_0 = oneDayOffset * i + 16;
                    CGFloat y_0 = oneTempOffset1 * (42 - maxValue);
                    CGFloat x_1 = oneDayOffset * (i + 1) + 16;
                    CGFloat y_1 = oneTempOffset1 * (42 - minValue);
                    if (maxValue < 36) {
                        y_0 = (chartHeight - v_offset) + oneTempOffset0*maxValue;
                    }
                    if (minValue < 36) {
                        y_1 = (chartHeight - v_offset) + oneTempOffset0*minValue;
                    }
                    CGFloat min_height = 1.0;
                    CGFloat tempHeight = (y_1 - y_0) > min_height ? (y_1 - y_0) : min_height;
                    CGRect frame = CGRectMake(x_0,y_0,(x_1-x_0)*0.5,tempHeight);
                    CGRect dragFrame = CGRectMake(x_0,0,(x_1-x_0)*0.5,chartHeight);
                    ILDeviceInfoModel * firstModel = [oneDayModels firstObject];
                    NSDateComponents * components1 = [ILUtility dateComponentsWithDateStr:firstModel.dateStr];
                    NSString * startTime = [NSString stringWithFormat:@"%02ld:%02ld",(long)0,(long)0];
                    NSString * endTime = [NSString stringWithFormat:@"%02ld:%02ld",(long)23,(long)59];
                    NSString * dateStr = [NSString stringWithFormat:@"%04ld-%02ld-%02ld",(long)components1.year,
                                          (long)components1.month,(long)components1.day];
                    NSDictionary * dic = @{@"frame":NSStringFromCGRect(dragFrame),
                                           @"date":dateStr,
                                           @"max":@(maxValue),
                                           @"min":@(minValue),
                                           @"startTime":startTime,
                                           @"endTime":endTime,
                                           @"title":@"温度"};
                    [tempDatas addObject:dic];
                    [tempFrames addObject:NSStringFromCGRect(frame)];
                }
                [valueArray removeAllObjects];
            }
        }
        _tempFrames = tempFrames;
        _tempDatas = tempDatas;
    }
    return self;
}

@end
