//
//  UITableView+CellSeparator.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "UITableView+CellSeparator.h"

@implementation UITableView (CellSeparator)

- (void)separatorInsetsZero:(UITableViewCell *)cell
{
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

@end
