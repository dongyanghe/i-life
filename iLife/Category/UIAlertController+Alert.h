//
//  UIAlertController+Alert.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ItemClickBlock)(UIAlertAction *alertAction,NSInteger index);

@interface UIAlertController (Alert)

+(UIAlertController *)actionSheetWithTitle:(NSString *)title
                                   message:(NSString *)message
                                  dataList:(NSArray *)list
                              cancelButton:(BOOL)hasButton itemClickBlock:(ItemClickBlock)itemClickBlock;

@end


