//
//  UITableView+CellSeparator.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (CellSeparator)
- (void)separatorInsetsZero:(UITableViewCell *)cell;
@end

NS_ASSUME_NONNULL_END
