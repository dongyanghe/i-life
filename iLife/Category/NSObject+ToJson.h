//
//  NSObject+ToJsonStr.h
//
//  Created by hedongyang on 2018/8/14.
//  Copyright © 2018年 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ToJson)
- (NSString*)objectToJsonString;
@end
