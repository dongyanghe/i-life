//
//  UIImage+ColorToImage.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (ColorToImage)

+ (UIImage*)imageWithColor:(UIColor*)color;

- (UIImage *)newImageWithColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
