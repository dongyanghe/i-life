//
//  NSObject+ModelToDic.h
//
//  Created by hedongyang on 2018/7/21.
//  Copyright © 2018年 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ToDic)
- (NSDictionary *)dicFromObject;
@end
