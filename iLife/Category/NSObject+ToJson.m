//
//  NSObject+ObjectToJsonStr.m
//
//  Created by hedongyang on 2018/8/14.
//  Copyright © 2018年 hedongyang. All rights reserved.
//

#import "NSObject+ToJson.h"

@implementation NSObject (ToJson)
- (NSString*)objectToJsonString
{
    NSString * jsonString = @"";
    if (   ![self isKindOfClass:[NSDictionary class]]
        && ![self isKindOfClass:[NSArray class]]) return jsonString;
    NSError * error;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}
@end
