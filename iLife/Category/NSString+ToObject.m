//
//  NSString+ToObject.m
//
//  Created by hedongyang on 2018/8/15.
//  Copyright © 2018年 hedongyang. All rights reserved.
//

#import "NSString+ToObject.h"

@implementation NSString (ToObject)
- (id)stringToObject{
    if (!self || self.length == 0) return @"";
    if (   [self rangeOfString:@"{"].location == NSNotFound
        && [self rangeOfString:@"["].location == NSNotFound) return self;
    id tmp = [NSJSONSerialization JSONObjectWithData:[self dataUsingEncoding:NSUTF8StringEncoding]
                                             options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers
                                               error:nil];
    if (!tmp)return self;
    return tmp;
}
@end
