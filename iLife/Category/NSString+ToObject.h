//
//  NSString+ToObject.h
//
//  Created by hedongyang on 2018/8/15.
//  Copyright © 2018年 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ToObject)
- (id)stringToObject;
@end
