//
//  UIAlertController+Alert.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "UIAlertController+Alert.h"

@implementation UIAlertController (Alert)

+(UIAlertController *)actionSheetWithTitle:(NSString *)title
                                   message:(NSString *)message
                                  dataList:(NSArray *)list
                              cancelButton:(BOOL)hasButton itemClickBlock:(ItemClickBlock)itemClickBlock
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:(UIAlertControllerStyleActionSheet)];
    for (NSInteger i = 0; i<list.count; i++) {
        NSString *item = list[i];
        UIAlertAction * alertAction = [UIAlertAction actionWithTitle:item style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            if (itemClickBlock) {
                itemClickBlock(action,i);
            }
        }];
        
        //修改按钮颜色
        @try {
            [alertAction setValue:MainColor forKey:@"_titleTextColor"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        [alertController addAction:alertAction];
    }
    
    if (hasButton) {
        UIAlertAction *alertActionCancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
            if (itemClickBlock) {
                itemClickBlock(action,-1);
            }
        }];
        //修改按钮颜色
        @try {
            [alertActionCancel setValue:MainColor forKey:@"_titleTextColor"];
        } @catch (NSException *exception) {
            
        } @finally {
            
        }
        [alertController addAction:alertActionCancel];
    }
    return alertController;
}

@end
