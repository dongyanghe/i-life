//
//  ILDBManager.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ILDBManager : NSObject

@property (nonatomic,strong,nullable) FMDatabaseQueue * dbQueue;

+ (NSString *)getTableNameWithClass:(Class)className;

+ (BOOL)createTableWithClass:(Class)className;

+ (NSDictionary *)getPropertysWithClass:(Class)class
                               isOrigin:(BOOL)isOrigin;

+ (NSArray <ILBaseModel *>*)queryAllColumnWithClass:(Class)className
                                           queryStr:(NSString *)queryStr;

+ (NSArray *)queryAsColumnElementWithClass:(Class)className
                                  queryStr:(NSString *)queryStr;

+ (NSArray <ILBaseModel *>*)getOneTableAllDataWithClassName:(Class)className;

+ (BOOL)saveWithModel:(ILBaseModel*)model;

+ (BOOL)updateWithModel:(ILBaseModel *)model;

+ (BOOL)delWithModel:(ILBaseModel *)model;

+ (BOOL)deleteCurrentTableWithClass:(Class)className;

+ (BOOL)deleteDataWithClass:(Class)className
                   queryStr:(NSString *)queryStr;

@end

NS_ASSUME_NONNULL_END
