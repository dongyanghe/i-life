//
//  ILDBManager.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <objc/runtime.h>
#import "ILDBManager.h"
#import "ILUtility.h"
#import "NSObject+ToDic.h"
#import "NSObject+ToJson.h"
#import "NSString+ToObject.h"

#define SQLTEXT     @"TEXT"
#define SQLINTEGER  @"INTEGER"
#define SQLREAL     @"REAL"
#define SQLBLOB     @"BLOB"
#define SQLNOTNULL  @"NOT NULL"
#define PrimaryKey  @"PRIMARY KEY AUTOINCREMENT"
#define PrimaryId   @"pk"

@interface ILDBManager ()
@property (nonatomic,copy,nullable) NSDictionary * allTableNames;
@end

@implementation ILDBManager

static ILDBManager * manager = nil;
+ (__kindof ILDBManager *)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        manager = [[super allocWithZone:NULL] init] ;
    }) ;
    return manager;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    return [ILDBManager shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [ILDBManager shareInstance];
}

- (FMDatabaseQueue *)dbQueue
{
    if (!_dbQueue) {
        _dbQueue = [[FMDatabaseQueue alloc]initWithPath:[self dbPath]];
    }
    return _dbQueue;
}

- (NSString *)dbPath
{
    NSString * docsdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    docsdir = [docsdir stringByAppendingPathComponent:@"ILDB"];
    NSError * error;
    [ILUtility createDirectoryWithPath:docsdir error:&error];
    NSString * dbpath = [docsdir stringByAppendingPathComponent:@"ILife.db"];
    return dbpath;
}

+ (NSString *)getTableNameWithClass:(Class)className
{
    ILDBManager * dbManager = [ILDBManager shareInstance];
    if (!dbManager.allTableNames) {
        dbManager.allTableNames = @{
                                     @"ILUserInfoModel"   : @"user_info_table",
                                     @"ILDeviceInfoModel" : @"device_info_table",
                                     @"ILSetUpInfoModel"  : @"set_up_info_table"
                                   };
    }
    NSString * classStr  = NSStringFromClass(className);
    NSString * tableName = [dbManager.allTableNames valueForKey:classStr];
    return tableName;
}

+ (NSArray <ILBaseModel *>*)getOneTableAllDataWithClassName:(Class)className
{
     __block NSMutableArray * oneTableArray = [NSMutableArray array];
    NSString * tableName = [ILDBManager getTableNameWithClass:className];
    if (tableName.length == 0)return oneTableArray;
    NSString * sqlStr = [NSString stringWithFormat:@"SELECT * FROM %@",tableName];
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        FMResultSet * resultSet = [db executeQuery:sqlStr];
        while ([resultSet next]) {
            ILBaseModel * model = [className new];
            for (int i = 0; i < model.originColumnNames.count; i++) {
                NSString * originColumeName = [model.originColumnNames objectAtIndex:i];
                NSString * columeName = [model.columnNames objectAtIndex:i];
                NSString * columeType = [model.columnTypes objectAtIndex:i];
                if ([columeType isEqualToString:SQLTEXT]) {
                    NSString * resultSetStr = [resultSet stringForColumn:columeName];
                    id value = [resultSetStr stringToObject];
                    [model setValue:value forKey:originColumeName];
                } else {
                    NSNumber * value = [NSNumber numberWithLongLong:[resultSet longLongIntForColumn:columeName]];
                    [model setValue:value forKey:originColumeName];
                }
            }
            [oneTableArray addObject:model];
        }
    }];
    return oneTableArray;
}

+ (NSArray *)queryAsColumnElementWithClass:(Class)className
                                  queryStr:(NSString *)queryStr
{
    NSString * tableName = [ILDBManager getTableNameWithClass:className];
    if (tableName.length == 0 || queryStr.length == 0)return [NSMutableArray array];
    return [ILDBManager queryAllColumnWithTableName:tableName className:className sqlStr:queryStr];
}

+ (NSArray <ILBaseModel *>*)queryAllColumnWithClass:(Class)className
                                           queryStr:(NSString *)queryStr
{
    NSString * tableName = [ILDBManager getTableNameWithClass:className];
    if (tableName.length == 0 || queryStr.length == 0)return [NSMutableArray array];
    NSString * sqlStr = [NSString stringWithFormat:@"SELECT * FROM %@ %@",tableName?:@"",queryStr?:@""];
    return [ILDBManager queryAllColumnWithTableName:tableName className:className sqlStr:sqlStr];
}

+ (NSArray <ILBaseModel *>*)queryAllColumnWithTableName:(NSString *)tableName
                                              className:(Class)className
                                                 sqlStr:(NSString *)sqlStr
{
    __block NSMutableArray * objects = [NSMutableArray array];
    if ([sqlStr rangeOfString:@"%"].location != NSNotFound)return objects;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (tableName) {
        if ([db tableExists:tableName]) {
            FMResultSet * resultSet = [db executeQuery:sqlStr];
            while ([resultSet next]) {
                ILBaseModel * model = [className new];
                for (int i = 0; i < model.originColumnNames.count; i++) {
                    NSString * originColumeName = [model.originColumnNames objectAtIndex:i];
                    NSString * columeName = [model.columnNames objectAtIndex:i];
                    NSString * columeType = [model.columnTypes objectAtIndex:i];
                    if ([columeType isEqualToString:SQLTEXT]) {
                        NSString * resultSetStr = [resultSet stringForColumn:columeName];
                        id value = [resultSetStr stringToObject];
                        [model setValue:value forKey:originColumeName];
                    } else {
                        NSNumber * value = [NSNumber numberWithLongLong:[resultSet longLongIntForColumn:columeName]];
                        [model setValue:value forKey:originColumeName];
                    }
                }
                [objects addObject:model];
            }
          }
        }
    }];
    return objects;
}

+ (BOOL)createTableWithClass:(Class)className
{
    __block BOOL res = YES;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    __weak typeof(self) weakSelf = self;
    [dbManager.dbQueue inTransaction:^(FMDatabase * db, BOOL * rollback) {
            __strong typeof(self) strongSelf = weakSelf;
        NSString * tableName = [ILDBManager getTableNameWithClass:className];
        if (tableName) {
            if ([db tableExists:tableName]){
                //更新字段
                NSDictionary * dic = [[self class]getPropertysWithClass:className isOrigin:NO];
                NSArray * proNames = dic[@"name"];
                NSArray * proTypes = dic[@"type"];
                for (int i = 0; i < proNames.count; i++) {
                    NSString * proName = [proNames objectAtIndex:i];
                    NSString * proType = [proTypes objectAtIndex:i];
                    if (![db columnExists:proName inTableWithName:tableName]) {
                        NSString * addField = [NSString stringWithFormat:@"%@ %@",proName,proType];
                        NSString * fieldSql = @"";
                        if ([proType isEqualToString:SQLTEXT]) {
                           fieldSql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ DEFAULT('')",tableName,addField];
                        }else {
                           fieldSql = [NSString stringWithFormat:@"ALTER TABLE %@ ADD COLUMN %@ DEFAULT(0)",tableName,addField];
                        }
                        if (![db executeUpdate:fieldSql]) {
                            res = NO;
                            *rollback = YES;
                        }
                    }
                }
            }else {
                //创建表
                NSString * propertyStr = [[strongSelf class]propertysToStringWithClass:className];
                NSString * tableSql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (%@)",tableName,propertyStr];
                if (![db executeUpdate:tableSql]) {//创建表
                    res = NO;
                    *rollback = YES;
                }
            }
        }else {
            res = NO;
            *rollback = YES;
        }
    }];
    return res;
}

+ (BOOL)deleteCurrentTableWithClass:(Class)className
{
    NSString * tableName = [ILDBManager getTableNameWithClass:className];
    NSString * sqlStr = [NSString stringWithFormat:@"DROP TABLE %@",tableName];
    __block BOOL res = YES;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (tableName){
            res = [db executeUpdate:sqlStr];
            if (!res) {
                *rollback = YES;
            }
        }else {
            res = NO;
            *rollback = YES;
        }
    }];
    return res;
}

+ (NSString *)propertysToStringWithClass:(Class)className
{
    NSMutableString * parStr = [NSMutableString string];
    NSDictionary * dic = [self getPropertysWithClass:className isOrigin:NO];
    NSArray * proNames = dic[@"name"];
    NSArray * proTypes = dic[@"type"];
    if (proTypes.count != proNames.count) {
        return parStr;
    }
    for (int i = 0; i < proNames.count; i++) {
        [parStr appendFormat:@"%@ %@ %@",[proNames objectAtIndex:i],[proTypes objectAtIndex:i],SQLNOTNULL];
        if (i < proNames.count - 1) {
            [parStr appendString:@","];
        }
    }
    return parStr;
}

+ (NSDictionary *)getPropertysWithClass:(Class)class
                               isOrigin:(BOOL)isOrigin
{
    NSMutableArray * proNames = [NSMutableArray array];
    NSMutableArray * proTypes = [NSMutableArray array];
    [[self class]getPropertysWithClass:class propertyNames:&proNames propertyTypes:&proTypes isOrigin:isOrigin];
    [[self class]getPropertysWithClass:[[class class] superclass] propertyNames:&proNames propertyTypes:&proTypes isOrigin:isOrigin];
    [proNames addObject:PrimaryId];
    NSArray * newProNames = [[proNames reverseObjectEnumerator] allObjects];
    [proTypes addObject:[NSString stringWithFormat:@"%@ %@",SQLINTEGER,PrimaryKey]];
    NSArray * newProTypes = [[proTypes reverseObjectEnumerator] allObjects];
    return [NSDictionary dictionaryWithObjectsAndKeys:newProNames,@"name",newProTypes,@"type",nil];
}

+ (void)getPropertysWithClass:(Class)modelClass
                propertyNames:(NSMutableArray **)propertyNames
                propertyTypes:(NSMutableArray **)propertyTypes
                     isOrigin:(BOOL)isOrigin
{
    unsigned int outCount, i;
    objc_property_t * properties = class_copyPropertyList(modelClass, &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString * propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        if (   [propertyName isEqualToString:@"columnNames"]
            || [propertyName isEqualToString:@"originColumnNames"]
            || [propertyName isEqualToString:@"columnTypes"]
            || [propertyName isEqualToString:@"pk"]
            || [propertyName isEqualToString:@"cmd"]
            || [propertyName isEqualToString:@"key"]) {
            continue;
        }
        NSString * newPropertyName = isOrigin ? propertyName : [ILUtility snakeStyleStringFromHumpStyleString:propertyName];
        [*propertyNames addObject:newPropertyName];
        NSString * propertyType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
        /*
         各种符号对应类型，部分类型在新版SDK中有所变化，如long 和long long
         c char         C unsigned char
         i int          I unsigned int
         l long         L unsigned long
         s short        S unsigned short
         d double       D unsigned double
         f float        F unsigned float
         q long long    Q unsigned long long
         B BOOL
         @ 对象类型 //指针 对象类型 如NSString 是@“NSString”
         64位下long 和long long 都是Tq
         SQLite 默认支持五种数据类型TEXT、INTEGER、REAL、BLOB、NULL
         因为在项目中用的类型不多，故只考虑了少数类型
         */
        if ([propertyType hasPrefix:@"T@"]) {
            [*propertyTypes addObject:SQLTEXT];
        }else if(  [propertyType hasPrefix:@"Ti"]
                 ||[propertyType hasPrefix:@"TI"]
                 ||[propertyType hasPrefix:@"Ts"]
                 ||[propertyType hasPrefix:@"TS"]
                 ||[propertyType hasPrefix:@"TB"]) {
            [*propertyTypes addObject:SQLINTEGER];
        } else {
            [*propertyTypes addObject:SQLREAL];
        }
    }
    free(properties);
}

+ (BOOL)saveWithModel:(ILBaseModel*)model
{
    NSString * tableName = [ILDBManager getTableNameWithClass:[model class]];
    if (!tableName)return NO;
    NSMutableString * keyString    = [NSMutableString string];
    NSMutableString * valueString  = [NSMutableString string];
    NSMutableArray  * insertValues = [NSMutableArray  array];
    [ILDBManager getInsertStrWithModel:model
                                keyStr:&keyString
                              valueStr:&valueString
                          insertValues:&insertValues];
    __block BOOL res = YES;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@(%@) VALUES (%@);",tableName,keyString,valueString];
        res = [db executeUpdate:sql withArgumentsInArray:insertValues];
        if (!res) {
            *rollback = YES;
        }
    }];
    return res;
}

+ (BOOL)updateWithModel:(ILBaseModel *)model
{
    __block BOOL res = YES;
    NSString * tableName = [ILDBManager getTableNameWithClass:[model class]];
    if (tableName) {
        id idValue = [model valueForKey:PrimaryId];
        if (!idValue || idValue <= 0){
            res = NO;
        }else {
            NSMutableString * keyString    = [NSMutableString string];
            NSMutableArray  * updateValues = [NSMutableArray array];
            [ILDBManager getUpdateStrWithModel:model keyStr:&keyString updateValues:&updateValues];
            ILDBManager * dbManager = [ILDBManager shareInstance];
            [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                NSString * sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE %@ = ?",tableName,keyString,PrimaryId];
                [updateValues addObject:idValue];
                res = [db executeUpdate:sql withArgumentsInArray:updateValues];
                if (!res) {
                    *rollback = YES;
                }
            }];
        }
    }else {
        res = NO;
    }
    return res;
}

+ (BOOL)delWithModel:(ILBaseModel *)model
{
    __block BOOL res = YES;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        NSString * tableName = [ILDBManager getTableNameWithClass:[model class]];
        if (tableName){
            NSString * sql = @"";
            sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE pk = %ld",tableName,(long)model.pk];
            res = [db executeUpdate:sql];
            if (!res) {
                *rollback = YES;
            }
        }else {
            res = NO;
            *rollback = YES;
        }
    }];
    return res;
}

+ (BOOL)deleteDataWithClass:(Class)className
                   queryStr:(NSString *)queryStr
{
    NSString * tableName = [ILDBManager getTableNameWithClass:className];
    if (tableName.length == 0 || queryStr.length == 0)return NO;
    NSString * sqlStr = [NSString stringWithFormat:@"DELETE FROM %@ %@",tableName?:@"",queryStr?:@""];
    __block BOOL res = YES;
    ILDBManager * dbManager = [ILDBManager shareInstance];
    [dbManager.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (tableName){
            res = [db executeUpdate:sqlStr];
            if (!res) {
                *rollback = YES;
            }
        }else {
            res = NO;
            *rollback = YES;
        }
    }];
    return res;
}

+ (void)getUpdateStrWithModel:(ILBaseModel *)model
                       keyStr:(NSMutableString **)keyStr
                 updateValues:(NSMutableArray **)updateValues
{
    for (int i = 0; i < model.columnNames.count; i++) {
        NSString * proName = [model.columnNames objectAtIndex:i];
        if ([proName isEqualToString:PrimaryId]) continue;
        if(keyStr)[*keyStr appendFormat:@" %@=?", proName];
        if (i > 0 && i < model.columnNames.count - 1) {
             if(keyStr)[*keyStr appendString:@","];
        }
    }
    for (int i = 0; i < model.originColumnNames.count; i++) {
        NSString * proName = [model.originColumnNames objectAtIndex:i];
        if ([proName isEqualToString:PrimaryId]) continue;
        id value = [model valueForKey:proName];
        if (!value) {
            value = @"";
        }else if ([value isKindOfClass:[NSDictionary class]]) {
            value = [value objectToJsonString];
        }
        else if ([value isKindOfClass:[NSArray class]]) {
            NSArray * array = (NSArray *)value;
            if([[array firstObject]isKindOfClass:[ILBaseModel class]]) {
                value = @"";
            }else {
                value = [array objectToJsonString];
            }
        }else if ([value isKindOfClass:[ILBaseModel class]]) {
            ILBaseModel * otherModel = value;
            NSDictionary * dic = [otherModel dicFromObject];
            value = [dic objectToJsonString];
        }
         if(updateValues)[*updateValues addObject:value];
    }
}


+ (void)getInsertStrWithModel:(ILBaseModel *)model
                       keyStr:(NSMutableString **)keyStr
                     valueStr:(NSMutableString **)valueStr
                 insertValues:(NSMutableArray **)insertValues
{
    for (int i = 0; i < model.columnNames.count; i++) {
        NSString * proName = [model.columnNames objectAtIndex:i];
        if ([proName isEqualToString:PrimaryId]) continue;
        if(keyStr)[*keyStr appendString:proName];
        if(valueStr)[*valueStr appendString:@"?"];
        if (i > 0 && i < model.columnNames.count - 1) {
             if(keyStr)[*keyStr appendString:@","];
             if(valueStr)[*valueStr appendString:@","];
        }
    }
    for (int i = 0; i < model.originColumnNames.count; i++) {
        NSString * proName = [model.originColumnNames objectAtIndex:i];
        if ([proName isEqualToString:PrimaryId]) continue;
        id value = [model valueForKey:proName];
        if (!value) {
            value = @"";
        }else if ([value isKindOfClass:[NSDictionary class]]) {
            value = [value objectToJsonString];
        }
        else if ([value isKindOfClass:[NSArray class]]) {
            NSArray * array = (NSArray *)value;
            if([[array firstObject]isKindOfClass:[ILBaseModel class]]) {
                value = @"";
            }else {
                value = [array objectToJsonString];
            }
        }else if ([value isKindOfClass:[ILBaseModel class]]) {
            ILBaseModel * otherModel = value;
            NSDictionary * dic = [otherModel dicFromObject];
            value = [dic objectToJsonString];
        }
        if(insertValues)[*insertValues addObject:value];
    }
}

@end
