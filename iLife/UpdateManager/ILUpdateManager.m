//
//  ILUpdateManager.m
//  iLife
//
//  Created by hedongyang on 2020/8/6.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILUpdateManager.h"
#import "ilife-Swift.h"
#import "ILBlueManager.h"
#import "ILStructModel.h"

NSString * getChinessNordicDfuErrorStatus(DFUError error) {
    switch (error) {
        case DFUErrorRemoteLegacyDFUSuccess:
            return @"远程旧版本DFU运行成功";
            break;
        case DFUErrorRemoteLegacyDFUInvalidState:
            return @"远程旧版本DFU运行无效";
            break;
        case DFUErrorRemoteLegacyDFUNotSupported:
            return @"远程旧版本DFU运行不被支持";
            break;
        case DFUErrorRemoteLegacyDFUDataExceedsLimit:
            return @"远程旧版本DFU数据包超过了限额";
            break;
        case DFUErrorRemoteLegacyDFUCrcError:
            return @"远程旧版本DFU校验错误";
            break;
        case DFUErrorRemoteLegacyDFUOperationFailed:
            return @"远程旧版本DFU操作失败";
            break;
        case DFUErrorRemoteSecureDFUSuccess:
            return @"远程稳定版本DFU运行成功";
            break;
        case DFUErrorRemoteSecureDFUOpCodeNotSupported:
            return @"远程稳定版本DFU操作命令不被支持";
            break;
        case DFUErrorRemoteSecureDFUInvalidParameter:
            return @"远程稳定版本DFU无效参数";
            break;
        case DFUErrorRemoteSecureDFUInsufficientResources:
            return @"远程稳定版本DFU缺少资源";
            break;
        case DFUErrorRemoteSecureDFUInvalidObject:
            return @"远程稳定版本DFU无效对象";
            break;
        case DFUErrorRemoteSecureDFUSignatureMismatch:
            return @"远程稳定版本DFU签名不匹配";
            break;
        case DFUErrorRemoteSecureDFUUnsupportedType:
            return @"远程稳定版本DFU不支持的类型";
            break;
        case DFUErrorRemoteSecureDFUOperationNotpermitted:
            return @"远程稳定版本DFU操作不允许";
            break;
        case DFUErrorRemoteSecureDFUOperationFailed:
            return @"远程稳定版本DFU操作失败";
            break;
        case DFUErrorFileNotSpecified:
            return @"文件没有详细说明";
            break;
        case DFUErrorFileInvalid:
            return @"文件无效的";
            break;
        case DFUErrorExtendedInitPacketRequired:
            return @"必须扩展初始化固件包";
            break;
        case DFUErrorFailedToConnect:
            return @"连接失败";
            break;
        case DFUErrorDeviceDisconnected:
            return @"手环设备断开";
            break;
        case DFUErrorBluetoothDisabled:
            return @"无效蓝牙功能";
            break;
        case DFUErrorServiceDiscoveryFailed:
            return @"发现服务失败";
            break;
        case DFUErrorDeviceNotSupported:
            return @"设备不支持DFU功能";
            break;
        case DFUErrorReadingVersionFailed:
            return @"固件包读取版本号失败";
            break;
        case DFUErrorEnablingControlPointFailed:
            return @"授权控制点失败";
            break;
        case DFUErrorWritingCharacteristicFailed:
            return @"正在写特征失败";
            break;
        case DFUErrorReceivingNotificationFailed:
            return @"接受通知失败";
            break;
        case DFUErrorUnsupportedResponse:
            return @"不支持无效响应";
            break;
        case DFUErrorBytesLost:
            return @"丢失字节";
            break;
        case DFUErrorCrcError:
            return @"校验失败";
            break;
        default:
            return @"DFU升级失败";
            break;
    }
}

@interface ILUpdateManager ()<DFUServiceDelegate,DFUProgressDelegate,LoggerDelegate>
@property (nonatomic,strong,nullable) NSConditionLock * condition;
@property (nonatomic,copy) NSString * message;
@property (nonatomic,assign) float  currentProgress;
@property (nonatomic) dispatch_queue_t il_update_queue_t;
@property (nonatomic) dispatch_semaphore_t il_update_semaphore;
@property (nonatomic,strong) DFUServiceController * controller;
@property (nonatomic,copy) void(^reconnectCallback)(int errorCode);
@property (nonatomic,copy) void(^otaCommandCallback)(int state, int errorCode);
@end

@implementation ILUpdateManager

static ILUpdateManager * updateManager = nil;

+ (__kindof ILUpdateManager *)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        updateManager = [[super allocWithZone:NULL] init] ;
    }) ;
    return updateManager;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    return [ILUpdateManager shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [ILUpdateManager shareInstance];
}

- (NSConditionLock *)condition
{
    if (!_condition) {
        _condition = [[NSConditionLock alloc]init];
    }
    return _condition;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addNotification];
    }
    return self;
}

- (void)addNotification
{
    addNotification(self, @selector(connectState:), @"ILBlueState");
    addNotification(self, @selector(connectError:), @"ILBlueError");
}

- (void)connectState:(NSNotification *)notification
{
    if(![ILBlueManager isOta])return;
    NSDictionary * dic = notification.object;
    if ([dic isKindOfClass:[NSDictionary class]]) {
        NSInteger state = [[dic valueForKey:@"state"] integerValue];
        if (state == IL_MANAGER_STATE_DID_CONNECT) {
            if (self.reconnectCallback) {
                self.reconnectCallback(0);
                self.reconnectCallback = nil;
            }
        }else if (state == IL_MANAGER_STATE_POWEREDOFF) {
            if (self.reconnectCallback) {
                self.reconnectCallback(27);
                self.reconnectCallback = nil;
            }
        }
    }
}

- (void)connectError:(NSNotification *)notification
{
    if(![ILBlueManager isOta])return;
    NSError * error = notification.object;
    if([error isKindOfClass:[NSError class]]) {
        if (error.code != IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE) {
            if (self.reconnectCallback) {
                self.reconnectCallback(27);
                self.reconnectCallback = nil;
            }
        }
    }
}

- (void)connectTimeout
{
    cancelDelayMethod(self, @selector(connectTimeout));
    if (self.reconnectCallback) {
        self.reconnectCallback(13);
        self.reconnectCallback = nil;
    }
}

+ (void)updateReceive:(void *)otaReply
{
    struct protocol_ota_reply * ota_reply = otaReply;
    if ([ILUpdateManager shareInstance].otaCommandCallback) {
        [ILUpdateManager shareInstance].otaCommandCallback(ota_reply->state,0);
    }
}


+ (void)startUpdate
{
    ILUpdateManager * manager = [ILUpdateManager shareInstance];
    if (![ILBlueManager isConnected]) {
        [manager setValue:@(IL_UPDATE_BLUETOOTH_DIS_CONNECT_TYPE) forKey:@"errorCode"];
        NSString * errorStr = @"bluetooth dis connect .";
        [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"蓝牙设备未连接"];
        return;
    }
    void (^task)(void) = ^{
       
        [manager.condition unlock];
       [ILUpdateManager determineFirmwarePackageExist];
       if(manager.errorCode == IL_UPDATE_PACKAGE_FILE_NO_EXIST_TYPE){ //文件不存在
             
       }else { //进入ota模式 设备重连
           [manager.condition lock];
           dispatch_async(dispatch_get_main_queue(), ^{
                [ILUpdateManager enterNordicOtaModeAndConnect];
           });
       }
       
        [manager.condition lock];
       if (  manager.errorCode == IL_UPDATE_PACKAGE_FILE_NO_EXIST_TYPE
          || manager.errorCode == IL_UPDATE_ENTER_OTA_LOW_ELECTRICITY_TYPE
          || manager.errorCode == IL_UPDATE_NO_SUPPORTED_OTA_TYPE
          || manager.errorCode == IL_UPDATE_ENTER_OTA_PARAMETER_ERROR_TYPE
          || manager.errorCode == IL_UPDATE_ENTER_OTA_FAILED_TYPE
          || manager.errorCode == IL_UPDATE_ENTER_OTA_RECONNECT_FAILED_TYPE) { //进入ota失败
          
       }else { //进入升级
           dispatch_async(dispatch_get_main_queue(), ^{
               [ILUpdateManager enterNordicUpdateFirmware];
           });
       }
    };
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), task);
}

+ (void)determineFirmwarePackageExist
{
    BOOL isExist = NO;
    ILUpdateManager * manager = [ILUpdateManager shareInstance];
    [manager setValue:@(0) forKey:@"errorCode"];
    if (manager.delegate && [manager.delegate respondsToSelector:@selector(currentPackagePathWithUpdateManager:)])
    {
        manager.packagePath = [manager.delegate currentPackagePathWithUpdateManager:manager];
    }
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    isExist = [fileManager fileExistsAtPath:manager.packagePath];
    
    if (!isExist) {
        [manager setValue:@(IL_UPDATE_PACKAGE_FILE_NO_EXIST_TYPE) forKey:@"errorCode"];
        NSString * errorStr = @"firmware package does not exist.";
        [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"固件包不存在"];
    }
}

+ (void)enterNordicOtaModeAndConnect
{
    ILUpdateManager * manager = [ILUpdateManager shareInstance];
    if ([ILBlueManager isOta] && [ILBlueManager isConnected]) {
        [manager.condition unlock];
    }else if ([ILBlueManager isOta] && ![ILBlueManager isConnected]) {
        manager.reconnectCallback = ^(int errorCode) {
             if (errorCode == 0) {
                 [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_DID_RECONECT_DEVICE) forKey:@"state"];
                 [ILUpdateManager executesStateDelegate];
                 [[ILUpdateManager shareInstance].condition unlock];
             }else {
                 [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_ENTER_OTA_RECONNECT_FAILED_TYPE) forKey:@"errorCode"];
                 NSString * errorStr = @"enter ota reconnection failure.";
                 [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"进入ota模式重连设备失败"];
                 [[ILUpdateManager shareInstance].condition unlock];
             }
        };
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [[ILBlueManager shareInstance] startScan];
          cancelDelayMethod([ILUpdateManager shareInstance], @selector(connectTimeout));
          addAfterDelay([ILUpdateManager shareInstance], @selector(connectTimeout), 20);
      });
   }else {
     manager.otaCommandCallback = ^(int state, int errorCode) {
           if (errorCode == 0) {
               if (state == 0x00) { // 成功
                  [ILUpdateManager shareInstance].reconnectCallback = ^(int errorCode) {
                       if (errorCode == 0) {
                           [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_DID_RECONECT_DEVICE) forKey:@"state"];
                           [ILUpdateManager executesStateDelegate];
                           [[ILUpdateManager shareInstance].condition unlock];
                       }else {
                           [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_ENTER_OTA_RECONNECT_FAILED_TYPE) forKey:@"errorCode"];
                           NSString * errorStr = @"enter ota reconnection failure.";
                           [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"进入ota模式重连设备失败"];
                           [[ILUpdateManager shareInstance].condition unlock];
                       }
                   };
                   if ([ILBlueManager isConnected]) {
                       [ILBlueManager cancelCurrentPeripheralConnection];
                   }
                   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                       [[ILBlueManager shareInstance] startScan];
                       cancelDelayMethod([ILUpdateManager shareInstance], @selector(connectTimeout));
                       addAfterDelay([ILUpdateManager shareInstance], @selector(connectTimeout), 20);
                   });
               }else if (state == 0x01) { // 电量过低
                   [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_ENTER_OTA_LOW_ELECTRICITY_TYPE) forKey:@"errorCode"];
                   NSString * errorStr = @"the device power is too low.";
                   [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"设备电量过低无法进入ota"];
                   [[ILUpdateManager shareInstance].condition unlock];
               }else if (state == 0x02) { // 设备不支持
                   [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_NO_SUPPORTED_OTA_TYPE) forKey:@"errorCode"];
                   NSString * errorStr = @"the device does not support ota.";
                   [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"设备不支持ota模式"];
                   [[ILUpdateManager shareInstance].condition unlock];
               }else if (state == 0x03) { // 参数不正确
                   [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_ENTER_OTA_PARAMETER_ERROR_TYPE) forKey:@"errorCode"];
                   NSString * errorStr = @"enter ota parameter error.";
                   [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"设置ota模式参数错误"];
                   [[ILUpdateManager shareInstance].condition unlock];
               }
           }else {
               [[ILUpdateManager shareInstance] setValue:@(IL_UPDATE_ENTER_OTA_FAILED_TYPE) forKey:@"errorCode"];
               NSString * errorStr = @"enter ota failed.";
               [ILUpdateManager executesErrorDelegateWithMessage:errorStr cnMessage:@"进入ota失败"];
               [[ILUpdateManager shareInstance].condition unlock];
           }
       };
       struct protocol_head head;
       head.cmd = 0x01;
       head.key = 0x01;
       [ILBlueManager sendData:&head dataLength:2];
   }
}

+ (void)enterNordicUpdateFirmware
{
    ILUpdateManager * updateManager = [ILUpdateManager shareInstance];
    updateManager.currentProgress = 0.0f;
    updateManager.message = [NSString string];
    CBCentralManager * manager = [ILBlueManager shareInstance].centralManager;
    CBPeripheral * peripheral = [ILBlueManager shareInstance].currentPeripheral;
    DFUServiceInitiator * service = [[DFUServiceInitiator alloc]initWithCentralManager:manager
                                                                                target:peripheral];
    NSURL * url = [NSURL URLWithString:updateManager.packagePath];
    service.forceDfu = YES;
    service.packetReceiptNotificationParameter = 10;
    service.enableUnsafeExperimentalButtonlessServiceInSecureDfu = YES;
    service.logger   = updateManager;
    service.delegate = updateManager;
    service.progressDelegate = updateManager;
    DFUFirmware * firmware = [[DFUFirmware alloc]initWithUrlToZipFile:url type:DFUFirmwareTypeApplication];
    service = [service withFirmware:firmware];
    updateManager.controller = [service start];
}

#pragma mark ======================== DFUProgressDelegate ===================================

- (void)dfuProgressDidChangeFor:(NSInteger)part
                          outOf:(NSInteger)totalParts
                             to:(NSInteger)progress
     currentSpeedBytesPerSecond:(double)currentSpeedBytesPerSecond
         avgSpeedBytesPerSecond:(double)avgSpeedBytesPerSecond
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.currentProgress = ((float) progress /totalParts)/100.00f;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(updateManager:progress:message:)]) {
            [strongSelf.delegate updateManager:strongSelf progress:strongSelf.currentProgress message:strongSelf.message];
        }
    });
}

#pragma mark ======================== LoggerDelegate ===================================

-(void)logWith:(enum LogLevel)level message:(NSString *)message
{
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        strongSelf.message = message;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(updateManager:progress:message:)]) {
            [strongSelf.delegate updateManager:strongSelf progress:strongSelf.currentProgress message:strongSelf.message];
        }
    });
}

#pragma mark ======================== DFUServiceDelegate===================================

- (void)dfuStateDidChangeTo:(enum DFUState)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (state == DFUStateStarting) {
            [self setValue:@(IL_UPDATE_STARTING) forKey:@"state"];
            [ILUpdateManager executesStateDelegate];
        }else if (state == DFUStateUploading) {
            [self setValue:@(IL_UPDATE_UPLOADING) forKey:@"state"];
            [ILUpdateManager executesStateDelegate];
        }else if (state == DFUStateCompleted){
            self.controller = nil;
            [self setValue:@(IL_UPDATE_COMPLETED) forKey:@"state"];
            [ILUpdateManager executesStateDelegate];
            [[ILBlueManager shareInstance] startScan];
        }
    });
}


- (void)dfuError:(enum DFUError)error
didOccurWithMessage:(NSString * _Nonnull)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (   error != DFUErrorRemoteLegacyDFUSuccess
            && error != DFUErrorRemoteSecureDFUSuccess
            && error != DFUErrorExtendedInitPacketRequired) {
            self.controller = nil;
            if (error == DFUErrorBluetoothDisabled) {
                [self setValue:@(IL_SYSTEM_BLUETOOTH_DISABLED_TYPE) forKey:@"errorCode"];
            }else {
                [self setValue:@(IL_UPDATE_FIRMWARE_FAILED_TYPE) forKey:@"errorCode"];
            }
            NSString * cnMessage = getChinessNordicDfuErrorStatus(error);
            [ILUpdateManager executesErrorDelegateWithMessage:message cnMessage:cnMessage];
        }
    });
}


+ (void)executesErrorDelegateWithMessage:(NSString *)message
                               cnMessage:(NSString *)cnMessage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ILUpdateManager * manager = [ILUpdateManager shareInstance];
        [manager setValue:@(IL_UPDATE_START_INIT) forKey:@"state"];
        [ILUpdateManager executesStateDelegate];
        if(manager.delegate && [manager.delegate respondsToSelector:@selector(updateManager:updateError:)]) {
            NSDictionary * userInfo = @{@"cn":cnMessage?:@"",@"en":message?:@""};
            NSError * error = [NSError errorWithDomain:message?:@"" code:manager.errorCode userInfo:userInfo];
            [manager.delegate updateManager:manager updateError:error];
        }
    });
}

+ (void)executesStateDelegate
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ILUpdateManager * manager = [ILUpdateManager shareInstance];
        if(manager.delegate && [manager.delegate respondsToSelector:@selector(updateManager:state:)]) {
            [manager.delegate updateManager:manager state:manager.state];
        }
    });
}

@end
