//
//  ILUpdateManager.h
//  iLife
//
//  Created by hedongyang on 2020/8/6.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * OTA升级状态枚举
 * OTA upgrade status enumeration
 */
typedef NS_ENUM(NSInteger, IL_UPDATE_STATE) {
    
    /**
     * 升级初始状态
     * Update initial state
     */
    IL_UPDATE_START_INIT = 0,
    
    /**
     * 开始进入OTA
     * Start entering OTA
     */
    IL_UPDATE_START_ENTER_OTA = 1,
    
    /**
     * 已进入OTA
     * Has entered OTA
     */
    IL_UPDATE_DID_ENTER_OTA,
    
    /**
     * 开始重连设备
     * Start reconnecting devices
     */
    IL_UPDATE_START_RECONECT_DEVICE,
    
    /**
     * 设备已经重新连接上
     * The device has been reconnected
     */
    IL_UPDATE_DID_RECONECT_DEVICE,
    /**
     * 开始启动升级固件
     * Start booting the firmware
     */
    IL_UPDATE_STARTING,
    
    /**
     * 固件升级中
     * Firmware upgrade
     */
    IL_UPDATE_UPLOADING,
    
    /**
     * 固件升级完成
     * Firmware upgrade completed
     */
    IL_UPDATE_COMPLETED,
    
};

/**
 * 设备升级错误类型枚举
 * Device upgrade error type enumeration
 */
typedef NS_ENUM(NSInteger, IL_UPDATE_ERROR_TYPE) {
    /**
     * 固件包文件不存在
     * The firmware package file does not exist
     */
    IL_UPDATE_PACKAGE_FILE_NO_EXIST_TYPE = 1,
    
    /**
     * 电量不足不能进入OTA模式
     * Insufficient power can not enter OTA mode
     */
    IL_UPDATE_ENTER_OTA_LOW_ELECTRICITY_TYPE,
    
    /**
     * 设备不支持OTA升级
     * The device does not support OTA upgrade
     */
    IL_UPDATE_NO_SUPPORTED_OTA_TYPE,
    
    /**
     * 进入OTA设置参数错误
     * Enter OTA setting parameter error
     */
    IL_UPDATE_ENTER_OTA_PARAMETER_ERROR_TYPE,
    
    /**
     * 进入OTA失败
     * Entering OTA failed
     */
    IL_UPDATE_ENTER_OTA_FAILED_TYPE,
    
    /**
     * 进入ota蓝牙重连失败
     * Enter OTA Bluetooth reconnection failed
     */
    IL_UPDATE_ENTER_OTA_RECONNECT_FAILED_TYPE,
    
    /**
     * 蓝牙断开无法升级
     * Bluetooth disconnect cannot be upgraded
     */
    IL_UPDATE_BLUETOOTH_DIS_CONNECT_TYPE,
    
    /**
     * 升级固件包失败
     * Upgrade firmware package failed
     */
    IL_UPDATE_FIRMWARE_FAILED_TYPE,
    
    /*
     * 升级过程中系统蓝牙断开
     * The system bluetooth was disconnected during the upgrade
     */
    IL_SYSTEM_BLUETOOTH_DISABLED_TYPE,
    
    /**
     * 手机系统版本过低不支持固件升级
     * Firmware upgrade is not supported in the lower version of the mobile system
     */
    IL_SYSTEM_VERSION_TOO_LOW_TYPE,
    
    /**
     * 不支持当前平台升级
     * no supported platform update
     */
    IL_NO_SUPPORTED_PLATFORM_TYPE,
} ;



@class ILUpdateManager;

@protocol ILUpdateManagerDelegate<NSObject>

/**
 * @brief 传入包的存放本地路径 | Local path to store incoming packets
 * @param manager 升级管理中心对象 | Upgrade Management Center Objects
 * @return 升级包的存放路径 | Upgrade package storage path
 */
- (NSString *_Nullable)currentPackagePathWithUpdateManager:(ILUpdateManager *_Nullable)manager;

/**
 * @brief  升级过程的进度 | Progress of the upgrade process
 * @param manager 升级管理中心对象 | Upgrade Management Center Objects
 * @param progress 进度 (0 ~ 1) | Progress (0 ~ 1)
 * @param message 升级日志信息 | Upgrade log information
 */
- (void)updateManager:(ILUpdateManager *_Nullable)manager
             progress:(float)progress
              message:(NSString *_Nullable)message;


/**
 * @brief 升级状态 | Upgrade status
 * @param manager 升级管理中心对象 | Upgrade Management Center Objects
 * @param state 状态值 | Status value
 */
- (void)updateManager:(ILUpdateManager *_Nullable)manager
                state:(IL_UPDATE_STATE)state;


/**
 * @brief 升级过程报错 | The upgrade process is reporting an error
 * @param manager 升级管理中心对象 | Upgrade Management Center Objects
 * @param error 错误信息 | Error message
 */
- (void)updateManager:(ILUpdateManager *_Nullable)manager
          updateError:(NSError *_Nullable)error;

@end

@interface ILUpdateManager : NSObject

/**
 设置代理 | Setting up the agent
 */
@property (nonatomic,weak,nullable) id <ILUpdateManagerDelegate> delegate;

/**
 固件包本地路径 | Firmware package local path
 */
@property (nonatomic,copy,nullable) NSString * packagePath;

/**
 升级错误码 | Upgrade error code
 */
@property (nonatomic,assign,readonly) IL_UPDATE_ERROR_TYPE errorCode;

/**
 升级状态 | Upgrade status
 */
@property (nonatomic,assign,readonly) IL_UPDATE_STATE state;

/**
 * @brief 初始化升级管理中心对象 | Initialize the Upgrade Management Center object
 * @return ILUpdateManager
 */
+ (__kindof ILUpdateManager *_Nonnull)shareInstance;

/**
 开始升级 | Start upgrading
 */
+ (void)startUpdate;

/**
 进入ota回调
 */
+ (void)updateReceive:(void *_Nullable)otaReply;

@end

