//
//  AppDelegate.h
//  iLife
//
//  Created by hedongyang on 2020/7/3.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
@end

