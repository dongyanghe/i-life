//
//  ILBlueEnum.h
//  iLife
//
//  Created by hedongyang on 2020/7/19.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#ifndef ILBlueEnum_h
#define ILBlueEnum_h

/**
 * 蓝牙连接错误类型枚举
 * Bluetooth connection error type enumeration
 */
typedef NS_ENUM(NSInteger, IL_BLUETOOTH_CONNECT_ERROR_TYPE) {
    /**
     * 蓝牙连接失败
     * Bluetooth connection failed
     */
    IL_BLUETOOTH_CONNECT_FAIL_TYPE = 1,
    
    /**
     * 蓝牙连接超时
     * Bluetooth connection timeout
     */
    IL_BLUETOOTH_CONNECT_TIME_OUT_TYPE,
        
    /**
     * 蓝牙发现外围设备服务失败
     * Bluetooth discovery peripheral service failed
     */
    IL_BLUETOOTH_DISCOVER_SERVICE_FAIL_TYPE,
    
    /**
     * 蓝牙发现外围设备服务不存在
     * Bluetooth discovery peripheral service does not exist
     */
    IL_BLUETOOTH_DISCOVER_SERVICE_NO_EXIST_TYPE,
    
    /**
     * 蓝牙发现外围设备特征失败
     * Bluetooth discovery peripheral feature failed
     */
    IL_BLUETOOTH_DISCOVER_CHARACTERISTICS_TYPE,
    
    /**
     * 蓝牙手动模式断开连接
     * Bluetooth manual mode disconnect
     */
    IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE,
        
};

/**
 * 蓝牙管理状态枚举
 * Bluetooth management status enumeration
 */
typedef NS_ENUM(NSInteger, IL_BLUETOOTH_MANAGER_STATE) {
    /**
     * 蓝牙关闭
     * Bluetooth off
     */
    IL_MANAGER_STATE_POWEREDOFF = 1,
    
    /**
     * 蓝牙打开
     * Bluetooth open
     */
    IL_MANAGER_STATE_POWEREDON,
    
    /**
     * 蓝牙扫描停止
     * Bluetooth scan stop
     */
    IL_MANAGER_STATE_SCAN_STOP,
    
    /**
     * 蓝牙手动扫描中
     * Bluetooth manual scanning
     */
    IL_MANAGER_STATE_SCANING,
        
    /**
     * 普通模式手动连接
     * Normal mode manual connection
     */
    IL_MANAGER_STATE_CONNECT,
    
    /**
     * 手环连接成功
     * The bracelet is connected successfully
     */
    IL_MANAGER_STATE_DID_CONNECT
};


#endif /* ILBlueEnum_h */
