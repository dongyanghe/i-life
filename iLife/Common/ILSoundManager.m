//
//  ILSoundManager.m
//  iLife
//
//  Created by hedongyang on 2020/8/10.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILSoundManager.h"
#import "SoundControlSingle.h"
#import "ILBlueManager.h"
#import "ILUpdateManager.h"
#import "ILGlobalEngineModel.h"

@interface ILSoundManager ()
@property (nonatomic,strong) SoundControlSingle * single1;
@property (nonatomic,strong) SoundControlSingle * single2;
@end

@implementation ILSoundManager

- (void)dealloc
{
    removeNotification(self);
}

static ILSoundManager * manager = nil;
+ (__kindof ILSoundManager *)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        manager = [[super allocWithZone:NULL] init] ;
    }) ;
    return manager;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    return [ILSoundManager shareInstance];
}

- (id)copyWithZone:(struct _NSZone *)zone
{
    return [ILSoundManager shareInstance];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        addNotification(self, @selector(ILBlueError:), @"ILBlueError");
        addNotification(self, @selector(BlueUpdate:), @"ILBlueUpdateValue");
        addNotification(self, @selector(BlueState:), @"ILBlueState");
        self.single1 = [SoundControlSingle sharedInstanceForSound];
        self.single2 = [SoundControlSingle sharedInstanceForVibrate];
    }
    return self;
}

- (ILSetUpInfoModel *)setUpModel
{
    if (!_setUpModel) {
        ILGlobalEngineModel * engine = [ILGlobalEngineModel shareInstance];
        NSString * sqlStr = [NSString stringWithFormat:@"WHERE user_id = '%@' AND mac_addr = '%@'",engine.userId,engine.macAddr];
        NSArray <ILBaseModel *>* array = [ILSetUpInfoModel queryModelsWithSqlStr:sqlStr];
        _setUpModel = (ILSetUpInfoModel *)[array firstObject];
        if (!_setUpModel) {
            _setUpModel = [[ILSetUpInfoModel alloc]init];
        }
    }
    return _setUpModel;
}

- (void)BlueUpdate:(NSNotification *)notification
{
    NSDictionary * dic = notification.object;
    NSString * tempStr = [dic valueForKey:@"value"];
    CGFloat value = [tempStr floatValue];
    if (   value >= [self.setUpModel.hotTemp floatValue]
        || value <= [self.setUpModel.lowTemp floatValue]) {
        if (self.setUpModel.isOpenBell) {
            [self.single1 play];
        }
        if (self.setUpModel.isVibration) {
            [self.single2 play];
        }
    }
}

- (void)ILBlueError:(NSNotification *)notification
{
    NSError * error = notification.object;
    if (error.code == IL_BLUETOOTH_MANUAL_DIS_CONNECT_TYPE) {
       if (self.setUpModel.isOpenBell) {
           [self.single1 play];
       }
       if (self.setUpModel.isVibration) {
           [self.single2 play];
       }
    }
}

- (void)BlueState:(NSNotification *)notification
{
    NSDictionary * dic = notification.object;
    IL_BLUETOOTH_MANAGER_STATE state = [[dic valueForKey:@"state"] integerValue];
    if (state == IL_MANAGER_STATE_POWEREDOFF) {
        if (self.setUpModel.isOpenBell) {
             [self.single1 play];
        }
        if (self.setUpModel.isVibration) {
             [self.single2 play];
        }
    }else if (state == IL_MANAGER_STATE_DID_CONNECT) {
        if (self.didConnectCallback) {
            self.didConnectCallback();
        }
    }
}

@end
