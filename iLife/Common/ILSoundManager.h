//
//  ILSoundManager.h
//  iLife
//
//  Created by hedongyang on 2020/8/10.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILSetUpInfoModel.h"

@interface ILSoundManager : NSObject

@property (nonatomic,strong) ILSetUpInfoModel * setUpModel;

@property (nonatomic,copy) void(^didConnectCallback)(void);

+ (__kindof ILSoundManager *)shareInstance;

@end
