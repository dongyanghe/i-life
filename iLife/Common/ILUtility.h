//
//  ILUtility.h
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILPeripheralModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ILUtility : NSObject

+ (NSString *)base64EncodeString:(NSString *)string;

+ (BOOL)createDirectoryWithPath:(NSString *)path
                          error:(NSError **)error;

+ (NSString *)snakeStyleStringFromHumpStyleString:(NSString *)originString;

+ (NSString *)getCurrentDayStr;

+ (ILPeripheralModel *)didDiscoverPeripheral:(CBPeripheral *)peripheral
                            advertisementData:(NSDictionary *)advertisementData
                                        RSSI:(NSNumber *)RSSI;

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day
                              andHour:(NSInteger)hour
                            andMinute:(NSInteger)minute;

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day
                              andHour:(NSInteger)hour;

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day;

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month;

+ (NSString *)get1970timeTempWithYear:(NSInteger)year;

+ (BOOL)isOtaModeWithPeripheral:(CBPeripheral *)peripheral;

+ (CGFloat)getHeightWithText:(NSString *)text
                       width:(CGFloat)textWidth
                    fontSize:(UIFont *)font;

+ (CGFloat)getWidthWithText:(NSString *)text
                     height:(CGFloat)textHeight
                   fontSize:(UIFont *)font;

+ (void)popGestureClose:(UIViewController *)VC;

+ (void)popGestureOpen:(UIViewController *)VC;

+ (NSString *)currentTimeStamp;

+ (NSString *)currentDateStr;

+ (NSString *)currentHourStr;

+ (NSString *)currentMinuteStr;

+ (NSDateComponents *)currentComponents;

+ (NSDateComponents *)dateComponentsWithDateStr:(NSString *)dateStr;

+ (NSInteger)getDaysInMonthWithYear:(NSInteger)year
                              month:(NSInteger)month;

+ (NSArray *)getBinaryByDecimal:(NSInteger)decimal;

+ (NSArray *)getServiceUUIDs;

@end

NS_ASSUME_NONNULL_END
