//
//  ILUtility.m
//  iLife
//
//  Created by hedongyang on 2020/7/11.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#import "ILUtility.h"

@implementation ILUtility

+ (NSString *)base64EncodeString:(NSString *)string {
    NSData * data =[string dataUsingEncoding:NSUTF8StringEncoding];
    return [data base64EncodedStringWithOptions:0];
}

+ (NSArray *)getBinaryByDecimal:(NSInteger)decimal {
    
    NSString *binary = @"";
    while (decimal) {
        binary = [[NSString stringWithFormat:@"%ld", (long)decimal % 2] stringByAppendingString:binary];
        if (decimal / 2 < 1) {
            break;
        }
        decimal = decimal / 2 ;
    }
    if (binary.length % 4 != 0) {
        NSMutableString *mStr = [[NSMutableString alloc]init];
        for (int i = 0; i < 4 - binary.length % 4; i++) {
            [mStr appendString:@"0"];
        }
        binary = [mStr stringByAppendingString:binary];
    }
    if (binary.length < 8) {
         NSMutableString *mStr = [[NSMutableString alloc]init];
        for (int i = 0; i < 8 - binary.length; i++) {
            [mStr appendString:@"0"];
        }
        binary = [mStr stringByAppendingString:binary];
    }
    NSMutableArray * binaryArray = [NSMutableArray array];
    for (int i = 0; i<binary.length; i++) {
        NSNumber * value = [NSNumber numberWithInteger:[[binary substringWithRange:NSMakeRange(i, 1)]integerValue]];
        [binaryArray addObject:value];
    }
    return binaryArray;
}

+ (BOOL)createDirectoryWithPath:(NSString *)path
                          error:(NSError **)error
{
    NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL isDir = NO;
    BOOL isDirExist = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    if (isDirExist && isDir)return YES;
    
    NSURL * fileUrl = [NSURL URLWithString:path];
    NSArray * pathComponents = fileUrl.pathComponents;
    NSMutableArray <NSString *>* noExistArray = [NSMutableArray array];
    if (pathComponents.count == 0)return NO;
    
    for (NSInteger i = pathComponents.count - 1; i >=0; i --) {
        NSString * component = [pathComponents objectAtIndex:i];
        NSRange range = [fileUrl.absoluteString rangeOfString:component];
        NSString * filePath = [fileUrl.absoluteString substringToIndex:range.location + range.length];
        BOOL isDir;
        BOOL exist = [fileManager fileExistsAtPath:filePath isDirectory:&isDir];
        if (exist) break;
        if (!exist || !isDir) {
            [noExistArray addObject:filePath];
        }
    }
    if (noExistArray.count == 0) return YES;
    NSInteger index = noExistArray.count - 1;
    NSMutableArray * boolArray = [NSMutableArray array];
    for (NSInteger i = index; i >= 0; i--) {
        NSString * filePath = [noExistArray objectAtIndex:i];
        BOOL isSuccess = [fileManager createDirectoryAtPath:filePath
                               withIntermediateDirectories:YES
                                                attributes:nil
                                                     error:error];
        [boolArray addObject:@(isSuccess)];
    }
    for (int i = 0; i < boolArray.count; i++) {
        BOOL isSuccess = [[boolArray objectAtIndex:i] boolValue];
        if (!isSuccess) {
            return NO;
        }
    }
    return YES;
}

+ (NSDate *)currentDate
{
    NSDate  * currentDate = [NSDate date];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]; //只考虑公历
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday|
    NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    NSDateComponents * componenets = [calendar components:unit fromDate:currentDate];
    NSDate * newDate = [calendar dateFromComponents:componenets];
    return newDate ? : currentDate;
}

+ (NSDateComponents *)currentComponents
{
    NSDate  * currentDate = [NSDate date];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]; //只考虑公历
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday|
    NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    NSDateComponents * componenets = [calendar components:unit fromDate:currentDate];
    return componenets;
}

+ (NSDateFormatter *)obtainDateFormatter
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:locale];
    return formatter;
}

+ (NSString *)currentTimeStamp
{
   return [NSString stringWithFormat:@"%lld",(long long)[[self currentDate] timeIntervalSince1970]];
}

+ (NSString *)currentDateStr
{
    NSDateComponents * components = [self currentComponents];
    return [self get1970timeTempWithYear:components.year
                                andMonth:components.month
                                  andDay:components.day];
}

+ (NSString *)currentHourStr
{
    NSDateComponents * components = [self currentComponents];
    return [self get1970timeTempWithYear:components.year
                                andMonth:components.month
                                  andDay:components.day
                                 andHour:components.hour];
}

+ (NSString *)currentMinuteStr
{
    NSDateComponents * components = [self currentComponents];
    return [self get1970timeTempWithYear:components.year
                                andMonth:components.month
                                  andDay:components.day
                                 andHour:components.hour
                               andMinute:components.minute];
}

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day
                              andHour:(NSInteger)hour
                            andMinute:(NSInteger)minute
{
    NSDateFormatter * formatter = [self obtainDateFormatter];
    [formatter setDateFormat:@"yyyyMMddHHmm"];
    NSString *dateString = [NSString stringWithFormat:@"%04ld%02ld%02ld%02ld%02ld",
                            (long)year,(long)month,(long)day,(long)hour,(long)minute];
    NSDate *date = [formatter dateFromString:dateString];
    return [NSString stringWithFormat:@"%lld",(long long)[date timeIntervalSince1970]];
}

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day
                              andHour:(NSInteger)hour
{
    NSDateFormatter * formatter = [self obtainDateFormatter];
    [formatter setDateFormat:@"yyyyMMddHH"];
    NSString *dateString = [NSString stringWithFormat:@"%04ld%02ld%02ld%02ld",
                            (long)year,(long)month,(long)day,(long)hour];
    NSDate *date = [formatter dateFromString:dateString];
    return [NSString stringWithFormat:@"%lld",(long long)[date timeIntervalSince1970]];
}

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
                               andDay:(NSInteger)day
{
    NSDateFormatter * formatter = [self obtainDateFormatter];
    [formatter setDateFormat:@"yyyyMMdd"];
    NSString *dateString = [NSString stringWithFormat:@"%04ld%02ld%02ld",
                            (long)year,(long)month,(long)day];
    NSDate *date = [formatter dateFromString:dateString];
    return [NSString stringWithFormat:@"%lld",(long long)[date timeIntervalSince1970]];
}

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
                             andMonth:(NSInteger)month
{
    NSDateFormatter * formatter = [self obtainDateFormatter];
    [formatter setDateFormat:@"yyyyMM"];
    NSString *dateString = [NSString stringWithFormat:@"%04ld%02ld",(long)year,(long)month];
    NSDate *date = [formatter dateFromString:dateString];
    return [NSString stringWithFormat:@"%lld",(long long)[date timeIntervalSince1970]];
}

+ (NSString *)get1970timeTempWithYear:(NSInteger)year
{
    NSDateFormatter * formatter = [self obtainDateFormatter];
    [formatter setDateFormat:@"yyyy"];
    NSString *dateString = [NSString stringWithFormat:@"%04ld",(long)year];
    NSDate *date = [formatter dateFromString:dateString];
    return [NSString stringWithFormat:@"%lld",(long long)[date timeIntervalSince1970]];
}

+ (NSDateComponents *)dateComponentsWithDateStr:(NSString *)dateStr
{
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:[dateStr integerValue]];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSCalendarUnit unit = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday|
    NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond;
    NSDateComponents * components = [calendar components:unit fromDate:date];
    return components;
}

+ (NSString *)snakeStyleStringFromHumpStyleString:(NSString *)originString {
    NSArray * separatedArray =  [originString componentsSeparatedByCharactersInSet:[NSCharacterSet uppercaseLetterCharacterSet]];
    NSMutableString * processedString = [[NSMutableString alloc]init];
    NSInteger loc = 0;
    for (NSInteger i = 0; i < separatedArray.count - 1; i ++) {
        NSString *item = [separatedArray objectAtIndex:i];
        [processedString appendString:item];
        NSString *upperCaseString = [originString substringWithRange:NSMakeRange(item.length + loc, 1)];
        [processedString appendFormat:@"_%@",upperCaseString.lowercaseString];
        loc += item.length;
        loc += 1;
    }
    if (separatedArray.count > 0) {
        [processedString appendString:separatedArray.lastObject];
    }
    return processedString;
}

+ (NSString *)getCurrentDayStr
{
    NSDateFormatter *dateFM = [[NSDateFormatter alloc] init];
    dateFM.dateFormat = @"yyyy-MM-dd";
    NSString * str = [dateFM stringFromDate:[NSDate date]];
    return str;
}

+ (float)calcDistByRssi:(int)rssi
{
    int iRssi = abs(rssi);
    float power = (iRssi-59)/(10*2.0);
    return pow(10, power);
}


+ (ILPeripheralModel *)didDiscoverPeripheral:(CBPeripheral *)peripheral
                            advertisementData:(NSDictionary *)advertisementData
                                         RSSI:(NSNumber *)RSSI
{
    ILPeripheralModel * model = [[ILPeripheralModel alloc]init];
    model.name     = [advertisementData valueForKey:@"kCBAdvDataLocalName"];
    model.uuidStr  = peripheral.identifier.UUIDString;
    model.rssi     = abs(RSSI.intValue);
    model.distance = [ILUtility calcDistByRssi:RSSI.intValue];
    id data = [advertisementData valueForKey:@"kCBAdvDataManufacturerData"];
    NSString * macAddr = [ILUtility getPeripheralMacAddr:data];
    model.macAddr    = macAddr;
    model.peripheral = peripheral;
    return model;
}

+ (NSString *)getPeripheralMacAddr:(id)data
{
    NSString * macStr = @"Bluetooth broadcast packages are not standard";
    if (!data) return macStr;
    if ([data length] < 8)return macStr;
    Byte codeBytes[[data length]];
    for (int i = 0 ; i < [data length]; i++) {
        NSData * itemData = [data subdataWithRange:NSMakeRange(i,1)];
        codeBytes[i] = ((Byte*)[itemData bytes])[0];
    }
    NSInteger count = [data length];
    macStr = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                    count > 4 ? codeBytes[4] : 0,
                    count > 5 ? codeBytes[5] : 0,
                    count > 6 ? codeBytes[6] : 0,
                    count > 7 ? codeBytes[7] : 0,
                    count > 8 ? codeBytes[8] : 0,
                    count > 9 ? codeBytes[9] : 0];
    return macStr;
}

+ (CGFloat)getHeightWithText:(NSString *)text
                       width:(CGFloat)textWidth
                    fontSize:(UIFont *)font{
    
    
    NSDictionary *dict = @{NSFontAttributeName:font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(textWidth, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    //返回计算出的行高
    return rect.size.height;
}

+ (CGFloat)getWidthWithText:(NSString *)text
                     height:(CGFloat)textHeight
                   fontSize:(UIFont *)font
{
    NSDictionary * dict = @{NSFontAttributeName:font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, textHeight) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    //返回计算出的行宽
    return rect.size.width;
}

+ (NSInteger)getDaysInMonthWithYear:(NSInteger)year
                              month:(NSInteger)month
{
    
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM"];
    NSString * dateStr = [NSString stringWithFormat:@"%ld-%ld",(long)year,(long)month];
    NSDate * date = [formatter dateFromString:dateStr];
    NSCalendar * calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSRange range = [calendar rangeOfUnit:NSCalendarUnitDay
                                   inUnit:NSCalendarUnitMonth
                                  forDate:date];
    return range.length;
}

+ (void)popGestureClose:(UIViewController *)VC
{
    // 禁用侧滑返回手势
    if ([VC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        //这里对添加到右滑视图上的所有手势禁用
        for (UIGestureRecognizer *popGesture in VC.navigationController.interactivePopGestureRecognizer.view.gestureRecognizers) {
            popGesture.enabled = NO;
        }
    }
}

+ (void)popGestureOpen:(UIViewController *)VC
{
    // 启用侧滑返回手势
    if ([VC.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        //这里对添加到右滑视图上的所有手势启用
        for (UIGestureRecognizer *popGesture in VC.navigationController.interactivePopGestureRecognizer.view.gestureRecognizers) {
            popGesture.enabled = YES;
        }
    }
}

+ (BOOL)isOtaModeWithPeripheral:(CBPeripheral *)peripheral
{
    BOOL isOtaMode = NO;
    CBUUID * uuid1 = [CBUUID UUIDWithString:@"00001530-1212-EFDE-1523-785FEABCD123"];
    CBUUID * uuid2 = [CBUUID UUIDWithString:@"FE59"];
       for (CBService * service in peripheral.services){
       if ([service.UUID isEqual:uuid1]
         || [service.UUID isEqual:uuid2]){
         isOtaMode = YES;
       }
    }
    return isOtaMode;
}

+ (NSArray *)getServiceUUIDs
{
    CBUUID * uuid1 = [CBUUID UUIDWithString:@"00001530-1212-EFDE-1523-785FEABCD123"];
    CBUUID * uuid2 = [CBUUID UUIDWithString:@"FE59"];
    CBUUID * uuid3 = [CBUUID UUIDWithString:@"0000FF10-0000-1000-8000-00805f9b34fb"];
    return @[uuid1,uuid2,uuid3];
}

@end
