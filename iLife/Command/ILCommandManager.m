//
//  ILCommandManager.m
//
//  Created by hedongyang on 2020/8/31.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import "ILCommandManager.h"
#import "ILCommandTimer.h"
#import "ILError.h"

@interface ILCommandManager ()<ILCommandTimerDelegate>
@property (nonatomic,strong) NSHashTable * hashTable;
@property (nonatomic,strong) NSMutableArray * commandArray;
@property (nonatomic,strong) NSMutableArray * timerArray;
@property (nonatomic,strong) NSConditionLock * commandLock;
@property (nonatomic) dispatch_queue_t il_command_queue_t;
@end

@implementation ILCommandManager

static ILCommandManager * instance = nil;

+ (ILCommandManager *)shareInstance
{
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        instance = [[super allocWithZone:NULL] init];
    }) ;
    return instance ;
}

+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [ILCommandManager shareInstance];
}

- (id) copyWithZone:(struct _NSZone *)zone
{
    return [ILCommandManager shareInstance];
}

- (NSHashTable *)hashTable
{
    if (!_hashTable) {
         _hashTable = [NSHashTable weakObjectsHashTable];
    }
    return _hashTable;
}

- (NSMutableArray *)commandArray
{
    if (!_commandArray) {
         _commandArray = [NSMutableArray array];
    }
    return _commandArray;
}

- (NSMutableArray *)timerArray
{
    if (!_timerArray) {
         _timerArray = [NSMutableArray array];
    }
    return _timerArray;
}

- (NSConditionLock *)commandLock
{
    if (!_commandLock) {
        _commandLock = [[NSConditionLock alloc]init];
    }
    return _commandLock;
}

- (dispatch_queue_t)il_command_queue_t
{
    if (!_il_command_queue_t) {
         _il_command_queue_t = dispatch_queue_create("com.il.command.queue",DISPATCH_QUEUE_SERIAL);
    }
    return _il_command_queue_t;
}

- (BOOL)newTimerWithCommand:(ILBaseCommand *)command
               currentTimer:(ILCommandTimer**)timer;
{
    for (ILCommandTimer * model in self.timerArray) {
        if (model.type == command.type) {
            if(timer)*timer = model;
            return NO;
        }
    }
    return YES;
}

- (ILCommandTimer *)creatTimerWithCommand:(ILBaseCommand *)command
{
    ILCommandTimer * timer = nil;
    if ([self newTimerWithCommand:command currentTimer:&timer]) {
        timer = [[ILCommandTimer alloc]init];
        timer.timeout  = command.timeout;
        timer.type = command.type;
        timer.delegate = self;
        timer.timerId  = self.timerArray.count;
        [self.timerArray addObject:timer];
    }else {
        [timer stopTiming];
        timer.timeout = command.timeout;
    }
    return timer;
}

//通过命令定时器获取命令对象
- (ILBaseCommand *)getCommandWithTimer:(ILCommandTimer *)timer
{
    for (NSDictionary * dic in self.commandArray) {
        ILBaseCommand * model = [dic valueForKey:@"command"];
        if (model.timeId == timer.timerId) {
            return model;
        }
    }
    return nil;
}

//通过命令获取命令定时器对象
- (ILCommandTimer *)getTimerWithCommand:(ILBaseCommand *)command
{
    for (ILCommandTimer * model in self.timerArray) {
        if (model.timerId == command.timeId) {
            return model;
        }
    }
    return nil;
}

//命令执行后删除当前命令对象
- (void)removeOneCommand:(ILBaseCommand *)command
{
   NSMutableArray * removeArray = [NSMutableArray array];
    for (int i = 0; i < self.commandArray.count; i++) {
        NSDictionary * dic = self.commandArray[i];
        ILBaseCommand * oneCommand = [dic valueForKey:@"command"];
        if ([oneCommand isEqual:command]) {
            [removeArray addObject:dic];
            break;
        }
    }
    for (int i = 0; i < removeArray.count; i++) {
        NSDictionary * dic = [removeArray objectAtIndex:i];
        [self.commandArray removeObject:dic];
    }
}

+ (BOOL)addCommand:(ILBaseCommand *)command target:(id)target
{
    if (!command || !target) return NO;
    ILCommandManager * manager = [ILCommandManager shareInstance];
    [manager removeRedundantCommands];
    [manager.hashTable addObject:target];
    command.state = 1;
    [manager.commandArray addObject:@{@"command":command,@"class":[target class]}];
    dispatch_async(manager.il_command_queue_t, ^{
        [manager.commandLock lock];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary * dic = [manager.commandArray firstObject];
            ILBaseCommand  * currentCommand = [dic valueForKey:@"command"];
            ILCommandTimer * timer = [manager creatTimerWithCommand:currentCommand];
            command.timeId = timer.timerId;
            command.state  = 2;
            if (![command sendCommand]) {
                [timer stopTiming];
                [manager.commandLock unlock];
            }else{
                [timer startTiming];
                manager.delegate = (id)command;
            }
        });
    });
    return YES;
}

+ (BOOL)didUpdateValue:(NSData *)data dataLength:(NSInteger)length
{
   if (length == 0) return NO;
   Byte codeBytes[length];
   for (int i = 0 ; i < length; i++) {
       NSData * itemData = [data subdataWithRange:NSMakeRange(i,1)];
       codeBytes[i] = ((Byte*)[itemData bytes])[0];
   }
    ILCommandManager * manager = [ILCommandManager shareInstance];
    if (manager.delegate && [manager.delegate respondsToSelector:@selector(commandCallback:dataLength:)]) {
        ILBaseCommand * command = [manager.delegate commandCallback:codeBytes dataLength:length];
        if (command)[manager didCompleteCommand:command];
    }
    return YES;
}

- (BOOL)isContainsObjectWithClass:(Class)class
{
    for (id object in self.hashTable) {
        if ([object isKindOfClass:class]) {
            return YES;
        }
    }
    return NO;
}

- (void)removeRedundantCommands
{
    NSMutableArray * removeArray = [NSMutableArray array];
    for (int i = 0; i < self.commandArray.count; i++) {
        NSDictionary * dic = self.commandArray[i];
        if (![self isContainsObjectWithClass:dic[@"class"]]) {
            [removeArray addObject:dic];
        }
    }
    for (int i = 0; i < removeArray.count; i++) {
        NSDictionary * dic = [removeArray objectAtIndex:i];
        [self.commandArray removeObject:dic];
    }
}


#pragma mark ===== ILCommandTimerDelegate =====
- (void)commandTimeoutWithModel:(ILCommandTimer *)timer
{
    ILBaseCommand * command = [self getCommandWithTimer:timer];
    if (!command) {
       [self.commandLock unlock];
       return;
    }
    if (command.state == 3) return;
     command.state = 3;
    if (command.sendCommandCallback) {
        command.sendCommandCallback(nil,ERROR_TIMEOUT);
    }
    [self removeOneCommand:command];
    [self.commandLock unlock];
}

- (BOOL)didCompleteCommand:(ILBaseCommand *)command
{
    ILCommandTimer * timer = [self getTimerWithCommand:command];
    if (command.state != 3) {
        [self.commandLock unlock];
        command.state = 3;
    }
    [self removeOneCommand:command];
    if (timer)[timer stopTiming];
    return YES;
}

@end
