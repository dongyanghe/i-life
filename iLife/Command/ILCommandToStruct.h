//
//  ILCommandToStruct.h
//
//  Created by hedongyang on 2020/9/2.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILBaseModel.h"

@interface ILCommandToStruct : NSObject

+ (void)setStructFromModel:(ILBaseModel *)model
                 errorCode:(int *)errorCode;

@end

