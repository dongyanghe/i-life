//
//  ILCommandTimer.m
//
//  Created by hedongyang on 2020/9/1.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import "ILCommandTimer.h"

@interface ILCommandTimer()
@property (nonatomic,strong) NSTimer * timer;
@end

@implementation ILCommandTimer

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.timer = [[NSTimer alloc]init];
    }
    return self;
}

- (void)timerOut
{
    if (self.timeout == 0)return;
    self.timeout = 0;
    if (self.delegate && [self.delegate respondsToSelector:@selector(commandTimeoutWithModel:)]) {
        [self.delegate commandTimeoutWithModel:self];
    }
}

- (void)startTiming
{
    if (_timer) {
        [_timer invalidate];
         _timer = nil;
    }
   _timer = [NSTimer scheduledTimerWithTimeInterval:self.timeout
                                             target:self
                                           selector:@selector(timerOut)
                                           userInfo:nil
                                            repeats:NO];
}


- (void)stopTiming
{
    self.timeout = 0;
    if (_timer) {
        [_timer invalidate];
         _timer = nil;
     }
}

@end
 
