//
//  ILError.h
//  iLife
//
//  Created by hedongyang on 2020/9/12.
//  Copyright © 2020 hedongyang. All rights reserved.
//

#ifndef ILError_h
#define ILError_h


#define SUCCESS 0
#define ERROR_TIMEOUT 1
#define ERROR_NOT_FIND 2
#define ERROR_DISCONNECT 3
#define ERROR_OTA 4

#endif /* ILError_h */
