//
//  ILCommandToStruct.m
//
//  Created by hedongyang on 2020/9/2.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import "ILCommandToStruct.h"
#import "ILDeviceOtaModel.h"
#import "ILBindDeivceModel.h"
#import "ILGetDeviceInfoModel.h"
#import "ILGetLiveTempModel.h"
#import "ILUnBindDeviceModel.h"
#import "ILSetTimeModel.h"
#import "ILBlueManager.h"
#import "ILStructModel.h"
#import "ILError.h"

@implementation ILCommandToStruct

+ (void)setStructFromModel:(ILBaseModel*)model
                 errorCode:(int *)errorCode
{
    if (![ILBlueManager isConnected] || [ILBlueManager isPoweredOff]) {
       if (errorCode) {
           *errorCode = ERROR_DISCONNECT;
        }
        return;
    }
    if ([ILBlueManager isOta]) {
        if (errorCode) {
           *errorCode = ERROR_OTA;
        }
        return;
    }
    if (   [model isKindOfClass:[ILDeviceOtaModel class]]
        || [model isKindOfClass:[ILGetDeviceInfoModel class]]
        || [model isKindOfClass:[ILGetLiveTempModel class]]
        || [model isKindOfClass:[ILUnBindDeviceModel class]]) {
      struct protocol_head head;
       head.cmd = model.cmd;
       head.key = model.key;
       [ILBlueManager sendData:&head dataLength:sizeof(head)];
       if (errorCode) {
           *errorCode = SUCCESS;
       }
        return;
    }else if ([model isKindOfClass:[ILBindDeivceModel class]]) {
        struct protocol_start_bind start_bind;
        start_bind.head.cmd = model.cmd;
        start_bind.head.key = model.key;
        start_bind.system = 0x01;
        start_bind.clear_data_flag = 0x01;
        start_bind.system_version = [[[UIDevice currentDevice] systemVersion] integerValue];
        [ILBlueManager sendData:&start_bind dataLength:sizeof(start_bind)];
        if (errorCode) {
            *errorCode = SUCCESS;
        }
        return;
    }else if([model isKindOfClass:[ILSetTimeModel class]]) {
        struct protocol_set_time set_time;
        set_time.head.cmd = model.cmd;
        set_time.head.key = model.key;
        ILSetTimeModel * timeModel = (ILSetTimeModel *)model;
        set_time.year  = timeModel.year;
        set_time.month = timeModel.month;
        set_time.day   = timeModel.day;
        set_time.hour  = timeModel.hour;
        set_time.minute = timeModel.minute;
        set_time.second = timeModel.second;
        [ILBlueManager sendData:&set_time dataLength:sizeof(set_time)];
        if (errorCode) {
            *errorCode = SUCCESS;
        }
    }
    if (errorCode) {
        *errorCode = ERROR_NOT_FIND;
    }
}

@end
