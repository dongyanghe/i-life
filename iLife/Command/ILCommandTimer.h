//
//  ILCommandTimer.h
//
//  Created by hedongyang on 2020/9/1.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ILCommandTimer;

@protocol ILCommandTimerDelegate <NSObject>

- (void)commandTimeoutWithModel:(ILCommandTimer *)timer;

@end

@interface ILCommandTimer : NSObject
@property (nonatomic,weak) id<ILCommandTimerDelegate> delegate;
@property (nonatomic,assign) NSInteger timerId;
@property (nonatomic,assign) double timeout;
@property (nonatomic,assign) NSInteger type;
- (void)startTiming;
- (void)stopTiming;
@end

