//
//  ILCommandManager.h
//
//  Created by hedongyang on 2020/8/31.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILBaseCommand.h"

@protocol ILCommandManagerDelegate <NSObject>
- (ILBaseCommand *)commandCallback:(const uint8_t *)data dataLength:(uint16_t)length;
@end

@interface ILCommandManager : NSObject
@property (nonatomic,weak) id<ILCommandManagerDelegate> delegate;
+ (ILCommandManager *)shareInstance;
+ (BOOL)addCommand:(ILBaseCommand *)command target:(id)target;
+ (BOOL)didUpdateValue:(NSData *)data dataLength:(NSInteger)length;
@end

