//
//  ILBaseCommand.m
//
//  Created by hedongyang on 2020/8/31.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import "ILBaseCommand.h"
#import "ILError.h"
#import "ILCommandToStruct.h"
#import "ILCommandManager.h"
#import "ILStructModel.h"
#import "ILGetDeviceInfoModel.h"
#import "ILDeviceOtaModel.h"
#import "ILGetLiveTempModel.h"
#import "ILBindDeivceModel.h"
#import "ILUnBindDeviceModel.h"
#import "ILSetTimeModel.h"

@interface ILBaseCommand ()<ILCommandManagerDelegate>

@end

@implementation ILBaseCommand

- (instancetype)init
{
    self = [super init];
    if(self) {
        self.timeout = 5000;
    }
    return self;
}

- (void)dealloc
{
    NSLog(@"设置命令销毁");
    _model = nil;
    _sendCommandCallback = nil;
}

- (BOOL)sendCommand
{
    int errorCode = 0;
    [ILCommandToStruct setStructFromModel:self.model errorCode:&errorCode];
    self.errorCode = errorCode;
    if (errorCode == 0) {
        return YES;
    }else {
        if(self.sendCommandCallback) {
           self.sendCommandCallback(nil,(int)errorCode);
        }
        return NO;
    }
}

- (ILBaseCommand *)commandCallback:(const uint8_t *)data dataLength:(uint16_t)length
{
    if (length == 0) return nil;
    struct protocol_head *head = (struct protocol_head *)data;
    if (self.model.cmd != head->cmd || self.model.key != head->key) return nil;
    if (head->cmd == 0x01 && head->key == 0x01) { //进入ota
       struct protocol_ota_reply * ota_reply = (struct protocol_ota_reply *)data;
        ILDeviceOtaModel * otaModel = (ILDeviceOtaModel *)self.model;
        otaModel.state = ota_reply->state;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(otaModel,SUCCESS);
        }
    }else if (head->cmd == 0x02 && head->key == 0x01) { //获取设备信息
        struct protocol_device_info * device_info = (struct protocol_device_info *)data;
        ILGetDeviceInfoModel * deviceModel = (ILGetDeviceInfoModel *)self.model;
        deviceModel.deviceId  = device_info->device_id;
        deviceModel.version   = device_info->version;
        deviceModel.battLevel = device_info->batt_level;
        deviceModel.bindState = device_info->bind_state;
        deviceModel.platform  = device_info->platform;
        NSString * macAddr = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X",
                              device_info->mac_addr[0],
                              device_info->mac_addr[1],
                              device_info->mac_addr[2],
                              device_info->mac_addr[3],
                              device_info->mac_addr[4],
                              device_info->mac_addr[5]];
        deviceModel.macAddr = macAddr;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(deviceModel,SUCCESS);
        }
    }else if (head->cmd == 0x02 && head->key == 0x02) { //获取实时温度
        struct protocol_get_real_time_temp_reply * temp_reply = (struct protocol_get_real_time_temp_reply *)data;
        ILGetLiveTempModel * tempModel = (ILGetLiveTempModel *)self.model;
        NSString * temp = [NSString stringWithFormat:@"%02X.%02X",temp_reply->int_temp[0],temp_reply->int_temp[1]];
        tempModel.temperature = temp;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(tempModel,SUCCESS);
        }
    }else if (head->cmd == 0x03 && head->key == 0x01) { //绑定设备
        struct protocol_start_bind_reply * bind_reply = (struct protocol_start_bind_reply *)data;
        ILBindDeivceModel * bindModel = (ILBindDeivceModel *)self.model;
        bindModel.bindState = bind_reply->bind_state;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(bindModel,SUCCESS);
        }
    }else if (head->cmd == 0x03 && head->key == 0x02) { //解绑设备
        struct protocol_un_bind_reply * un_bind_reply = (struct protocol_un_bind_reply *)data;
        ILUnBindDeviceModel * unBindModel = (ILUnBindDeviceModel *)self.model;
        unBindModel.unBindState = un_bind_reply->state;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(unBindModel,SUCCESS);
        }
    }else if (head->cmd == 0x03 && head->key == 0x03) { //设置时间
        struct protocol_set_time_reply * time_reply = (struct protocol_set_time_reply *)data;
        ILSetTimeModel * timeModel = (ILSetTimeModel *)self.model;
        timeModel.state = time_reply->state;
        if(self.sendCommandCallback) {
           self.sendCommandCallback(timeModel,SUCCESS);
        }
    }
    return self;
}

@end
