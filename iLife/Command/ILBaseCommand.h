//
//  ILBaseCommand.h
//
//  Created by hedongyang on 2020/8/31.
//  Copyright © 2020 何东阳. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ILBaseModel.h"

@interface ILBaseCommand : NSObject
/**
 0:初始状态 1:等待发送 2:正在发送 3:发送完成
 */
@property (nonatomic,assign) NSInteger state;
/**
 1:发送命令 2:获取命令
 */
@property (nonatomic,assign) NSInteger type;
/**
 定时器ID
 */
@property (nonatomic,assign) NSInteger timeId;
/**
 超时时常 默认 5000毫秒
 */
@property (nonatomic,assign) double timeout;
/**
 命令错误码
 */
@property (nonatomic,assign) NSInteger errorCode;

/**
 命令模型数据
 */
@property (nonatomic,strong) ILBaseModel * model;
/**
 命令回调方法
 */
@property (nonatomic,copy) void(^sendCommandCallback)(ILBaseModel * model,int errorCode);

/**
 执行发送
 */
- (BOOL)sendCommand;

@end

